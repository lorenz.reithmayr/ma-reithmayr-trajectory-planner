import traceback

import rclpy
from geometry_msgs.msg import PoseStamped, PointStamped
from rclpy.node import Node


class TrajectoryGuidanceTester(Node):
    def __init__(self):
        super().__init__("trajectory_tester")
        self.goal_pub = self.create_publisher(PoseStamped, "/goal_pose", 10)

    def point_callback(self, msg):
        goal = PoseStamped()
        goal.header.frame_id = "map"
        goal.pose.position.x = -324.8
        goal.pose.position.y = 266.3
        goal.pose.position.z = 0.0
        self.goal_pub.publish(goal)


def main():
    rclpy.init(args=None)
    trajectory_tester = TrajectoryGuidanceTester()
    trajectory_tester.create_subscription(PointStamped, "/clicked_point", trajectory_tester.point_callback, 10)

    try:
        rclpy.spin(trajectory_tester)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        trajectory_tester.destroy_node()
        rclpy.shutdown()


if __name__ == "main":
    main()
