#ifndef NAV2_GROUND_TRUTH_COSTMAP_PLUGIN_GROUND_TRUTH_LAYER_H_
#define NAV2_GROUND_TRUTH_COSTMAP_PLUGIN_GROUND_TRUTH_LAYER_H_

#include <functional>
#include <mutex>
#include <algorithm>
#include <chrono>
#include <queue>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp/parameter_events_filter.hpp"

#include "tf2/LinearMath/Quaternion.h"
#include "tf2/LinearMath/Matrix3x3.h"

#include "nav2_costmap_2d/layer.hpp"
#include "nav2_costmap_2d/layered_costmap.hpp"
#include "nav2_costmap_2d/costmap_math.hpp"
#include "nav2_costmap_2d/footprint.hpp"

#include "pluginlib/class_list_macros.hpp"

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/pose_array.hpp"
#include "geometry_msgs/msg/pose2_d.hpp"
#include "custom_msgs/msg/trajectory2_d.hpp"
#include "custom_msgs/msg/custom_pose_stamped.hpp"
#include "custom_msgs/msg/custom_pose_array.hpp"
#include "custom_msgs/msg/custom_float64.hpp"
#include "custom_msgs/msg/radar_data.hpp"
#include "custom_msgs/msg/radar_returns.hpp"

typedef struct rollPitchYaw
{
	double roll = 0.0;
	double pitch = 0.0;
	double yaw = 0.0;
} rollPitchYaw;

namespace nav2_ground_truth_costmap_plugin
{
	using nav2_costmap_2d::FREE_SPACE;
	using nav2_costmap_2d::INSCRIBED_INFLATED_OBSTACLE;
	using nav2_costmap_2d::LETHAL_OBSTACLE;

	class GroundTruthLayer : public nav2_costmap_2d::Layer
	{
	 public:
		GroundTruthLayer();
		~GroundTruthLayer() override;

		// Base class methods

		/**
		 * @brief Initialization process of layer on startup
		 * @details Inherited from nav2_costmap_2d::Layer. Needs to be implemented by child class.
		 */
		void onInitialize() override;

		/**
		 * @brief Reset this costmap. Not used any further here.
		 * @details Inherited from nav2_costmap_2d::Layer. Needs to be implemented by child class.
		 */
		void reset() override
		{
		}

		/**
		 * @brief If clearing operations should be processed on this layer or not. Not used any further here.
		 */
		bool isClearable() override
		{
			return false;
		}

		/**
		 * @brief Update the bounds of the master costmap by this layer's update dimensions. Not used any further here.
		 * @param robot_x X pose of robot
		 * @param robot_y Y pose of robot
		 * @param robot_yaw Robot orientation
		 * @param min_x X min map coord of the window to update
		 * @param min_y Y min map coord of the window to update
		 * @param max_x X max map coord of the window to update
		 * @param max_y Y max map coord of the window to update
		 */
		void updateBounds(double robot_x,
			double robot_y,
			double robot_yaw,
			double *min_x,
			double *min_y,
			double *max_x,
			double *max_y) override;

		/**
		 * @brief Update the costs in the master costmap in the window. Not used any further here.
		 * @param master_grid The master costmap grid to update
		 * @param min_x X min map coord of the window to update
		 * @param min_y Y min map coord of the window to update
		 * @param max_x X max map coord of the window to update
		 * @param max_y Y max map coord of the window to update
		 */
		void updateCosts(nav2_costmap_2d::Costmap2D &master_grid, int min_i, int min_j, int max_i, int max_j) override;

	 protected:
		// Class methods
		/**
		 * @brief Declare the YAML file parameters that define plugin behaviour
		 */
		void declareParameters();

		/**
		 * @brief Assign the YAML file parameters that define plugin behaviour
		 */
		void getParameters();

		void radarCallback(const custom_msgs::msg::RadarReturns &radar_returns_msg);

		/**
		 * @brief Main loop that processes data and modifies the costmap.
		 */
		void eventLoop();

		/**
		 * @brief Overloaded method that draws the vessel position, its footprint and a buffer gradient onto the costmap
		 * @param pos_x x coordinate of the ground truth position
		 * @param pos_y y coordinate of the ground truth position
		 * @param theta Heading in [rad]
		 */
		void renderFootprint(const int32_t pos_x,
			const int32_t pos_y,
			double theta, const double dist);

		/**
		 * @brief Overloaded method that draws the vessel position, its footprint and a buffer gradient onto the costmap
		 * @param pos_x x coordinate of the ground truth position
		 * @param pos_y y coordinate of the ground truth position
		 * @param quaternion Quaternion defining the orientation of the ground truth position
		 */
		void renderFootprint(const int32_t pos_x,
			const int32_t pos_y,
			geometry_msgs::msg::Quaternion &quaternion);

		/**
		 * @brief Convert a world pose to costmap coordinates
		 * @param x x coordinate of the ground truth position
		 * @param y y coordinate of the ground truth position
		 */
		void convertPoseToCostmap(const int32_t x, const int32_t y);

		/**
		 * @brief Calculate the footprint of the obstacle vessel and mark the contained map cells as LETHAL_OBSTACLE
		 * @param orientation Quaternion describing the orientation
		 */
		void calculateOrientedFootprint(const geometry_msgs::msg::Quaternion &orientation);

		/**
		 * @brief Calculate the footprint of the obstacle vessel and mark the contained map cells as LETHAL_OBSTACLE
		 * @param orientation Orientation in radians
		 */
		void calculateOrientedFootprint(const double orientation);

		/**
		 * @brief Render a circular lethal zone with a cost of LETHAL_OBSTACLE around the current position of the OV
		 */
		void drawGradient(const double dist);

		/**
		 * @brief Reset the map cells of the previous position to FREE_SPACE and clear the associated buffers
		 */
		void clearMapCells();

		/**
		 * @brief Converts a Quaternion to an Euler representation
		 * @param q
		 * @return rollPitchYaw Struct containing roll, pitch and yaw angle as double
		 */
		static rollPitchYaw quaternionToEuler(const geometry_msgs::msg::Quaternion &q);

		/** @brief  Given a distance, compute a cost.
		 * @param  distance The distance in cells
		 * @return A cost value for the distance */
		inline unsigned char computeCost(double distance) const
		{
			unsigned char cost = 0;
			if (distance == 0)
			{
				cost = INSCRIBED_INFLATED_OBSTACLE;
			}
				// TODO: Manipulate inscribed_radius_ instead of fixing a number here
			// else if (distance * resolution_ <= 4.0 /*inscribed_radius_*/)
			// {
			// 	cost = INSCRIBED_INFLATED_OBSTACLE;
			// }
			// else
			{
				// make sure cost falls off by Euclidean distance
				double
					factor = exp(-1.0 * footprint_cost_scaling_factor_ * (distance * resolution_ - inscribed_radius_));
				cost = static_cast<unsigned char>((INSCRIBED_INFLATED_OBSTACLE - 1) * factor);
			}
			return cost;
		}

		inline static double computeEuclidianDistance(double x1, double y1, double x2, double y2)
		{
			return sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
		};

	 private:
		nav2_costmap_2d::Costmap2D *master_;
		int32_t costmap_pos_x_;
		int32_t costmap_pos_y_;

		rclcpp::Subscription<custom_msgs::msg::RadarReturns>::SharedPtr radar_sub_;
		rclcpp::TimerBase::SharedPtr timer_;

		std::vector<custom_msgs::msg::RadarData> radar_returns_;
		std::vector<nav2_costmap_2d::MapLocation> footprint_map_cells_{};
		std::vector<nav2_costmap_2d::MapLocation> filled_cells_{};
		std::vector<unsigned char> filled_cells_previous_costs_{};
		bool map_cells_filled_;
		double resolution_;
		double inscribed_radius_;

		std::queue<custom_msgs::msg::CustomPoseStamped> incoming_pose_queue_;

		// YAML Configuration Parameters
		std::vector<std::string> vessels_;
		std::string pose_topic_;
		std::string predictions_topic_;
		std::string distance_topic_;
		std::string covariance_ellipses_topic_;
		bool enable_predictions_;
		std::string footprint_;
		bool enable_footprint_inflation_;
		double footprint_inflation_radius_;
		double footprint_cost_scaling_factor_;
		double min_prediction_distance_;
	};

} // namespace nav2_ground_truth_costmap_plugin

#endif // NAV2_GROUND_TRUTH_COSTMAP_PLUGIN_GROUND_TRUTH_LAYER_H_
