#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma ide diagnostic ignored "cppcoreguidelines-pro-type-member-init"
#include "nav2_ground_truth_costmap_plugin/ground_truth_layer.h"
PLUGINLIB_EXPORT_CLASS(nav2_ground_truth_costmap_plugin::GroundTruthLayer, nav2_costmap_2d::Layer)

using namespace std::chrono_literals;

namespace nav2_ground_truth_costmap_plugin
{
	GroundTruthLayer::GroundTruthLayer()
		: master_(nullptr),
		  costmap_pos_x_(0),
		  costmap_pos_y_(0),
		  map_cells_filled_(false)
	{
	}

	GroundTruthLayer::~GroundTruthLayer()
	{
		delete master_;
	}

	void GroundTruthLayer::onInitialize()
	{
		declareParameters();
		getParameters();
		auto node = node_.lock();
		if (!node)
		{
			throw std::runtime_error("Failed to lock node!");
		}

		radar_sub_ = node->create_subscription<custom_msgs::msg::RadarReturns>("/radar_returns",
			rclcpp::QoS(10),
			std::bind(&GroundTruthLayer::radarCallback, this, std::placeholders::_1));
		timer_ = node->create_wall_timer(1000ms, std::bind(&GroundTruthLayer::eventLoop, this));

		resolution_ = layered_costmap_->getCostmap()->getResolution();

		// TODO: Check if this is even set for our case, or just the default value
		inscribed_radius_ = layered_costmap_->getInscribedRadius();
	}

	void GroundTruthLayer::declareParameters()
	{
		declareParameter("enabled", rclcpp::ParameterValue(true));
		declareParameter("enable_predictions", rclcpp::ParameterValue(false));
		declareParameter("min_prediction_distance", rclcpp::ParameterValue(50.0));
		declareParameter("pose_topic", rclcpp::ParameterValue(""));
		declareParameter("predictions_topic", rclcpp::ParameterValue(""));
		declareParameter("distance_topic", rclcpp::ParameterValue(""));
		declareParameter("footprint", rclcpp::ParameterValue(""));
		declareParameter("enable_footprint_inflation", rclcpp::ParameterValue(true));
		declareParameter("footprint_inflation_radius", rclcpp::ParameterValue(10.0));
		declareParameter("footprint_cost_scaling_factor", rclcpp::ParameterValue(0.5));
		std::vector<std::string> vessels_init;
	}

	void GroundTruthLayer::getParameters()
	{
		auto node = node_.lock();
		if (!node)
		{
			throw std::runtime_error("Failed to lock node!");
		}
		node->get_parameter(name_ + "." + "enabled", enabled_);
		node->get_parameter(name_ + "." + "enable_predictions", enable_predictions_);
		node->get_parameter(name_ + "." + "pose_topic", pose_topic_);
		node->get_parameter(name_ + "." + "predictions_topic", predictions_topic_);
		node->get_parameter(name_ + "." + "distance_topic", distance_topic_);
		node->get_parameter(name_ + "." + "min_prediction_distance", min_prediction_distance_);
		node->get_parameter(name_ + "." + "footprint", footprint_);
		node->get_parameter(name_ + "." + "enable_footprint_inflation", enable_footprint_inflation_);
		node->get_parameter(name_ + "." + "footprint_inflation_radius", footprint_inflation_radius_);
		node->get_parameter(name_ + "." + "footprint_cost_scaling_factor", footprint_cost_scaling_factor_);
	}

	void GroundTruthLayer::radarCallback(const custom_msgs::msg::RadarReturns &radar_returns_msg)
	{
		radar_returns_ = radar_returns_msg.returns;
	}

	void GroundTruthLayer::eventLoop()
	{
		master_ = layered_costmap_->getCostmap();
		if (map_cells_filled_)
		{
			clearMapCells();
		}

		// Loop through all vessels in the simulation
		for (auto &blip : radar_returns_)
		{
			// Only draw footprints if the WAMV is closer than the minimum distance
			if (blip.distance < min_prediction_distance_)
			{
				// Predictions disabled: Only render the footprint at the vessel pose
				renderFootprint(blip.pose.position.x, blip.pose.position.y, blip.pose.orientation);

				// Predictions enabled: Also render a gradient footprint at each trajectory pose
				if (enable_predictions_)
				{
					auto first_pose = blip.pose;

					// Traversing the pose array backwards so that the vessel pose is drawn on top of the predicted
					// poses
					for (std::reverse_iterator it = blip.trajectory.poses.rbegin(); it != blip.trajectory.poses.rend(); ++it)
					{
						auto dist_to_first_pose = computeEuclidianDistance(first_pose.position.x, first_pose.position.y, it->x, it->y);
						renderFootprint(it->x, it->y,it->theta, dist_to_first_pose);
					}
				}
			}
		}
	}

	void GroundTruthLayer::renderFootprint(const int32_t pos_x,
		const int32_t pos_y,
		geometry_msgs::msg::Quaternion &quaternion)
	{
		convertPoseToCostmap(pos_x, pos_y);
		calculateOrientedFootprint(quaternion);
		drawGradient(0.0);
	}

	void GroundTruthLayer::renderFootprint(const int32_t pos_x, const int32_t pos_y, double theta, const double dist)
	{
		convertPoseToCostmap(pos_x, pos_y);
		calculateOrientedFootprint(theta);
		drawGradient(dist);
	}

	void GroundTruthLayer::convertPoseToCostmap(const int32_t x, const int32_t y)
	{
		costmap_pos_x_ = static_cast<int32_t>((x - master_->getOriginX()) * 1
			/ master_->getResolution());
		costmap_pos_y_ = static_cast<int32_t>((y - master_->getOriginY()) * 1
			/ master_->getResolution());
	}

	void GroundTruthLayer::calculateOrientedFootprint(const geometry_msgs::msg::Quaternion &orientation)
	{
		std::vector<geometry_msgs::msg::Point> footprint_polygon{};
		nav2_costmap_2d::makeFootprintFromString(footprint_, footprint_polygon);

		rollPitchYaw euler_angles = quaternionToEuler(orientation);
		std::vector<geometry_msgs::msg::Point> oriented_footprint_polygon{};
		nav2_costmap_2d::transformFootprint(costmap_pos_x_,
			costmap_pos_y_,
			euler_angles.yaw,
			footprint_polygon,
			oriented_footprint_polygon);
		nav2_costmap_2d::MapLocation fp_point_map_location{};
		std::vector<nav2_costmap_2d::MapLocation> footprint_map_location{};
		if (costmap_pos_x_ && costmap_pos_y_)
		{
			for (auto &pt : oriented_footprint_polygon)
			{
				fp_point_map_location.x = pt.x;
				fp_point_map_location.y = pt.y;
				footprint_map_location.emplace_back(fp_point_map_location);
			}
		}
		// Get all cells that are inside the footprint outline and store them in <<footprint_map_cells_>>
		master_->convexFillCells(footprint_map_location, footprint_map_cells_);
	}

	void GroundTruthLayer::calculateOrientedFootprint(const double orientation)
	{
		std::vector<geometry_msgs::msg::Point> footprint_polygon{};
		nav2_costmap_2d::makeFootprintFromString(footprint_, footprint_polygon);

		std::vector<geometry_msgs::msg::Point> oriented_footprint_polygon{};
		nav2_costmap_2d::transformFootprint(costmap_pos_x_,
			costmap_pos_y_,
			orientation,
			footprint_polygon,
			oriented_footprint_polygon);
		nav2_costmap_2d::MapLocation fp_point_map_location{};
		std::vector<nav2_costmap_2d::MapLocation> footprint_map_location{};
		if (costmap_pos_x_ && costmap_pos_y_)
		{
			for (auto &pt : oriented_footprint_polygon)
			{
				fp_point_map_location.x = pt.x;
				fp_point_map_location.y = pt.y;
				footprint_map_location.emplace_back(fp_point_map_location);
			}
		}
		// Get all cells that are inside the footprint outline and store them in <<footprint_map_cells_>>
		master_->convexFillCells(footprint_map_location, footprint_map_cells_);
	}

	void GroundTruthLayer::drawGradient(const double dist)
	{
		std::lock_guard<nav2_costmap_2d::Costmap2D::mutex_t> guard(*master_->getMutex());
		auto cost = 0.0;
		if (enable_footprint_inflation_)
		{
			cost = computeCost(dist);
		}
		else
		{
			cost = LETHAL_OBSTACLE;
		}
		for (auto &cell : footprint_map_cells_)
		{
			master_->setCost(cell.x, cell.y, cost);
			filled_cells_.emplace_back(cell);
		}
		footprint_map_cells_.clear();
		map_cells_filled_ = true;
	}

	void GroundTruthLayer::clearMapCells()
	{
		std::lock_guard<nav2_costmap_2d::Costmap2D::mutex_t> guard(*master_->getMutex());
		for (auto &cell : filled_cells_)
		{
			// auto previous_cost = filled_cells_previous_costs_.at(idx);
			master_->setCost(cell.x, cell.y, FREE_SPACE);
		}
		filled_cells_.clear();
		filled_cells_previous_costs_.clear();
		map_cells_filled_ = false;
	}

	rollPitchYaw GroundTruthLayer::quaternionToEuler(const geometry_msgs::msg::Quaternion &q)
	{
		tf2::Quaternion q_tf2(q.x, q.y, q.z, q.w);
		tf2::Matrix3x3 mat(q_tf2);
		rollPitchYaw rpy;
		mat.getRPY(rpy.roll, rpy.pitch, rpy.yaw);
		return rpy;
	}

	void GroundTruthLayer::updateBounds(double /*robot_x*/,
		double /*robot_y*/,
		double /*robot_yaw*/,
		double * /*min_x*/,
		double * /*min_y*/,
		double * /*max_x*/,
		double * /*max_y*/)
	{
	}

	void GroundTruthLayer::updateCosts(nav2_costmap_2d::Costmap2D & /*master_grid*/,
		int /*min_i*/,
		int /*min_j*/,
		int /*max_i*/,
		int /*max_j*/)
	{
	}
} // namespace nav2_ground_truth_costmap_plugin