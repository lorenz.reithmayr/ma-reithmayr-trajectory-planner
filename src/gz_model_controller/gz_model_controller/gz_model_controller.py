import traceback
# noinspection PyUnresolvedReferences
from custom_msgs.msg import RadarData, RadarReturns
from geometry_msgs.msg import Twist

import rclpy
from rclpy.node import Node

"""
Publishes a velocity command to every model vessel in the Gazebo simulation
"""


class GzModelController(Node):
    def __init__(self):
        super().__init__("gz_model_controller")
        self.declare_parameters(namespace='', parameters=[('surge_vel', 2.0)])
        self.surge_vel = self.get_parameter('surge_vel').get_parameter_value().double_value

        self.radar_returns = None

    def radar_callback(self, radar_msg):
        self.radar_returns = radar_msg
        self.send_cmd_vel()

    def send_cmd_vel(self):
        if self.radar_returns is None:
            return
        for vessel in self.radar_returns.returns:
            cmd_vel = Twist()
            cmd_vel.linear.x = self.surge_vel
            cmd_vel_pub = self.create_publisher(Twist, f"/model{vessel.name}/cmd_vel", 10)
            cmd_vel_pub.publish(cmd_vel)


def main():
    rclpy.init(args=None)

    # Spawn modules
    gz_controller = GzModelController()

    gz_controller.create_subscription(msg_type=RadarReturns, topic="/radar_returns",
                                      callback=gz_controller.radar_callback, qos_profile=10)

    try:
        rclpy.spin(gz_controller)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        gz_controller.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
