#include "global_planner/global_planner_node.h"

GlobalPlanner::GlobalPlanner() : rclcpp::Node("global_planner"),
								 costmap_msg_(nullptr),
								 costmap_(nullptr)
{
	logger_ = this->get_logger();
	clock_ = this->get_clock();

	declareParameters();
	setupSubscriptions();
	setupPublishers();

	std::vector<rclcpp::Parameter> theta_star_planner_params;
	theta_star_planner_params.push_back(this->get_parameter("allow_unknown"));
	theta_star_planner_params.push_back(this->get_parameter("how_many_corners"));
	theta_star_planner_params.push_back(this->get_parameter("w_euc_cost"));
	theta_star_planner_params.push_back(this->get_parameter("w_traversal_cost"));
	theta_star_planner_ = std::make_unique<ThetaStarPlanner>(logger_, costmap_, theta_star_planner_params);
}

void GlobalPlanner::declareParameters()
{
	this->declare_parameter("allow_unknown", rclcpp::ParameterValue(false));
	this->declare_parameter("how_many_corners", rclcpp::ParameterValue(8));
	this->declare_parameter("w_euc_cost", rclcpp::ParameterValue(1.0));
	this->declare_parameter("w_traversal_cost", rclcpp::ParameterValue(2.0));
}

void GlobalPlanner::setupSubscriptions()
{
	initial_pose_sub_ = this->create_subscription<geometry_msgs::msg::PoseWithCovarianceStamped>(
		"/initialpose",
		rclcpp::QoS(10),
		std::bind(&GlobalPlanner::initialPoseCallback, this, std::placeholders::_1));

	goal_sub_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
		"/goal_pose",
		rclcpp::QoS(10),
		std::bind(&GlobalPlanner::goalCallback, this, std::placeholders::_1));

	costmap_sub_ = this->create_subscription<nav2_msgs::msg::Costmap>("/global_costmap/costmap_raw",
		rclcpp::QoS(10),
		std::bind(&GlobalPlanner::costmapCallback, this, std::placeholders::_1));

	pose_sub_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
		"/wamv/pose_stamped",
		rclcpp::QoS(10),
		std::bind(&GlobalPlanner::wamvPoseCallback, this, std::placeholders::_1));
}

void GlobalPlanner::setupPublishers()
{
	global_path_pub_ = this->create_publisher<nav_msgs::msg::Path>("/global_path", 10);
}

void GlobalPlanner::goalCallback(const geometry_msgs::msg::PoseStamped &goal_msg)
{
	goal_ = goal_msg;
	planGlobalPath();
}

void GlobalPlanner::costmapCallback(const nav2_msgs::msg::Costmap::SharedPtr msg) // NOLINT(*-unnecessary-value-param)
{
	costmap_msg_ = msg;
	costmapMsgToCostmap2D();
}

void GlobalPlanner::wamvPoseCallback(const geometry_msgs::msg::PoseStamped &pose_msg)
{
	current_pose_ = pose_msg;
}

void GlobalPlanner::initialPoseCallback(const geometry_msgs::msg::PoseWithCovarianceStamped &initial_pose_msg)
{
	initial_pose_.header = initial_pose_msg.header;
	initial_pose_.pose = initial_pose_msg.pose.pose;
	USE_INITIAL_POSE = true;
}

void GlobalPlanner::planGlobalPath()
{
	theta_star_planner_->setCostmap(costmap_);
	if (USE_INITIAL_POSE)
	{
		theta_star_planner_->initPlan(initial_pose_, goal_);
	}
	else
	{
		theta_star_planner_->initPlan(current_pose_, goal_);
	}
	global_path_ = theta_star_planner_->getPlan();
	if (global_path_.poses.empty())
	{
		RCLCPP_ERROR(logger_, "No valid path was found!");
		return;
	}
	global_path_pub_->publish(global_path_);
	global_path_.poses.clear();
	USE_INITIAL_POSE = false;
}

void GlobalPlanner::costmapMsgToCostmap2D()
{
	if (costmap_ == nullptr)
	{
		costmap_ = std::make_shared<nav2_costmap_2d::Costmap2D>(
			costmap_msg_->metadata.size_x, costmap_msg_->metadata.size_y,
			costmap_msg_->metadata.resolution, costmap_msg_->metadata.origin.position.x,
			costmap_msg_->metadata.origin.position.y);
	}
	else if (costmap_->getSizeInCellsX() != costmap_msg_->metadata.size_x ||
		costmap_->getSizeInCellsY() != costmap_msg_->metadata.size_y ||
		costmap_->getResolution() != costmap_msg_->metadata.resolution ||
		costmap_->getOriginX() != costmap_msg_->metadata.origin.position.x ||
		costmap_->getOriginY() != costmap_msg_->metadata.origin.position.y)
	{
		// Update the size of the costmap
		costmap_->resizeMap(
			costmap_msg_->metadata.size_x, costmap_msg_->metadata.size_y,
			costmap_msg_->metadata.resolution,
			costmap_msg_->metadata.origin.position.x,
			costmap_msg_->metadata.origin.position.y);
	}

	unsigned char *master_array = costmap_->getCharMap();
	unsigned int index = 0;
	for (unsigned int i = 0; i < costmap_msg_->metadata.size_x; ++i)
	{
		for (unsigned int j = 0; j < costmap_msg_->metadata.size_y; ++j)
		{
			master_array[index] = costmap_msg_->data[index];
			++index;
		}
	}
}
