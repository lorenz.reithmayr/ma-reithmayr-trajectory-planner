#include <memory>
#include "global_planner/global_planner_node.h"
#include "rclcpp/rclcpp.hpp"

int main(int argc, char *argv[])
{
	rclcpp::init(argc, argv);
	auto global_planner_node = std::make_shared<GlobalPlanner>();
	rclcpp::executors::MultiThreadedExecutor executor;
	executor.add_node(global_planner_node);
	executor.spin();
	rclcpp::shutdown();
	return 0;
}
