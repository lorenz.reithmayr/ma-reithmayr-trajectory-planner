/*
 * Adapted from nav2_theta_star_planner at
 * https://github.com/ros-planning/navigation2/tree/main/nav2_theta_star_planner
 *
 * Copyright 2020 Anshumaan Singh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "global_planner/theta_star_planner.h"

ThetaStarPlanner::ThetaStarPlanner(rclcpp::Logger &parent_logger,
	std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap, std::vector<rclcpp::Parameter> &params) :
	logger_(parent_logger), costmap_(costmap), use_final_approach_orientation_(false)
{
	theta_star_ = std::make_unique<theta_star::ThetaStar>();
	theta_star_->costmap_ = costmap_.get();

	// Parameters passed in from parent LocalPlanner node
	theta_star_->allow_unknown_ = params.at(0).as_bool();
	theta_star_->how_many_corners_ = params.at(1).as_int();
	theta_star_->w_euc_cost_ = params.at(2).as_double();
	theta_star_->w_traversal_cost_ = params.at(3).as_double();
	theta_star_->w_heuristic_cost_ = (theta_star_->w_euc_cost_ < 1.0) ? theta_star_->w_euc_cost_ : 1.0;

	RCLCPP_INFO(logger_, "Got Theta* parameters:\n");
	for (auto &p : params)
	{
		RCLCPP_INFO(logger_, "%s: %s", p.get_name().c_str(), p.value_to_string().c_str());
	}
}

void ThetaStarPlanner::initPlan(const geometry_msgs::msg::PoseStamped &start,
	const geometry_msgs::msg::PoseStamped &goal)
{
	auto start_time = std::chrono::steady_clock::now();
	nav_msgs::msg::Path global_path;
	std::unique_lock<nav2_costmap_2d::Costmap2D::mutex_t> lock(*(theta_star_->costmap_->getMutex()));

	// Corner case of start and goal beeing on the same cell
	unsigned int mx_start = 0, my_start = 0, mx_goal = 0, my_goal = 0;
	if (!theta_star_->costmap_->worldToMap(
		start.pose.position.x, start.pose.position.y, mx_start, my_start))
	{
		RCLCPP_ERROR(logger_, "Start outside of map bounds!");
		return;
	}

	if (!theta_star_->costmap_->worldToMap(
		goal.pose.position.x, goal.pose.position.y, mx_goal, my_goal))
	{
		RCLCPP_ERROR(logger_, "Goal outside of map bounds!");
		return;
	}

	if (theta_star_->costmap_->getCost(mx_start, my_start) == nav2_costmap_2d::LETHAL_OBSTACLE)
	{
		RCLCPP_ERROR(logger_, "Start on lethal cell!");
		return;
	}

	if (theta_star_->costmap_->getCost(mx_goal, my_goal) == nav2_costmap_2d::LETHAL_OBSTACLE)
	{
		RCLCPP_ERROR(logger_, "Goal on lethal cell!");
		return;
	}

	if (mx_start == mx_goal && my_start == my_goal)
	{
		global_path.header.frame_id = "map";
		geometry_msgs::msg::PoseStamped pose;
		pose.header = global_path.header;
		pose.pose.position.z = 0.0;

		pose.pose = start.pose;

		// if we have a different start and goal orientation, set the unique path pose to the goal
		// orientation, unless use_final_approach_orientation=true where we need it to be the start
		// orientation to avoid movement from the local planner
		if (start.pose.orientation != goal.pose.orientation && !use_final_approach_orientation_)
		{
			pose.pose.orientation = goal.pose.orientation;
		}
		global_path.poses.push_back(pose);
	}

	theta_star_->setStartAndGoal(start, goal);
	// RCLCPP_WARN(
	// 	logger_, "Got the src and dst... (%i, %i) && (%i, %i)",
	// 	theta_star_->src_.x, theta_star_->src_.y, theta_star_->dst_.x, theta_star_->dst_.y);
	RCLCPP_INFO(logger_, "Commencing planning.");
	createPlan(global_path);
	// check if a plan is generated
	size_t plan_size = global_path.poses.size();
	if (plan_size > 0)
	{
		global_path.poses.back().pose.orientation = goal.pose.orientation;
	}

	// If use_final_approach_orientation=true, interpolate the last pose orientation from the
	// previous pose to set the orientation to the 'final approach' orientation of the robot so
	// it does not rotate.
	// And deal with corner case of plan of length 1
	if (use_final_approach_orientation_)
	{
		if (plan_size == 1)
		{
			global_path.poses.back().pose.orientation = start.pose.orientation;
		}
		else if (plan_size > 1)
		{
			double dx = 0.0, dy = 0.0, theta = 0.0;
			auto last_pose = global_path.poses.back().pose.position;
			auto approach_pose = global_path.poses[plan_size - 2].pose.position;
			dx = last_pose.x - approach_pose.x;
			dy = last_pose.y - approach_pose.y;
			theta = atan2(dy, dx);
			global_path.poses.back().pose.orientation =
				nav2_util::geometry_utils::orientationAroundZAxis(theta);
		}
	}

	auto stop_time = std::chrono::steady_clock::now();
	auto dur = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time);
	RCLCPP_WARN(logger_, "Theta* time: %d ms", static_cast<int>(dur.count()));
	// RCLCPP_WARN(logger_, "The nodes_opened are:  %i", theta_star_->nodes_opened);
	// RCLCPP_WARN(logger_, "The number of nodes in the path is: %zu", plan_size);
	global_path_ = global_path;
}

void ThetaStarPlanner::createPlan(nav_msgs::msg::Path &path_msg)
{
	std::vector<coordsW> path;
	if (theta_star_->isUnsafeToPlan())
	{
		path_msg.poses.clear();
		RCLCPP_ERROR(logger_, "Either of the start or goal pose are an obstacle!");
		return;
	}
	else if (theta_star_->generatePath(path))
	{
		path_msg = constructPathMsg(path, costmap_->getResolution());
	}
	else
	{
		path_msg.poses.clear();
		RCLCPP_ERROR(logger_, "Could not generate path between the given poses!");
		return;
	}
	path_msg.header.frame_id = "map";
}

nav_msgs::msg::Path ThetaStarPlanner::constructPathMsg(const std::vector<coordsW> &raw_path,
	const double &costmap_resolution)
{
	nav_msgs::msg::Path pa;
	auto dist = 0.0;

	geometry_msgs::msg::PoseStamped p1;
	for (unsigned int j = 0; j < raw_path.size() - 1; j++)
	{
		coordsW pt1 = raw_path[j];
		p1.pose.position.x = pt1.x;
		p1.pose.position.y = pt1.y;
		pa.poses.push_back(p1);

		coordsW pt2 = raw_path[j + 1];
		double distance = std::hypot(pt2.x - pt1.x, pt2.y - pt1.y);
		dist += distance;
		int loops = static_cast<int>(distance / costmap_resolution);
		double sin_alpha = (pt2.y - pt1.y) / distance;
		double cos_alpha = (pt2.x - pt1.x) / distance;
		for (int k = 1; k < loops; k++)
		{
			p1.pose.position.x = pt1.x + k * costmap_resolution * cos_alpha;
			p1.pose.position.y = pt1.y + k * costmap_resolution * sin_alpha;
			pa.poses.push_back(p1);
		}
	}
	RCLCPP_WARN(logger_, "Theta* distance: %f m", dist);
	return pa;
}

void ThetaStarPlanner::setCostmap(std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap)
{
	costmap_ = costmap;
	theta_star_->costmap_ = costmap.get();
}
