#ifndef MA_REITHMAYR_TRAJECTORY_PLANNER_SRC_GLOBAL_PLANNER_SRC_GLOBAL_PLANNER_NODE_H_
#define MA_REITHMAYR_TRAJECTORY_PLANNER_SRC_GLOBAL_PLANNER_SRC_GLOBAL_PLANNER_NODE_H_

#include "rclcpp/rclcpp.hpp"
#include "rclcpp/logger.hpp"

#include "theta_star_planner.h"

#include "nav2_costmap_2d/costmap_2d.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"
#include "nav_msgs/msg/path.hpp"

class GlobalPlanner : public rclcpp::Node
{
 public:
	GlobalPlanner();

	void costmapCallback(const nav2_msgs::msg::Costmap::SharedPtr msg);
	void wamvPoseCallback(const geometry_msgs::msg::PoseStamped &pose_msg);
	void initialPoseCallback(const geometry_msgs::msg::PoseWithCovarianceStamped &initial_pose_msg);
	void goalCallback(const geometry_msgs::msg::PoseStamped &goal_msg);

	void declareParameters();
	void setupSubscriptions();
	void setupPublishers();

	void planGlobalPath();
	void costmapMsgToCostmap2D();

 private:
	rclcpp::Logger logger_{ rclcpp::get_logger("GlobalPlanner") };
	std::shared_ptr<rclcpp::Clock> clock_;

	rclcpp::Subscription<nav2_msgs::msg::Costmap>::SharedPtr costmap_sub_;
	rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr pose_sub_;
	rclcpp::Subscription<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr initial_pose_sub_;
	rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr goal_sub_;

	rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr global_path_pub_;

	nav2_msgs::msg::Costmap::SharedPtr costmap_msg_;
	std::shared_ptr<nav2_costmap_2d::Costmap2D> costmap_;
	geometry_msgs::msg::PoseStamped current_pose_;
	geometry_msgs::msg::PoseStamped initial_pose_;
	geometry_msgs::msg::PoseStamped goal_;
	nav_msgs::msg::Path global_path_;

	std::unique_ptr<ThetaStarPlanner> theta_star_planner_;

	bool USE_INITIAL_POSE;
};
#endif //MA_REITHMAYR_TRAJECTORY_PLANNER_SRC_GLOBAL_PLANNER_SRC_GLOBAL_PLANNER_NODE_H_
