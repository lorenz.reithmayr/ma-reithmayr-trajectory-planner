/*
 * Adapted from nav2_theta_star_planner at
 * https://github.com/ros-planning/navigation2/tree/main/nav2_theta_star_planner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef DWA_LOCAL_PLANNER_SRC_DWA_LOCAL_PLANNER_SRC_REPLANNER_H_
#define DWA_LOCAL_PLANNER_SRC_DWA_LOCAL_PLANNER_SRC_REPLANNER_H_

#include <cmath>

#include "rclcpp/rclcpp.hpp"
#include "nav2_theta_star_planner/theta_star.hpp"
#include "nav2_costmap_2d/costmap_2d.hpp"
#include "nav2_costmap_2d/cost_values.hpp"
#include "nav2_util/robot_utils.hpp"
#include "nav2_util/geometry_utils.hpp"

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/path.hpp"

class ThetaStarPlanner
{
 public:
	ThetaStarPlanner(rclcpp::Logger &parent_logger,
		std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap,
		std::vector<rclcpp::Parameter> &params);

/**
	 * Called from the DWA Node. Does some setup and error checking, and calls createPlan
	 * @param start Start pose of the path
	 * @param goal Goal pose of the path
	 * @return The global path ROS msg
	 */
	void initPlan(const geometry_msgs::msg::PoseStamped &start, const geometry_msgs::msg::PoseStamped &goal);

	/**
	 * Called from initPlan. Calls out to the actual Theta* algorithm to obtain the raw path,
	 * and constructs the ROS path message from it by calling constructPathMsg
	 * @param[out] path_msg The resulting global path
	 */
	void createPlan(nav_msgs::msg::Path &path_msg);

	/**
	 * Called from createPlan. Linearly interpolates between raw path points to align them with the costmap grid, and
	 * creates a ROS path message for createPlan
	 * @param raw_path Vector of x,y coordinates obtained from the Theta* path planner
	 * @param costmap_resolution Resolution of the costmap in [m] used for interpolation
	 * @return ROS path message of the global path
	 */
	nav_msgs::msg::Path constructPathMsg(const std::vector<coordsW> &raw_path, const double &costmap_resolution);

	nav_msgs::msg::Path getPlan()
	{
		return global_path_;
	}
	void setCostmap(std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap);

 private:
	rclcpp::Logger logger_{ rclcpp::get_logger("LocalPlanner") };
	std::shared_ptr<nav2_costmap_2d::Costmap2D> costmap_;
	std::unique_ptr<theta_star::ThetaStar> theta_star_;
	bool use_final_approach_orientation_;
	nav_msgs::msg::Path global_path_;
};

#endif //DWA_LOCAL_PLANNER_SRC_DWA_LOCAL_PLANNER_SRC_REPLANNER_H_
