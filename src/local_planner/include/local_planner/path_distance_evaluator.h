#ifndef LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_PATH_DISTANCE_EVALUATOR_H_
#define LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_PATH_DISTANCE_EVALUATOR_H_

#include <cmath>
#include <nav_msgs/msg/path.hpp>

#include "trajectory_evaluator.h"

class PathDistanceEvaluator : public TrajectoryEvaluator
{
 public:
	PathDistanceEvaluator() = default;

	/**
	 * Evaluates a trajectory by how far it strays from the global path by adding up the euclidian distances
	 * from each point on the the trajectory to each point on the path
	 * @param logger
	 * @param costmap
	 * @param global_path
	 */
	PathDistanceEvaluator(rclcpp::Logger &logger);

	std::optional<double> evaluateTrajectory(const Trajectory &traj) override;
	static nav_msgs::msg::Path getGlobalPathSlice(nav_msgs::msg::Path &global_path);
};

#endif //LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_PATH_DISTANCE_EVALUATOR_H_
