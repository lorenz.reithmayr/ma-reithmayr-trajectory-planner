#ifndef DWA_LOCAL_PLANNER_SRC_DWA_LOCAL_PLANNER_SRC_COLLISION_CHECKER_H_
#define DWA_LOCAL_PLANNER_SRC_DWA_LOCAL_PLANNER_SRC_COLLISION_CHECKER_H_

#include <utility>
#include <algorithm>
#include <functional>

#include "rclcpp/rclcpp.hpp"
#include "local_planner/collision_checker.h"
#include "nav2_costmap_2d/costmap_2d.hpp"
#include "nav2_costmap_2d/cost_values.hpp"
#include "nav2_costmap_2d/footprint_collision_checker.hpp"
#include "nav2_costmap_2d/footprint.hpp"
#include "nav_msgs/msg/path.hpp"

#include "custom_msgs/msg/trajectory2_d.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "geometry_msgs/msg/polygon_stamped.hpp"

typedef std::vector<geometry_msgs::msg::Point> Footprint;
typedef custom_msgs::msg::Trajectory2D Trajectory;
typedef nav2_costmap_2d::MapLocation MapLocation;

class CollisionChecker
{
 public:
	CollisionChecker(rclcpp::Logger &parent_logger,
		std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap,
		Footprint &robot_footprint,
		std::shared_ptr<nav2_costmap_2d::FootprintCollisionChecker<std::shared_ptr<nav2_costmap_2d::Costmap2D>>> &footprint_collision_checker,
		rclcpp::Publisher<geometry_msgs::msg::PolygonStamped>::SharedPtr &collision_marker_pub,
		double dt
	);

 public:
	bool predictCollision(const Trajectory &t1,
		const Trajectory &t2,
		const Footprint &footprint1,
		const Footprint &footprint2);
	bool checkFootprintCollision(const Footprint &footprint1, const Footprint &footprint2);
	int checkPathCollision(const nav_msgs::msg::Path &path);
	void constructOrientedFootprint(const geometry_msgs::msg::Pose2D &pose);
	static Footprint constructOrientedFootprint(const geometry_msgs::msg::Pose2D &pose, const Footprint &footprint);
	std::vector<MapLocation> getCellsInsideFootprint(const Footprint &fp);
	void setCostmap(const std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap);
	void publishCollisionMarker(const Footprint &fp1);
	[[nodiscard]] double getTimeToCollision() const;

 private:
	rclcpp::Logger logger_{ rclcpp::get_logger("LocalPlanner") };
	std::shared_ptr<nav2_costmap_2d::Costmap2D> costmap_;
	Footprint footprint_;
	Footprint oriented_footprint_;
	std::shared_ptr<nav2_costmap_2d::FootprintCollisionChecker<std::shared_ptr<nav2_costmap_2d::Costmap2D>>>
		footprint_collision_checker_;
	rclcpp::Publisher<geometry_msgs::msg::PolygonStamped>::SharedPtr collision_marker_pub_;
	double dt_;
	double time_to_collision_;
};
#endif //DWA_LOCAL_PLANNER_SRC_DWA_LOCAL_PLANNER_SRC_COLLISION_CHECKER_H_
