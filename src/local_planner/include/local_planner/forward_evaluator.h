#ifndef LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_FORWARD_EVALUATOR_H_
#define LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_FORWARD_EVALUATOR_H_

#include "trajectory_evaluator.h"

class ForwardEvaluator : public TrajectoryEvaluator
{
 public:
	ForwardEvaluator() = default;
	explicit ForwardEvaluator(rclcpp::Logger &logger);

	std::optional<double> evaluateTrajectory(const Trajectory &traj) override;
	void setMaxYawVel(const double val);

 private:
	double max_cmd_yaw_{ 0.2 };
};

#endif //LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_FORWARD_EVALUATOR_H_
