#ifndef LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_OBSTACLE_VELOCITY_EVALUATOR_H_
#define LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_OBSTACLE_VELOCITY_EVALUATOR_H_

#include "trajectory_evaluator.h"

#include <cmath>
#include "geometry_msgs/msg/pose_stamped.hpp"

class ObstacleDistanceEvaluator : public TrajectoryEvaluator
{
 public:
	ObstacleDistanceEvaluator() = default;
	explicit ObstacleDistanceEvaluator(rclcpp::Logger &logger);

	std::optional<double> evaluateTrajectory(const Trajectory &traj) override;
};

#endif //LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_OBSTACLE_VELOCITY_EVALUATOR_H_
