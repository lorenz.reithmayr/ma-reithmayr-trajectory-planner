#ifndef LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_GOAL_DISTANCE_EVALUATOR_H_
#define LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_GOAL_DISTANCE_EVALUATOR_H_

#include "trajectory_evaluator.h"

#include "geometry_msgs/msg/pose_stamped.hpp"

class GoalDistanceEvaluator : public TrajectoryEvaluator
{
 public:
	GoalDistanceEvaluator() = default;
	GoalDistanceEvaluator(rclcpp::Logger &logger);

	std::optional<double> evaluateTrajectory(const Trajectory &traj) override;
};

#endif //LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_GOAL_DISTANCE_EVALUATOR_H_

