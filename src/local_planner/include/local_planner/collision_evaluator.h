#ifndef LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_COLLISION_EVALUATOR_H_
#define LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_COLLISION_EVALUATOR_H_

#include "trajectory_evaluator.h"

class CollisionEvaluator : public TrajectoryEvaluator
{
 public:
	explicit CollisionEvaluator(rclcpp::Logger &logger, std::shared_ptr<CollisionChecker> &collision_checker, Footprint footprint, Footprint vessel_fp);

	std::optional<double> evaluateTrajectory(const Trajectory &traj) override;

 private:
	std::shared_ptr<CollisionChecker> collision_checker_;
	Footprint wamv_fp_;
	Footprint vessel_fp_;
};

#endif //LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_COLLISION_EVALUATOR_H_
