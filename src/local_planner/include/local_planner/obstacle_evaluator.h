#ifndef LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_OBSTACLE_EVALUATOR_H_
#define LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_OBSTACLE_EVALUATOR_H_

#include "trajectory_evaluator.h"
#include "nav2_costmap_2d/footprint_collision_checker.hpp"

class ObstacleEvaluator : public TrajectoryEvaluator
{
 public:
	ObstacleEvaluator() = default;

	/**
	 * The obstacle evaluation criterion scores a trajectory based on wheter it stays clear of lethal cells.
	 * The trajectory is traversed pose-by-pose and the cost of each cell added up. The resulting score is returned, if no obstacle cell
	 * has been intersected. Otherwise "empty" is returned, signifying an illegal trajectory.
	 * @param logger
	 * @param costmap
	 */
	ObstacleEvaluator(rclcpp::Logger &logger, std::shared_ptr<nav2_costmap_2d::FootprintCollisionChecker<std::shared_ptr<nav2_costmap_2d::Costmap2D>>> &footprint_collision_checker_,
		nav2_costmap_2d::Footprint &footprint);

	std::optional<double> evaluateTrajectory(const Trajectory &traj) override;
	static bool isValidCost(const unsigned char cost);

 private:
	std::shared_ptr<nav2_costmap_2d::FootprintCollisionChecker<std::shared_ptr<nav2_costmap_2d::Costmap2D>>>
		footprint_collision_checker_;
	nav2_costmap_2d::Footprint footprint_;
};
#endif //LOCAL_PLANNER_SRC_LOCAL_PLANNER_SRC_OBSTACLE_EVALUATOR_H_
