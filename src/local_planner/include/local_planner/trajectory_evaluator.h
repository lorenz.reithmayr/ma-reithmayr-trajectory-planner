#ifndef LOCAL_PLANNER_SRC_LOCAL_PLANNER_INCLUDE_LOCAL_PLANNER_TRAJECTORY_EVALUATOR_H_
#define LOCAL_PLANNER_SRC_LOCAL_PLANNER_INCLUDE_LOCAL_PLANNER_TRAJECTORY_EVALUATOR_H_

#include "rclcpp/rclcpp.hpp"
#include "nav2_costmap_2d/costmap_2d.hpp"
#include "custom_msgs/msg/trajectory2_d.hpp"
#include "nav2_costmap_2d/cost_values.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/path.hpp"
#include "collision_checker.h"

typedef custom_msgs::msg::Trajectory2D Trajectory;

class TrajectoryEvaluator
{
 public:
	virtual ~TrajectoryEvaluator() = default;

	[[nodiscard]] std::string getName() const
	{
		return name_;
	}

	void setLogger(rclcpp::Logger &parent_logger)
	{
		logger_ = parent_logger;
	}

	void setCostmap(std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap)
	{
		costmap_ = costmap;
	}

	void setObstacleTrajectory(const Trajectory &t)
	{
		obstacle_trajectory_ = t;
	}

	void setGoal(const geometry_msgs::msg::PoseStamped &goal)
	{
		goal_ = goal;
	}

	void setPath(const nav_msgs::msg::Path &path)
	{
		path_ = path;
	}

	void setWeight(uint16_t weight)
	{
		sigma_ = weight;
	}

	virtual std::optional<double> evaluateTrajectory(const Trajectory &traj) = 0;

	inline static double computeEuclidianDistance(double x1, double y1, double x2, double y2)
	{
		return sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
	};

	rclcpp::Logger logger_{ rclcpp::get_logger("LocalPlanner") };
	std::shared_ptr<nav2_costmap_2d::Costmap2D> costmap_;
	std::string name_;
	Trajectory obstacle_trajectory_;
	geometry_msgs::msg::PoseStamped goal_;
	nav_msgs::msg::Path path_;

	uint16_t sigma_{1};
};

#endif //LOCAL_PLANNER_SRC_LOCAL_PLANNER_INCLUDE_LOCAL_PLANNER_TRAJECTORY_EVALUATOR_H_
