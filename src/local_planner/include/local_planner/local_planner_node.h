#ifndef LOCAL_PLANNER_LOCAL_PLANNER_H_
#define LOCAL_PLANNER_LOCAL_PLANNER_H_

#include <chrono>
#include <thread>
#include <Eigen/Dense>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp/logger.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include "nav2_costmap_2d/costmap_2d.hpp"
#include "nav2_costmap_2d/cost_values.hpp"
#include "nav2_costmap_2d/footprint.hpp"
#include "collision_checker.h"
#include "collision_avoidance_planner.h"
#include "trajectory_evaluator.h"
#include "collision_evaluator.h"
#include "obstacle_evaluator.h"
#include "path_distance_evaluator.h"
#include "goal_distance_evaluator.h"
#include "forward_evaluator.h"
#include "obstacle_distance_evaluator.h"

#include "nav2_msgs/msg/costmap.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"
#include "nav_2d_msgs/msg/pose2_d_stamped.hpp"
#include "nav_2d_msgs/msg/twist2_d_stamped.hpp"
#include "custom_msgs/msg/trajectory2_d.hpp"
#include "custom_msgs/msg/trajectory2_d_array.hpp"
#include "geometry_msgs/msg/polygon_stamped.hpp"
#include "geometry_msgs/msg/pose2_d.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "custom_interfaces/srv/send_escape_trajectories.hpp"
#include "custom_interfaces/action/execute_trajectory.hpp"
#include "std_msgs/msg/bool.hpp"
#include "nav_msgs/msg/path.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "geometry_msgs/msg/point.hpp"
#include "geometry_msgs/msg/polygon_stamped.hpp"
#include "custom_msgs/msg/radar_data.hpp"
#include "custom_msgs/msg/radar_returns.hpp"
#include "std_msgs/msg/int32.hpp"

class LocalPlanner : public rclcpp::Node
{
 public:
	LocalPlanner();

	void costmapCallback(const nav2_msgs::msg::Costmap::SharedPtr msg);
	void wamvPoseCallback(const geometry_msgs::msg::PoseStamped &pose_msg);
	void wamvVelNEDCallback(const nav_2d_msgs::msg::Twist2DStamped &vel_msg);
	void trackVectorCallback(const custom_msgs::msg::Trajectory2D &track_vector_msg);
	void goalCallback(const geometry_msgs::msg::PoseStamped &goal_msg);
	void globalPathCallback(const nav_msgs::msg::Path &global_path_msg);
	void radarCallback(const custom_msgs::msg::RadarReturns &radar_returns_msg);
	void eventLoop();

 private:
	void declareParameters();
	void getParameters();
	void setupSubscriptions();
	void setupPublishers();
	void loadEvaluators();
	void costmapMsgToCostmap2D();
	void findClosestVessel();
	void executeDWA();
	void replan();
	void terminateDWAManeuver();
	bool requestTrajectories();
	void evaluateTrajectories();
	std::optional<double> scoreTrajectory(Trajectory &t);
	Trajectory selectBestLegalTrajectory();
	void sendVelocityCommand(Trajectory &t);
	void sendVelocityCommand(const double u, const double v, const double r);
	void sendStopCommand();
	void sendPath(Trajectory &t);
	void resetTrajectories();
	bool checkGoalReached(const double goal_x,
		const double goal_y,
		const double radius,
		const bool publish_distance) const;
	void publishTrajectoryMarker(Trajectory &t);
	void toggleTrajectoryFollowing(bool val);
	void activate();
	void deactivate();
	std::optional<Trajectory> getClosestTrajectory();
	void computeRelativeVelocity();

 private:
	rclcpp::Logger logger_{ rclcpp::get_logger("LocalPlanner") };
	std::shared_ptr<rclcpp::Clock> clock_;
	rclcpp::TimerBase::SharedPtr timer_;

	rclcpp::Subscription<nav2_msgs::msg::Costmap>::SharedPtr costmap_sub_;
	rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr pose_sub_;
	rclcpp::Subscription<nav_2d_msgs::msg::Pose2DStamped>::SharedPtr pose2d_sub_;
	rclcpp::Subscription<custom_msgs::msg::Trajectory2D>::SharedPtr track_vector_sub_;
	rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr goal_sub_;
	rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr global_path_sub_;
	rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr test_sub_;
	rclcpp::Subscription<custom_msgs::msg::Trajectory2D>::SharedPtr vessel1_trajectory_sub_;
	rclcpp::Subscription<custom_msgs::msg::RadarReturns>::SharedPtr radar_sub_;
	rclcpp::Subscription<nav_2d_msgs::msg::Twist2DStamped>::SharedPtr wamv_vel_sub_;
	rclcpp::Client<custom_interfaces::srv::SendEscapeTrajectories>::SharedPtr escape_traj_client_;
	rclcpp::CallbackGroup::SharedPtr client_cb_group_;
	rclcpp::CallbackGroup::SharedPtr timer_cb_group_;

	rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr replanned_path_pub_;
	rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr dwa_path_pub_;
	rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr cmd_vel_pub_;
	rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr los_active_pub_;
	rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr local_planner_active_pub_;
	rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr traj_marker_pub_;
	rclcpp::Publisher<geometry_msgs::msg::PolygonStamped>::SharedPtr collision_marker_pub_;
	rclcpp::Publisher<nav_2d_msgs::msg::Twist2DStamped>::SharedPtr rel_vel_pub_;
	rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr closest_pub_;

	nav2_msgs::msg::Costmap::SharedPtr costmap_msg_;
	// Orientation as quaternion
	geometry_msgs::msg::PoseStamped current_pose_;
	// Orientation in rad as Euler angle (theta)
	nav_2d_msgs::msg::Pose2DStamped current_pose_3dof_;
	nav_2d_msgs::msg::Twist2DStamped current_vel_ned_3dof_;
	custom_msgs::msg::Trajectory2D track_vector_;
	custom_msgs::msg::Trajectory2DArray escape_trajectories_;
	std_msgs::msg::Bool local_planner_active_flag_;
	std_msgs::msg::Bool los_active_flag_;
	nav_msgs::msg::Odometry cmd_vel_;

	std::shared_ptr<nav2_costmap_2d::Costmap2D> costmap_;
	nav_msgs::msg::Path global_path_;
	nav_msgs::msg::Path replanned_path_;
	nav_msgs::msg::Path dwa_trajectory_path_;
	std::string footprint_str_;
	Footprint footprint_;
	double footprint_padding_{ 0 };
	std::string vessel_fp_str_{ "[[-10.50, 5.25],[10.50, 5.25],[10.50, -5.25],[-10.50, -5.25]]" };
	Footprint vessel_fp_;
	geometry_msgs::msg::PoseStamped goal_;
	std::vector<std::pair<double, Trajectory>> legal_trajectories_;
	std::vector<Trajectory> illegal_trajectories_;
	geometry_msgs::msg::Pose2D current_trajectory_goal_;
	double dt_{ 0.5 };
	double t_col_thresh_{ 10.0 };
	std::vector<custom_msgs::msg::RadarData> radar_returns_;
	int32_t closest_vessel_idx_{ -1 };
	Trajectory closest_trajectory_;
	Eigen::Vector3d relative_vel_;
	Trajectory selected_trajectory_;
	std::chrono::time_point<std::chrono::steady_clock> timer_start_;
	std::chrono::duration<double> trajectory_duration_;

	std::shared_ptr<nav2_costmap_2d::FootprintCollisionChecker<std::shared_ptr<nav2_costmap_2d::Costmap2D>>>
		footprint_collision_checker_;
	std::shared_ptr<CollisionChecker> collision_checker_;
	std::unique_ptr<CollisionAvoidancePlanner> collision_avoidance_planner_;

	std::vector<std::shared_ptr<TrajectoryEvaluator>> evaluators_;

	nav_2d_msgs::msg::Twist2DStamped rel_vel_msg_;

	bool GOAL_RCVD_{ false };
	bool PATH_RCVD_{ false };
	bool ESCAPE_IN_PROGRESS_{ false };
	bool USE_LOS_GUIDANCE{ true };
};

#endif //LOCAL_PLANNER_LOCAL_PLANNER_H_
