#include "local_planner/collision_evaluator.h"
CollisionEvaluator::CollisionEvaluator(rclcpp::Logger &logger, std::shared_ptr<CollisionChecker> &collision_checker, Footprint footprint, Footprint vessel_fp)
	: collision_checker_(collision_checker), wamv_fp_(footprint), vessel_fp_(vessel_fp)
{
	logger_ = logger;
	name_ = "Collision Evaluator";
}

std::optional<double> CollisionEvaluator::evaluateTrajectory(const Trajectory &traj)
{
	assert(!obstacle_trajectory_.poses.empty());
	if (collision_checker_->predictCollision(traj, obstacle_trajectory_, wamv_fp_, vessel_fp_))
	{
		return {};
	}
	return std::optional<double>(0.0);
}
