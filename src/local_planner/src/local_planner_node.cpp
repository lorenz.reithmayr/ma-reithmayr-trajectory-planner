#include "local_planner/local_planner_node.h"

using namespace std::chrono_literals;

LocalPlanner::LocalPlanner() : rclcpp::Node("local_planner"),
							   costmap_msg_(nullptr),
							   costmap_(nullptr)
{
	logger_ = this->get_logger();
	clock_ = this->get_clock();

	declareParameters();
	getParameters();
	setupSubscriptions();
	setupPublishers();

	timer_ = this->create_wall_timer(0010ms, std::bind(&LocalPlanner::eventLoop, this), timer_cb_group_);

	// Construct a Footprint object from the footprint string defined in the config file
	if (!nav2_costmap_2d::makeFootprintFromString(footprint_str_, footprint_))
	{
		RCLCPP_ERROR(logger_, "Footprint creation from config file failed!");
		return;
	}
	nav2_costmap_2d::padFootprint(footprint_, footprint_padding_);

	if (!nav2_costmap_2d::makeFootprintFromString(vessel_fp_str_, vessel_fp_))
	{
		RCLCPP_ERROR(logger_, "Vessel footprint creation failed!");
		return;
	}

	footprint_collision_checker_ = std::make_shared<nav2_costmap_2d::FootprintCollisionChecker<std::shared_ptr<
		nav2_costmap_2d::Costmap2D>>>(costmap_);
	collision_checker_ = std::make_shared<CollisionChecker>(logger_,
		costmap_,
		footprint_,
		footprint_collision_checker_,
		collision_marker_pub_,
		dt_);

	std::vector<rclcpp::Parameter> ca_planner_params;
	ca_planner_params.push_back(this->get_parameter("allow_unknown"));
	ca_planner_params.push_back(this->get_parameter("how_many_corners"));
	ca_planner_params.push_back(this->get_parameter("w_euc_cost"));
	ca_planner_params.push_back(this->get_parameter("w_traversal_cost"));
	collision_avoidance_planner_ = std::make_unique<CollisionAvoidancePlanner>(logger_, costmap_, ca_planner_params);

	loadEvaluators();
	sendStopCommand();
	toggleTrajectoryFollowing(false);
	deactivate();
}

void LocalPlanner::declareParameters()
{
	this->declare_parameter("footprint", rclcpp::ParameterValue(""));
	this->declare_parameter("footprint_padding", rclcpp::ParameterValue(0.0));
	this->declare_parameter("dt", rclcpp::ParameterValue(0.5));
	this->declare_parameter("t_col_thresh", rclcpp::ParameterValue(10.0));
	this->declare_parameter("allow_unknown", rclcpp::ParameterValue(false));
	this->declare_parameter("how_many_corners", rclcpp::ParameterValue(8));
	this->declare_parameter("w_euc_cost", rclcpp::ParameterValue(1.0));
	this->declare_parameter("w_traversal_cost", rclcpp::ParameterValue(2.0));
	this->declare_parameter("sigma_obstacle", rclcpp::ParameterValue(1));
	this->declare_parameter("sigma_obst_distance", rclcpp::ParameterValue(1));
	this->declare_parameter("sigma_goal_dist", rclcpp::ParameterValue(1));
	this->declare_parameter("sigma_path_dist", rclcpp::ParameterValue(1));
	this->declare_parameter("sigma_forward", rclcpp::ParameterValue(1));
	this->declare_parameter("max_cmd_yaw_vel", rclcpp::ParameterValue(0.2));
	this->declare_parameter("los_guidance", rclcpp::ParameterValue(true));
}

void LocalPlanner::getParameters()
{
	this->get_parameter("footprint", footprint_str_);
	this->get_parameter("footprint_padding", footprint_padding_);
	this->get_parameter("dt", dt_);
	this->get_parameter("t_col_thresh", t_col_thresh_);
	this->get_parameter("los_guidance", USE_LOS_GUIDANCE);
}

void LocalPlanner::setupSubscriptions()
{
	client_cb_group_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
	timer_cb_group_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);

	escape_traj_client_ = this
		->create_client<custom_interfaces::srv::SendEscapeTrajectories>("escape_trajectories_service",
			rmw_qos_profile_default,
			client_cb_group_);

	costmap_sub_ = this->create_subscription<nav2_msgs::msg::Costmap>("/global_costmap/costmap_raw",
		rclcpp::QoS(10),
		std::bind(&LocalPlanner::costmapCallback, this, std::placeholders::_1));

	pose_sub_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
		"/wamv/pose_stamped",
		rclcpp::QoS(10),
		std::bind(&LocalPlanner::wamvPoseCallback, this, std::placeholders::_1));

	track_vector_sub_ = this->create_subscription<custom_msgs::msg::Trajectory2D>(
		"/wamv/track_vector",
		rclcpp::QoS(10),
		std::bind(&LocalPlanner::trackVectorCallback, this, std::placeholders::_1));

	goal_sub_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
		"/goal_pose",
		rclcpp::QoS(10),
		std::bind(&LocalPlanner::goalCallback, this, std::placeholders::_1));

	global_path_sub_ = this->create_subscription<nav_msgs::msg::Path>(
		"/global_path",
		rclcpp::QoS(10),
		std::bind(&LocalPlanner::globalPathCallback, this, std::placeholders::_1));

	radar_sub_ = this->create_subscription<custom_msgs::msg::RadarReturns>("/radar_returns",
		rclcpp::QoS(10),
		std::bind(&LocalPlanner::radarCallback, this, std::placeholders::_1));

	wamv_vel_sub_ = this->create_subscription<nav_2d_msgs::msg::Twist2DStamped>("/wamv/velocity_3dof_ned",
		rclcpp::QoS(10),
		std::bind(&LocalPlanner::wamvVelNEDCallback, this, std::placeholders::_1));

}

void LocalPlanner::setupPublishers()
{
	replanned_path_pub_ = this->create_publisher<nav_msgs::msg::Path>("/replanned_path", 10);
	dwa_path_pub_ = this->create_publisher<nav_msgs::msg::Path>("/dwa_path", 10);
	local_planner_active_pub_ = this->create_publisher<std_msgs::msg::Bool>("/local_planner_active", 10);
	los_active_pub_ = this->create_publisher<std_msgs::msg::Bool>("/wamv/los_active", 10);
	traj_marker_pub_ = this->create_publisher<visualization_msgs::msg::Marker>("/dwa_traj_marker", 10);
	collision_marker_pub_ = this->create_publisher<geometry_msgs::msg::PolygonStamped>("/collision_point_marker", 10);
	rel_vel_pub_ = this->create_publisher<nav_2d_msgs::msg::Twist2DStamped>("/relative_velocity", 10);
	closest_pub_ = this->create_publisher<std_msgs::msg::Int32>("/closest_vessel_index", 10);
	cmd_vel_pub_ = this->create_publisher<nav_msgs::msg::Odometry>("/wamv/cmd_vel_3dof", 10);
}

void LocalPlanner::loadEvaluators()
{
	auto collision_evaluator = std::make_shared<CollisionEvaluator>(logger_, collision_checker_, footprint_, vessel_fp_);
	evaluators_.push_back(collision_evaluator);

	auto goal_dist_evaluator = std::make_shared<GoalDistanceEvaluator>(logger_);
	goal_dist_evaluator->setWeight(this->get_parameter("sigma_goal_dist").as_int());
	evaluators_.push_back(goal_dist_evaluator);

	auto obstacle_distance_evaluator = std::make_shared<ObstacleDistanceEvaluator>(logger_);
	obstacle_distance_evaluator->setWeight(this->get_parameter("sigma_obst_distance").as_int());
	evaluators_.push_back(obstacle_distance_evaluator);

	auto forward_evaluator = std::make_shared<ForwardEvaluator>(logger_);
	forward_evaluator->setWeight(this->get_parameter("sigma_forward").as_int());
	forward_evaluator->setMaxYawVel(this->get_parameter("max_cmd_yaw_vel").as_double());
	evaluators_.push_back(forward_evaluator);

	/**
	auto obstacle_evaluator = std::make_shared<ObstacleEvaluator>(logger_,
		footprint_collision_checker_,
		footprint_);
	obstacle_evaluator->setWeight(this->get_parameter("sigma_obstacle").as_int());
	evaluators_.push_back(obstacle_evaluator);

	auto path_dist_evaluator = std::make_shared<PathDistanceEvaluator>(logger_);
	obstacle_evaluator->setWeight(this->get_parameter("sigma_path_dist").as_int());
	evaluators_.push_back(path_dist_evaluator);
	 **/
}

void LocalPlanner::costmapCallback(const nav2_msgs::msg::Costmap::SharedPtr msg) // NOLINT(*-unnecessary-value-param)
{
	costmap_msg_ = msg;
	costmapMsgToCostmap2D();
	collision_checker_->setCostmap(costmap_);
	collision_avoidance_planner_->setCostmap(costmap_);
}

void LocalPlanner::wamvPoseCallback(const geometry_msgs::msg::PoseStamped &pose_msg)
{
	current_pose_ = pose_msg;
}

void LocalPlanner::wamvVelNEDCallback(const nav_2d_msgs::msg::Twist2DStamped &vel_msg)
{
	current_vel_ned_3dof_ = vel_msg;
}

void LocalPlanner::trackVectorCallback(const custom_msgs::msg::Trajectory2D &track_vector_msg)
{
	track_vector_ = track_vector_msg;
}

void LocalPlanner::radarCallback(const custom_msgs::msg::RadarReturns &radar_returns_msg)
{
	radar_returns_ = radar_returns_msg.returns;
}

void LocalPlanner::goalCallback(const geometry_msgs::msg::PoseStamped &goal_msg)
{
	goal_ = goal_msg;
	GOAL_RCVD_ = true;
	ESCAPE_IN_PROGRESS_ = false;
}

void LocalPlanner::globalPathCallback(const nav_msgs::msg::Path &global_path_msg)
{
	global_path_ = global_path_msg;
	PATH_RCVD_ = true;
}

void LocalPlanner::eventLoop()
{
	findClosestVessel();
	if (costmap_ == nullptr || !PATH_RCVD_)
	{
		return;
	}
	if (checkGoalReached(goal_.pose.position.x, goal_.pose.position.y, 5.0, false))
	{
		GOAL_RCVD_ = false;
		PATH_RCVD_ = false;
		toggleTrajectoryFollowing(false);
	}

	computeRelativeVelocity();

	if (ESCAPE_IN_PROGRESS_)
	{
		sendVelocityCommand(selected_trajectory_);
		const auto t = std::chrono::steady_clock::now();
		const std::chrono::duration<double> d = t - timer_start_;
		RCLCPP_INFO_THROTTLE(logger_,
			*clock_,
			1000,
			"Time on Trajectory: %f	Time: %f",
			d.count(),
			trajectory_duration_.count());
		if (!USE_LOS_GUIDANCE && d >= trajectory_duration_)
		{
			RCLCPP_WARN(logger_, "Time limit has been reached!");
			terminateDWAManeuver();
			return;
		}
		if (checkGoalReached(current_trajectory_goal_.x, current_trajectory_goal_.y, 3.0, true))
		{
			RCLCPP_WARN(logger_, "Trajectory completed!");
			terminateDWAManeuver();
			return;
		}

		return;
	}

	sendVelocityCommand(4.0, 0.0, 0.0);

	if (!radar_returns_.empty())
	{
		if (collision_checker_->predictCollision(track_vector_, getClosestTrajectory().value(), footprint_, vessel_fp_))
		{
			auto ttc = collision_checker_->getTimeToCollision();
			if (ttc <= t_col_thresh_)
			{
				RCLCPP_WARN(logger_, "Collision detected in %f sec", ttc);
				executeDWA();
			}
		}

	}
}

void LocalPlanner::executeDWA()
{
	if (!USE_LOS_GUIDANCE)
	{
		toggleTrajectoryFollowing(true);
		RCLCPP_WARN(logger_, "Toggled tf");
	}
	if (requestTrajectories())
	{
		evaluateTrajectories();
		if (legal_trajectories_.empty())
		{
			RCLCPP_ERROR(logger_, "No legal trajectories found!");

			sendStopCommand();
			toggleTrajectoryFollowing(false);
			replan();
			resetTrajectories();
			return;
		}
		selected_trajectory_ = selectBestLegalTrajectory();
		sendVelocityCommand(selected_trajectory_);
		RCLCPP_WARN(logger_,
			"Sent trajectory velocity command: %f, %f, %f",
			selected_trajectory_.cmd_vel.linear.x,
			selected_trajectory_.cmd_vel.linear.y,
			selected_trajectory_.cmd_vel.angular.z);
		sendPath(selected_trajectory_);
		trajectory_duration_ = std::chrono::duration<double>(
			selected_trajectory_.poses.size() * selected_trajectory_.dt);
		timer_start_ = std::chrono::steady_clock::now();
		current_trajectory_goal_ = selected_trajectory_.poses.back();
		ESCAPE_IN_PROGRESS_ = true;
		publishTrajectoryMarker(selected_trajectory_);
		resetTrajectories();
		return;
	}
	else
	{
		sendStopCommand();
		resetTrajectories();
		return;
	}
}

void LocalPlanner::replan()
{
	collision_avoidance_planner_->setCostmap(costmap_);
	collision_avoidance_planner_->initPlan(current_pose_, goal_);
	replanned_path_ = collision_avoidance_planner_->getPlan();
	if (replanned_path_.poses.empty())
	{
		RCLCPP_ERROR(logger_, "No valid path was found!");
		return;
	}
	replanned_path_pub_->publish(replanned_path_);
	replanned_path_.poses.clear();
}

void LocalPlanner::terminateDWAManeuver()
{
	toggleTrajectoryFollowing(false);
	ESCAPE_IN_PROGRESS_ = false;
	replan();
}

bool LocalPlanner::requestTrajectories()
{
	RCLCPP_WARN(logger_, "Requesting escape trajectories!");
	auto req = std::make_shared<custom_interfaces::srv::SendEscapeTrajectories::Request>();
	auto res = escape_traj_client_->async_send_request(req).share();
	std::future_status status = res.wait_for(10s);
	if (status == std::future_status::ready)
	{
		if (res.get()->success)
		{
			RCLCPP_WARN(logger_, "Request for escape trajectories successful!");
			escape_trajectories_ = res.get()->trajectories;
			return true;
		}
		else
		{
			RCLCPP_ERROR(logger_, "Request for escape trajectories failed!");
			return false;
		}
	}
	return false;
}

void LocalPlanner::evaluateTrajectories()
{
	for (auto &t : escape_trajectories_.trajectories)
	{
		RCLCPP_WARN(logger_, "======= START ========");
		// TODO: Consider optimizing the scoring by ending early if a trajectory scores higher than the lowest so far
		std::optional<double> score = scoreTrajectory(t);
		if (score.has_value())
		{
			RCLCPP_INFO(logger_, "Total Score: %f", score.value());
			std::pair<double, Trajectory> scored_traj = std::make_pair(score.value(), t);
			legal_trajectories_.push_back(scored_traj);
		}
		RCLCPP_WARN(logger_, "======= END ========");
	}
}

std::optional<double> LocalPlanner::scoreTrajectory(Trajectory &t)
{
	double total_score = 0.0;
	for (const auto &e : evaluators_)
	{
		e->setCostmap(costmap_);
		e->setGoal(goal_);
		e->setPath(global_path_);
		e->setObstacleTrajectory(getClosestTrajectory().value());
		std::optional<double> score = e->evaluateTrajectory(t);
		if (score.has_value())
		{
			RCLCPP_WARN(logger_, "%s: %f", e->getName().c_str(), score.value());
			total_score += score.value();
		}
		else
		{
			illegal_trajectories_.push_back(t);
			RCLCPP_WARN(logger_, "%s: Declared illegal!", e->getName().c_str());
			return {};
		}
	}
	return total_score;
}

Trajectory LocalPlanner::selectBestLegalTrajectory()
{
	auto res = *std::min_element(legal_trajectories_.cbegin(),
		legal_trajectories_.cend(),
		[](const auto &lhs, const auto &rhs)
		{
		  return lhs.first < rhs.first;
		});

	Trajectory t = res.second;
	RCLCPP_INFO(logger_, "Winning trajectory scored: %f", res.first);
	return t;
}

void LocalPlanner::sendVelocityCommand(Trajectory &t)
{
	cmd_vel_.twist.twist = t.cmd_vel;
	cmd_vel_pub_->publish(cmd_vel_);
}

void LocalPlanner::sendVelocityCommand(const double u, const double v, const double r)
{
	cmd_vel_.twist.twist.linear.x = u;
	cmd_vel_.twist.twist.linear.y = v;
	cmd_vel_.twist.twist.angular.z = r;
	cmd_vel_pub_->publish(cmd_vel_);
}

void LocalPlanner::sendStopCommand()
{
	sendVelocityCommand(0.0, 0.0, 0.0);
}

void LocalPlanner::sendPath(Trajectory &t)
{
	dwa_trajectory_path_.header.frame_id = "map";
	assert(!t.poses.empty());
	for (const auto &pose : t.poses)
	{
		geometry_msgs::msg::PoseStamped p;
		p.pose.position.x = pose.x;
		p.pose.position.y = pose.y;
		p.pose.position.z = 0.0;
		dwa_trajectory_path_.poses.push_back(p);
	}
	dwa_path_pub_->publish(dwa_trajectory_path_);
	dwa_trajectory_path_.poses.clear();
}

void LocalPlanner::resetTrajectories()
{
	legal_trajectories_.clear();
	illegal_trajectories_.clear();
}

bool LocalPlanner::checkGoalReached(const double goal_x,
	const double goal_y,
	const double radius,
	const bool publish_distance) const
{
	auto dist_to_goal = TrajectoryEvaluator::computeEuclidianDistance(
		current_pose_.pose.position.x, current_pose_.pose.position.y, goal_x, goal_y);
	if (publish_distance)
	{
		RCLCPP_INFO_THROTTLE(logger_, *clock_, 1000, "Distance to goal: %f", dist_to_goal);
	}
	return (dist_to_goal < radius);
}

void LocalPlanner::costmapMsgToCostmap2D()
{
	if (costmap_ == nullptr)
	{
		costmap_ = std::make_shared<nav2_costmap_2d::Costmap2D>(
			costmap_msg_->metadata.size_x, costmap_msg_->metadata.size_y,
			costmap_msg_->metadata.resolution, costmap_msg_->metadata.origin.position.x,
			costmap_msg_->metadata.origin.position.y);
	}
	else if (costmap_->getSizeInCellsX() != costmap_msg_->metadata.size_x ||
		costmap_->getSizeInCellsY() != costmap_msg_->metadata.size_y ||
		costmap_->getResolution() != costmap_msg_->metadata.resolution ||
		costmap_->getOriginX() != costmap_msg_->metadata.origin.position.x ||
		costmap_->getOriginY() != costmap_msg_->metadata.origin.position.y)
	{
		// Update the size of the costmap
		costmap_->resizeMap(
			costmap_msg_->metadata.size_x, costmap_msg_->metadata.size_y,
			costmap_msg_->metadata.resolution,
			costmap_msg_->metadata.origin.position.x,
			costmap_msg_->metadata.origin.position.y);
	}

	unsigned char *master_array = costmap_->getCharMap();
	unsigned int index = 0;
	for (unsigned int i = 0; i < costmap_msg_->metadata.size_x; ++i)
	{
		for (unsigned int j = 0; j < costmap_msg_->metadata.size_y; ++j)
		{
			master_array[index] = costmap_msg_->data[index];
			++index;
		}
	}
}

void LocalPlanner::publishTrajectoryMarker(Trajectory &t)
{
	visualization_msgs::msg::Marker m;
	m.type = visualization_msgs::msg::Marker::LINE_STRIP;
	m.header.frame_id = "map";
	m.action = 0;
	m.color.a = 1.0;
	m.color.r = 0.0;
	m.color.g = 1.0;
	m.color.b = 1.0;
	m.scale.x = 0.5;
	for (size_t i = 0; i < t.poses.size() - 1; ++i)
	{
		geometry_msgs::msg::Point p;
		p.x = t.poses.at(i).x;
		p.y = t.poses.at(i).y;
		p.z = 0.0;
		m.points.push_back(p);
	}
	traj_marker_pub_->publish(m);
}

void LocalPlanner::findClosestVessel()
{
	if (radar_returns_.empty())
	{
		return;
	}
	auto closest = DBL_MAX;
	size_t clst_idx = 0;
	for (size_t i = 0; i < radar_returns_.size(); ++i)
	{
		auto curr = radar_returns_[i].distance;
		if (curr < closest)
		{
			closest = curr;
			clst_idx = i;
		}
	}
	closest_vessel_idx_ = clst_idx;
	std_msgs::msg::Int32 msg;
	msg.data = closest_vessel_idx_;
	closest_pub_->publish(msg);
}

std::optional<Trajectory> LocalPlanner::getClosestTrajectory()
{
	if (closest_vessel_idx_ == -1)
	{
		RCLCPP_ERROR(logger_, "Closest vessel undetermined!");
		return {};
	}
	return radar_returns_.at(closest_vessel_idx_).trajectory;
}

void LocalPlanner::toggleTrajectoryFollowing(bool val)
{
	local_planner_active_flag_.data = val;
	los_active_flag_.data = !val;
	los_active_pub_->publish(los_active_flag_);
	local_planner_active_pub_->publish(local_planner_active_flag_);
}

void LocalPlanner::activate()
{
	local_planner_active_flag_.data = true;
	local_planner_active_pub_->publish(local_planner_active_flag_);
}

void LocalPlanner::deactivate()
{
	local_planner_active_flag_.data = false;
	local_planner_active_pub_->publish(local_planner_active_flag_);
}

void LocalPlanner::computeRelativeVelocity()
{
	if (closest_vessel_idx_ == -1)
	{
		return;
	}
	Eigen::Vector3d closest_vessel_vel(radar_returns_.at(closest_vessel_idx_).velocity.x,
		radar_returns_.at(closest_vessel_idx_).velocity.y,
		radar_returns_.at(closest_vessel_idx_).velocity.theta);
	Eigen::Vector3d wamv_vel(current_vel_ned_3dof_.velocity.x,
		current_vel_ned_3dof_.velocity.y,
		current_vel_ned_3dof_.velocity.theta);
	// v_A|B = v_A - v_B --> Velocity of A relative to B, i.e. the velocity of A in the frame where B is at rest
	relative_vel_ = wamv_vel - closest_vessel_vel;

	rel_vel_msg_.velocity.x = relative_vel_.x();
	rel_vel_msg_.velocity.y = relative_vel_.y();
	rel_vel_msg_.velocity.theta = relative_vel_.z();
	rel_vel_pub_->publish(rel_vel_msg_);
}
