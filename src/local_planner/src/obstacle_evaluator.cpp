#include "local_planner/obstacle_evaluator.h"

ObstacleEvaluator::ObstacleEvaluator(rclcpp::Logger &logger,
	std::shared_ptr<nav2_costmap_2d::FootprintCollisionChecker<std::shared_ptr<
		nav2_costmap_2d::Costmap2D>>> &footprint_collision_checker,
	nav2_costmap_2d::Footprint &footprint)
	: footprint_collision_checker_(footprint_collision_checker), footprint_(footprint)
{
	logger_ = logger;
	name_ = "Obstacle Evaluator";
}

std::optional<double> ObstacleEvaluator::evaluateTrajectory(const Trajectory &traj)
{
	footprint_collision_checker_->setCostmap(costmap_);
	double score = 0.0;
	for (const auto &pose : traj.poses)
	{
		auto cost = footprint_collision_checker_->footprintCostAtPose(pose.x, pose.y, pose.theta, footprint_);
		if (!isValidCost(cost))
		{
			return {};
		}
		score += cost;
	}
	return sigma_ * score;
}

bool ObstacleEvaluator::isValidCost(const unsigned char cost)
{
	return cost != nav2_costmap_2d::LETHAL_OBSTACLE &&
		cost != nav2_costmap_2d::INSCRIBED_INFLATED_OBSTACLE &&
		cost != nav2_costmap_2d::NO_INFORMATION;
}
