#include "local_planner/collision_checker.h"

CollisionChecker::CollisionChecker(rclcpp::Logger &parent_logger,
	std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap,
	Footprint &robot_footprint,
	std::shared_ptr<nav2_costmap_2d::FootprintCollisionChecker<std::shared_ptr<nav2_costmap_2d::Costmap2D>>>
	&footprint_collision_checker,
	rclcpp::Publisher<geometry_msgs::msg::PolygonStamped>::SharedPtr &collision_marker_pub,
	double dt)
	:
	logger_(parent_logger),
	costmap_(costmap),
	footprint_(robot_footprint),
	footprint_collision_checker_(footprint_collision_checker),
	collision_marker_pub_(collision_marker_pub),
	dt_(dt),
	time_to_collision_(DBL_MAX)
{
}

bool CollisionChecker::predictCollision(const Trajectory &t1,
	const Trajectory &t2,
	const Footprint &footprint1,
	const Footprint &footprint2)
{
	/*
	 * We need to check if the footprint of vessel1 and the footprint of vessel2 intersect at the same time on the
	 * trajectory. Time is encoded by position of the pose in the trajectory. So, in order to predict a positive
	 * collision, the footprints at poses at the same array position need to overlap.
	 */
	auto smaller_traj = (t1.poses.size() <= t2.poses.size()) ? t1 : t2;
	if (smaller_traj.poses.empty())
	{
		return false;
	}
	for (size_t i = 0; i < smaller_traj.poses.size(); ++i)
	{
		Footprint ofp1 = constructOrientedFootprint(t1.poses[i], footprint1);
		Footprint ofp2 = constructOrientedFootprint(t2.poses[i], footprint2);
		if (checkFootprintCollision(ofp1, ofp2))
		{
			time_to_collision_ = dt_ * (i - 1);
			// publishCollisionMarker(ofp1);
			return true;
		}
	}
	return false;
}

bool CollisionChecker::checkFootprintCollision(const Footprint &footprint1, const Footprint &footprint2)
{
	std::vector<MapLocation> cells_fp1 = getCellsInsideFootprint(footprint1);
	std::vector<MapLocation> cells_fp2 = getCellsInsideFootprint(footprint2);
	auto checkEqual = [](const MapLocation ml1, const MapLocation ml2)
	{ return (ml1.x == ml2.x && ml1.y == ml2.y); };

	for (const auto &c2 : cells_fp2)
	{
		for (const auto &c1 : cells_fp1)
		{
			if (checkEqual(c1, c2))
			{
				return true;
			}
		}
	}
	return false;
}

int CollisionChecker::checkPathCollision(const nav_msgs::msg::Path &path)
{
	if (costmap_ == nullptr)
	{
		return -1;
	}

	for (auto &pose : path.poses)
	{
		unsigned int mx = 0, my = 0;
		costmap_->worldToMap(pose.pose.position.x, pose.pose.position.y, mx, my);
		if (costmap_->getCost(mx, my) != nav2_costmap_2d::FREE_SPACE)
		{
			return 1;
		}
	}
	return 0;
}

void CollisionChecker::constructOrientedFootprint(const geometry_msgs::msg::Pose2D &pose)
{
	oriented_footprint_.clear();
	// Orient the constructed footprint along the predicted pose
	nav2_costmap_2d::transformFootprint(pose.x, pose.y, pose.theta, footprint_, oriented_footprint_);
}

Footprint CollisionChecker::constructOrientedFootprint(const geometry_msgs::msg::Pose2D &pose,
	const Footprint &footprint)
{
	Footprint oriented_footprint;
	nav2_costmap_2d::transformFootprint(pose.x, pose.y, pose.theta, footprint, oriented_footprint);
	return oriented_footprint;
}

std::vector<MapLocation> CollisionChecker::getCellsInsideFootprint(const Footprint &fp)
{
	MapLocation fp_pt_map_location;
	std::vector<MapLocation> fp_map_locations;
	for (const auto &pt : fp)
	{
		fp_pt_map_location.x = pt.x;
		fp_pt_map_location.y = pt.y;
		fp_map_locations.push_back(fp_pt_map_location);
	}
	std::vector<MapLocation> fp_cells;
	costmap_->convexFillCells(fp_map_locations, fp_cells);
	return fp_cells;
}

void CollisionChecker::setCostmap(const std::shared_ptr<nav2_costmap_2d::Costmap2D> &costmap)
{
	costmap_ = costmap;
	footprint_collision_checker_->setCostmap(costmap);
}

void CollisionChecker::publishCollisionMarker(const Footprint &fp)
{
	geometry_msgs::msg::PolygonStamped collision_fp_polygon;
	collision_fp_polygon.header.frame_id = "map";
	for (auto const &pt : fp)
	{
		geometry_msgs::msg::Point32 fp_pt32;
		fp_pt32.x = pt.x;
		fp_pt32.y = pt.y;
		fp_pt32.z = pt.z;
		collision_fp_polygon.polygon.points.push_back(fp_pt32);
	}
	collision_marker_pub_->publish(collision_fp_polygon);
}

double CollisionChecker::getTimeToCollision() const
{
	return time_to_collision_;
}
