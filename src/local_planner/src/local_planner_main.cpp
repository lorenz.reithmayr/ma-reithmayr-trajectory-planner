#include <memory>
#include "local_planner/local_planner_node.h"
#include "rclcpp/rclcpp.hpp"

int main(int argc, char *argv[])
{
	rclcpp::init(argc, argv);
	auto local_planner_node = std::make_shared<LocalPlanner>();
	rclcpp::executors::MultiThreadedExecutor executor;
	executor.add_node(local_planner_node);
	executor.spin();
	rclcpp::shutdown();
	return 0;
}
