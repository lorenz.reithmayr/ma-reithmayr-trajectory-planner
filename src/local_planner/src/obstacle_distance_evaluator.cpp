#include "local_planner/obstacle_distance_evaluator.h"

ObstacleDistanceEvaluator::ObstacleDistanceEvaluator(rclcpp::Logger &logger)
{
	logger_ = logger;
	name_ = "Obstacle Distance Evaluator";
}

std::optional<double> ObstacleDistanceEvaluator::evaluateTrajectory(const Trajectory &traj)
{
	assert(!obstacle_trajectory_.poses.empty());
	double summed_dist = 0.0;
	auto smaller_traj = (traj.poses.size() <= obstacle_trajectory_.poses.size()) ? traj : obstacle_trajectory_;
	for (size_t i = 0; i < smaller_traj.poses.size(); ++i)
	{
		auto weighted_dist = (i + 1) * computeEuclidianDistance(
			traj.poses[i].x,
			traj.poses[i].y,
			obstacle_trajectory_.poses[i].x,
			obstacle_trajectory_.poses[i].y);
		summed_dist += weighted_dist;
	}

	auto bonus = sigma_ * (-summed_dist);
	return bonus;
}
