#include "local_planner/goal_distance_evaluator.h"
GoalDistanceEvaluator::GoalDistanceEvaluator(rclcpp::Logger &logger)
{
	logger_ = logger;
	name_ = "Goal Distance Evaluator";
}

std::optional<double> GoalDistanceEvaluator::evaluateTrajectory(const Trajectory &traj)
{
	double total_dist = 0.0;
	double score = 0.0;
	for (const auto &pose : traj.poses) {
		total_dist += computeEuclidianDistance(pose.x, pose.y, goal_.pose.position.x, goal_.pose.position.y);
	}
	score = sigma_ * total_dist;
	return score;
}
