#include "local_planner/path_distance_evaluator.h"
PathDistanceEvaluator::PathDistanceEvaluator(rclcpp::Logger &logger)
{

	logger_ = logger;
	name_ = "Path Distance Evaluator";
}

std::optional<double> PathDistanceEvaluator::evaluateTrajectory(const Trajectory &traj)
{
	double total_dist = 0.0;
	double score = 0.0;
	for (const auto &pose : traj.poses)
	{
		for (const auto &wpt : path_.poses)
		{
			total_dist += computeEuclidianDistance(wpt.pose.position.x, wpt.pose.position.y, pose.x, pose.y);
		}
	}
	score = sigma_ * total_dist;
	return score;
}

nav_msgs::msg::Path PathDistanceEvaluator::getGlobalPathSlice(nav_msgs::msg::Path &global_path)
{
	return nav_msgs::msg::Path();
}
