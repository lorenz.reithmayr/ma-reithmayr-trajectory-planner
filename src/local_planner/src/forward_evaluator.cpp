#include "local_planner/forward_evaluator.h"
ForwardEvaluator::ForwardEvaluator(rclcpp::Logger &logger)
{
	logger_ = logger;
	name_ = "Forward Evaluator";
}

std::optional<double> ForwardEvaluator::evaluateTrajectory(const Trajectory &traj)
{
	if (traj.cmd_vel.linear.x < 0 || std::abs(traj.cmd_vel.angular.z) >= max_cmd_yaw_)
	{
		return {};
	}
	else
	{
		// Penalize turning too much
		auto penalty = sigma_ * std::abs(traj.cmd_vel.angular.z);
		return penalty;
	}
}

void ForwardEvaluator::setMaxYawVel(const double val)
{
	max_cmd_yaw_ = val;
}
