#! /bin/bash

ros2 launch vrx_gazebo generate_wamv.launch.py component_yaml:="$PWD"/custom_wamv_config.yaml wamv_target:=`pwd`/wamv_target.urdf wamv_locked:=False
