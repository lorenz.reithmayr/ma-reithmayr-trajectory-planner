from glob import glob

from setuptools import find_packages, setup

package_name = 'trajectory_planner'

setup(
    name=package_name,
    version='0.0.1',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
         ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # World files
        ('share/' + package_name + '/gz_files/world', glob('gz_files/world/*')),
        # Custom config files
        ('share/' + package_name + '/config', glob('config/*.yaml')),
        # Vessel mesh files
        ('share/' + package_name + '/gz_files/meshes', glob('gz_files/meshes/*')),
        # Map files
        ('share/' + package_name + '/map', glob('map/[!_]*')),
        # rviz2 config
        ('share/' + package_name + '/rviz2_config', glob('rviz2_config/rviz2_config.rviz')),
        # Custom WAMV files
        ('share/' + package_name + '/gz_files/custom_wamv', glob('gz_files/custom_wamv/*')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='lorenz',
    maintainer_email='lorenz.reithmayr@rwth-aachen.de',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'vessel_pose_publisher = trajectory_planner.vessel_pose_publisher:main',
            'vessel_model_publisher = trajectory_planner.vessel_model_publisher:main',
            'global_path_planner = trajectory_planner.global_path_planner:main',
            'map_handler = trajectory_planner.map_handler:main',
            'obstacle_tracker = trajectory_planner.obstacle_tracker:main',
            'wamv_odometry = trajectory_planner.wamv_odometry:main',
            'los_guidance = trajectory_planner.los_guidance:main',
            'visualization_module = trajectory_planner.visualization_module:main',
            'trajectory_generator = trajectory_planner.trajectory_generator:main',
            'radar = trajectory_planner.radar:main',
        ],
    },
)
