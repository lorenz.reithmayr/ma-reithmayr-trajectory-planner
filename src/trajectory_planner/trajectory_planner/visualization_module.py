import traceback

import rclpy
from nav_2d_msgs.msg import Twist2DStamped
from geometry_msgs.msg import PoseStamped, Point
from std_msgs.msg import Int32
from nav_msgs.msg import Path
from rclpy.node import Node
# noinspection PyUnresolvedReferences
from custom_msgs.msg import Trajectory2D, Trajectory2DArray, RadarReturns, RadarData
from visualization_msgs.msg import Marker, MarkerArray


class Visualizer(Node):
    def __init__(self):
        super().__init__("visualization_module")
        self.esc_trajectories = None
        self.track_vector = None
        self.vel_3dof_ned = None
        self.esc_trajectories_markers = MarkerArray()
        self.vel_vector_marker = Marker()
        self.vel_vector_start_point = Point()
        self.vel_vector_end_point = Point()
        self.dt = 3.0

        self.wamv_footprint = [[-2.50, 1.25], [2.50, 1.25], [2.50, -1.25], [-2.50, -1.25]]

        self.radar_returns = None

        self.rel_vel_vector = None
        self.rel_vel_vector_marker = Marker()
        self.rel_vel_vector_start_point = Point()
        self.rel_vel_vector_end_point = Point()

        self.closest_vessel_marker = Marker()

        self.track_vector_pub = self.create_publisher(Path, "visualizer/track_vector", 10)
        self.esc_markers_pub = self.create_publisher(MarkerArray, "visualizer/dwa_trajectories", 10)
        self.vel_vector_pub = self.create_publisher(Marker, "visualizer/vel_vector_3dof_ned", 10)
        self.rel_vel_vector_pub = self.create_publisher(Marker, "visualizer/rel_vel_vector", 10)
        self.closest_pub = self.create_publisher(Marker, "visualizer/closest_vessel", 10)

    def track_vector_callback(self, tv_msg: Trajectory2D):
        self.track_vector = tv_msg
        self.visualize_track_vector()

    def escape_trajectory_callback(self, esc_trajectory_msg: Trajectory2DArray):
        self.esc_trajectories = esc_trajectory_msg
        self.visualize_escape_trajectories()

    def velocity_ned_callback(self, vel_ned_msg):
        self.vel_3dof_ned = vel_ned_msg
        self.visualize_vel_3dof_ned()

    def relative_vel_callback(self, rel_vel_msg):
        self.rel_vel_vector = rel_vel_msg
        self.visualize_rel_vel()

    def radar_callback(self, radar_msg):
        self.radar_returns = radar_msg

    def closest_callback(self, closest_idx_msg):
        self.visualize_closest_vessel(closest_idx_msg)

    def visualize_closest_vessel(self, msg):
        if self.radar_returns is None:
            return
        self.closest_vessel_marker.type = Marker.CUBE
        self.closest_vessel_marker.header.frame_id = "map"
        self.closest_vessel_marker.action = 0
        self.closest_vessel_marker.color.a = 1.0
        self.closest_vessel_marker.color.r = 0.7
        self.closest_vessel_marker.color.g = 0.5
        self.closest_vessel_marker.color.b = 1.0
        self.closest_vessel_marker.scale.x = 4.0
        self.closest_vessel_marker.scale.y = 4.0
        self.closest_vessel_marker.scale.z = 4.0
        self.closest_vessel_marker.pose.position.x = self.radar_returns.returns[msg.data].pose.position.x
        self.closest_vessel_marker.pose.position.y = self.radar_returns.returns[msg.data].pose.position.y
        self.closest_vessel_marker.pose.position.z = 12.0
        self.closest_pub.publish(self.closest_vessel_marker)

    def visualize_track_vector(self):
        track_vector_path = Path()
        frame_id = self.track_vector.header.frame_id
        track_vector_path.header.frame_id = frame_id
        for pose in self.track_vector.poses:
            tv_path_pose = PoseStamped()
            tv_path_pose.header.frame_id = frame_id
            tv_path_pose.pose.position.x = pose.x
            tv_path_pose.pose.position.y = pose.y
            tv_path_pose.pose.position.z = 0.0
            track_vector_path.poses.append(tv_path_pose)
        self.track_vector_pub.publish(track_vector_path)

    def visualize_escape_trajectories(self):
        if self.esc_trajectories is None:
            return
        for i, t in enumerate(self.esc_trajectories.trajectories):
            esc_t_marker = Marker()
            esc_t_marker.type = Marker.LINE_STRIP
            esc_t_marker.header.frame_id = "map"
            esc_t_marker.id = i
            esc_t_marker.scale.x = 0.07
            esc_t_marker.action = 0
            esc_t_marker.color.a = 1.0
            esc_t_marker.color.r = 1.0
            esc_t_marker.color.g = 0.0
            esc_t_marker.color.b = 0.0
            for j in range(len(t.poses) - 1):
                point = Point()
                point.x = t.poses[j].x
                point.y = t.poses[j].y
                point.z = 0.0
                esc_t_marker.points.append(point)
            self.esc_trajectories_markers.markers.append(esc_t_marker)
        self.esc_markers_pub.publish(self.esc_trajectories_markers)
        self.esc_trajectories_markers.markers.clear()

    def visualize_vel_3dof_ned(self):
        if self.track_vector is None:
            return
        self.vel_vector_marker.type = Marker.ARROW
        self.vel_vector_marker.header.frame_id = "map"
        self.vel_vector_marker.action = 0
        self.vel_vector_marker.color.a = 1.0
        self.vel_vector_marker.color.r = 1.0
        self.vel_vector_marker.color.g = 0.0
        self.vel_vector_marker.color.b = 0.0
        self.vel_vector_marker.scale.x = 0.5  # Shaft diameter
        self.vel_vector_marker.scale.y = 1.0  # Arrow head width
        self.vel_vector_marker.scale.z = 1.5  # Arrow head length

        current_pose = self.track_vector.poses[0]

        self.vel_vector_start_point.x = current_pose.x
        self.vel_vector_start_point.y = current_pose.y
        self.vel_vector_start_point.z = 0.0
        self.vel_vector_end_point.x = current_pose.x + self.vel_3dof_ned.velocity.x * self.dt
        self.vel_vector_end_point.y = current_pose.y + self.vel_3dof_ned.velocity.y * self.dt
        self.vel_vector_end_point.z = 0.0
        self.vel_vector_marker.points.append(self.vel_vector_start_point)
        self.vel_vector_marker.points.append(self.vel_vector_end_point)
        self.vel_vector_pub.publish(self.vel_vector_marker)
        self.vel_vector_marker.points.clear()

    def visualize_rel_vel(self):
        if self.track_vector is None:
            return
        self.rel_vel_vector_marker.type = Marker.ARROW
        self.rel_vel_vector_marker.header.frame_id = "map"
        self.rel_vel_vector_marker.action = 0
        self.rel_vel_vector_marker.color.a = 1.0
        self.rel_vel_vector_marker.color.r = 1.0
        self.rel_vel_vector_marker.color.g = 1.0
        self.rel_vel_vector_marker.color.b = 0.0
        self.rel_vel_vector_marker.scale.x = 0.5  # Shaft diameter
        self.rel_vel_vector_marker.scale.y = 1.0  # Arrow head width
        self.rel_vel_vector_marker.scale.z = 1.5  # Arrow head length

        current_pose = self.track_vector.poses[0]

        self.rel_vel_vector_start_point.x = current_pose.x
        self.rel_vel_vector_start_point.y = current_pose.y
        self.rel_vel_vector_start_point.z = 0.0
        self.rel_vel_vector_end_point.x = current_pose.x + self.rel_vel_vector.velocity.x * self.dt
        self.rel_vel_vector_end_point.y = current_pose.y + self.rel_vel_vector.velocity.y * self.dt
        self.rel_vel_vector_end_point.z = 0.0
        self.rel_vel_vector_marker.points.append(self.rel_vel_vector_start_point)
        self.rel_vel_vector_marker.points.append(self.rel_vel_vector_end_point)
        self.rel_vel_vector_pub.publish(self.rel_vel_vector_marker)
        self.rel_vel_vector_marker.points.clear()


def main():
    rclpy.init(args=None)

    # Spawn modules
    visualizer = Visualizer()

    # Spawn the subscribers
    visualizer.create_subscription(msg_type=Trajectory2D, topic="/wamv/track_vector",
                                   callback=visualizer.track_vector_callback, qos_profile=10)
    visualizer.create_subscription(msg_type=Trajectory2DArray, topic="/wamv/escape_trajectories",
                                   callback=visualizer.escape_trajectory_callback, qos_profile=10)
    visualizer.create_subscription(msg_type=Twist2DStamped, topic="/wamv/velocity_3dof_ned",
                                   callback=visualizer.velocity_ned_callback, qos_profile=10)
    visualizer.create_subscription(msg_type=Twist2DStamped, topic="/relative_velocity",
                                   callback=visualizer.relative_vel_callback, qos_profile=10)
    visualizer.create_subscription(msg_type=RadarReturns, topic="/radar_returns", callback=visualizer.radar_callback,
                                   qos_profile=10)
    visualizer.create_subscription(msg_type=Int32, topic="/closest_vessel_index", callback=visualizer.closest_callback,
                                   qos_profile=10)

    try:
        rclpy.spin(visualizer)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        visualizer.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
