import traceback

import numpy as np

import rclpy
from nav_msgs.msg import Odometry
from rclpy.node import Node
from std_msgs.msg import Float64
from nav_2d_msgs.msg import Pose2DStamped, Twist2DStamped
from geometry_msgs.msg import PoseStamped
from .common.lib_transformations import euler_from_quaternion, yaw_to_heading, vel_body_to_ned, heading_to_yaw

"""
/wamv/pose_stamped: x, y, Quaternion
/wamv/pose_3dof: x, y, theta (Euler angle, rad)
"""


class WAMVOdometry(Node):
    def __init__(self):
        super().__init__("wamv_odometry")
        self.odom_data = None
        self.wamv_position = None
        self.hdg_deg = None  # WAM-V heading (psi, Angle btw. ENU/NED North/x-Axis and WAM-V x-Axis) in Degrees
        self.hdg_rad = None  # WAM-V heading (psi, Angle btw. ENU/NED North/x-Axis and WAM-V x-Axis) in Radians
        self.course = None

        self.velocity_vector_transform = None
        self.linear_velocity_vector = None
        self.angular_velocity_vector = None
        self.velocity_vector_3dof_b = Twist2DStamped()  # u, v, r in {b}
        self.velocity_vector_3dof_ned = Twist2DStamped()  # vel_x, vel_y, vel_theta in {n}
        self.pose_3dof = Pose2DStamped()  # x, y, theta
        self.pose_stamped = PoseStamped()  # x, y, quaternion

        self.hdg_deg_pub = self.create_publisher(Float64, "/wamv/hdg_deg", 10)
        self.hdg_rad_pub = self.create_publisher(Float64, "/wamv/hdg_rad", 10)
        self.velocity_3dof_b_pub = self.create_publisher(Twist2DStamped, "/wamv/velocity_3dof", 10)
        self.velocity_3dof_ned_pub = self.create_publisher(Twist2DStamped, "/wamv/velocity_3dof_ned", 10)
        self.pose2d_pub = self.create_publisher(Pose2DStamped, "/wamv/pose_3dof", 10)
        self.pose_stamped_pub = self.create_publisher(PoseStamped, "/wamv/pose_stamped", 10)
        self.course_pub = self.create_publisher(Float64, "/wamv/course_angle", 10)
        self.beta_pub = self.create_publisher(Float64, "/wamv/beta", 10)

    def odom_callback(self, odom_data_msg: Odometry):
        self.odom_data = odom_data_msg
        self.wamv_position = np.array([odom_data_msg.pose.pose.position.x, odom_data_msg.pose.pose.position.y])
        self.linear_velocity_vector = odom_data_msg.twist.twist.linear
        self.angular_velocity_vector = odom_data_msg.twist.twist.angular
        self.calculate_heading()
        self.calculate_course()
        self.publish_hdg()
        self.publish_velocity_3dof_b()
        self.publish_velocity_3dof_ned()
        self.publish_pose2d()
        self.publish_pose_stamped()

    def calculate_heading(self):
        if self.odom_data is None:
            return
        orientation = self.odom_data.pose.pose.orientation
        q = [orientation.x, orientation.y, orientation.z, orientation.w]
        euler = euler_from_quaternion(q)  # Roll, Pitch, Yaw
        self.hdg_deg = yaw_to_heading(euler[2])
        self.hdg_rad = euler[2]

    def calculate_course(self):
        x_n = np.array([1., 0])
        u_n = np.array([self.linear_velocity_vector.x, self.linear_velocity_vector.y])
        self.course = np.arccos(np.dot(x_n, u_n) / (np.linalg.norm(x_n) * np.linalg.norm(u_n)))
        course_msg = Float64()
        course_msg.data = self.course
        self.course_pub.publish(course_msg)
        self.publish_beta()

    def compute_velocity_vector_ned(self):
        if self.odom_data is None:
            return
        eta_dot = vel_body_to_ned(self.linear_velocity_vector.x, self.linear_velocity_vector.y,
                                  self.angular_velocity_vector.z, self.hdg_rad)
        return eta_dot

    def publish_beta(self):
        if self.hdg_rad is None or self.course is None:
            return
        beta = self.course - self.hdg_rad
        beta_msg = Float64()
        beta_msg.data = beta
        self.beta_pub.publish(beta_msg)

    def publish_hdg(self):
        if self.hdg_deg is None:
            self.get_logger().warning("No heading has been calculated!")
            return
        hdg_deg_msg = Float64()
        hdg_rad_msg = Float64()
        hdg_deg_msg.data = self.hdg_deg
        hdg_rad_msg.data = self.hdg_rad
        self.hdg_deg_pub.publish(hdg_deg_msg)
        self.hdg_rad_pub.publish(hdg_rad_msg)

    """
    Publishes the 3 DOF velocity vector in the {b} reference frame. 
    nu = (u, v, r)
    
    This is a little confusing, since the nav2d_msgs/Twist2D message was designed for wheeled robots, and they don't 
    use SNAME nomenclature. So in the message, x and y refer to velocities in the frame of the robot (corresponding to 
    u and v in SNAME), while in SNAME they refer to coordinates in the NED frame. 
    
    SNAME Conversion:
        velocity.x = u (Surge velocity)
        velocity.y = v (Sway velocity)
        velocity.theta = r (Yaw velocity)
    """

    def publish_velocity_3dof_b(self):
        if self.linear_velocity_vector is None:
            self.get_logger().warning("No valid velocity data to publish!")
            return
        self.velocity_vector_3dof_b.header.frame_id = "map"
        self.velocity_vector_3dof_b.velocity.x = self.linear_velocity_vector.x  # Surge velocity u
        self.velocity_vector_3dof_b.velocity.y = self.linear_velocity_vector.y  # Sway velocity v
        self.velocity_vector_3dof_b.velocity.theta = self.angular_velocity_vector.z  # Yaw velocity r
        self.velocity_3dof_b_pub.publish(self.velocity_vector_3dof_b)

    """
    Publishes the 3DOF velocity vector eta_dot in the global NED frame {n}.
    eta_dot = (vel_x, vel_y, vel_theta) 
    """

    def publish_velocity_3dof_ned(self):
        eta_dot = self.compute_velocity_vector_ned()
        self.velocity_vector_3dof_ned.header.frame_id = "map"
        self.velocity_vector_3dof_ned.velocity.x = eta_dot[0]
        self.velocity_vector_3dof_ned.velocity.y = eta_dot[1]
        self.velocity_vector_3dof_ned.velocity.theta = eta_dot[2]
        self.velocity_3dof_ned_pub.publish(self.velocity_vector_3dof_ned)

    def publish_pose2d(self):
        if self.hdg_rad is None:
            self.get_logger().warning("No HDG received yet!")
            return
        self.pose_3dof.header.frame_id = "map"
        self.pose_3dof.pose.x = self.wamv_position[0]
        self.pose_3dof.pose.y = self.wamv_position[1]
        self.pose_3dof.pose.theta = self.hdg_rad
        self.pose2d_pub.publish(self.pose_3dof)

    def publish_pose_stamped(self):
        if self.odom_data is None:
            return
        self.pose_stamped.header.frame_id = "map"
        self.pose_stamped.pose.position.x = self.wamv_position[0]
        self.pose_stamped.pose.position.y = self.wamv_position[1]
        self.pose_stamped.pose.orientation = self.odom_data.pose.pose.orientation
        self.pose_stamped_pub.publish(self.pose_stamped)


def main():
    rclpy.init(args=None)

    # Spawn modules
    odom = WAMVOdometry()

    # Spawn the subscribers
    odom.create_subscription(msg_type=Odometry, topic="/wamv/sensors/position/ground_truth_odometry",
                             callback=odom.odom_callback, qos_profile=10)

    try:
        rclpy.spin(odom)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        odom.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
