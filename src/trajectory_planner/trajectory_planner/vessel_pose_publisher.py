import traceback

import rclpy
from nav_msgs.msg import Odometry
# noinspection PyUnresolvedReferences
from custom_msgs.msg import CustomPoseStamped
from rclpy.clock import Clock
from rclpy.node import Node

"""
Uses Quaternion for orientation
"""

class VesselPosePublisher(Node):
    def __init__(self):
        super().__init__('vessel_pose_pub')
        self.clock = Clock()
        self.vessel_pose_pub = self.create_publisher(CustomPoseStamped, 'pose_stamped', 10)

    def vessel_odometry_callback(self, odom_msg: Odometry):
        pose = odom_msg.pose.pose  # type: ignore
        odom_msg.header.frame_id = 'map'
        odom_msg.header.stamp.sec = self.clock.now().to_msg().sec
        odom_msg.header.stamp.nanosec = self.clock.now().to_msg().nanosec
        vessel_pose_stamped = CustomPoseStamped()
        vessel_pose_stamped.publishing_node = self.get_namespace()
        vessel_pose_stamped.header = odom_msg.header
        vessel_pose_stamped.pose = pose
        self.vessel_pose_pub.publish(vessel_pose_stamped)


def main():
    rclpy.init(args=None)
    vessel_pose_pub = VesselPosePublisher()
    vessel_pose_pub.create_subscription(Odometry, '/model{}/odometry'.format(vessel_pose_pub.get_namespace()),
                                        vessel_pose_pub.vessel_odometry_callback, 10)
    try:
        rclpy.spin(vessel_pose_pub)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        vessel_pose_pub.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()
