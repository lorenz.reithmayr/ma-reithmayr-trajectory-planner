import os
import traceback

import rclpy
from ament_index_python.packages import get_package_share_directory
from geometry_msgs.msg import Quaternion, PoseStamped
from custom_msgs.msg import CustomPoseStamped
from rclpy.node import Node
from visualization_msgs.msg import Marker
from .common.lib_transformations import quaternion_from_euler, quaternion_multiply


class VesselModelPublisher(Node):
    def __init__(self):
        super().__init__("vessel_model_publisher")

        """
        Define a Marker to publish the mesh of the vessel
        """
        self.vessel_model = Marker()
        self.vessel_model.type = Marker.MESH_RESOURCE
        self.vessel_model.header.frame_id = "map"
        self.vessel_model.mesh_resource = "file://{}".format(
            os.path.join(get_package_share_directory("trajectory_planner"), "gz_files", "meshes", "Boat06.dae"))
        self.vessel_model.action = 0
        self.vessel_model.scale.x = 1.0
        self.vessel_model.scale.y = 1.0
        self.vessel_model.scale.z = 1.0
        self.vessel_model.color.a = 1.0
        self.vessel_model.color.g = 1.0

        self.vessel_model_pose_stamped = PoseStamped()
        """
        In the model.sdf of the ship, the "visual" and "collision" pose is specified as a RPY rotation around the 
        coordinate sytem defined in Gazebo.
        This rotation as defined in the SDF is not applied to the model in Rviz2, though. That's why the vessel's pose 
        quaternion needs to be rotated by that original rotation, as well.
        """
        self.q_rot = quaternion_from_euler(1.5708, 0, 1.5708)

        self.vessel_model_pub = self.create_publisher(Marker, "dae_model", 10)
        self.vessel_model_pose_pub = self.create_publisher(PoseStamped, "dae_model/poseStamped", 10)

    def vessel_pose_callback(self, vessel_pose: CustomPoseStamped):
        self.vessel_model.pose.position = vessel_pose.pose.position
        self.vessel_model.pose.orientation = self.rotateVesselOrientation(vessel_pose.pose.orientation)

        self.vessel_model_pose_stamped.header.frame_id = "map"
        self.vessel_model_pose_stamped.pose = self.vessel_model.pose
        self.publish_vessel_model()

    def publish_vessel_model(self):
        self.vessel_model_pub.publish(self.vessel_model)
        self.vessel_model_pose_pub.publish(self.vessel_model_pose_stamped)

    def rotateVesselOrientation(self, vessel_orientation_gt_pose: Quaternion) -> Quaternion:
        q = [vessel_orientation_gt_pose.x, vessel_orientation_gt_pose.y, vessel_orientation_gt_pose.z,
             vessel_orientation_gt_pose.w]
        pose_quaternion_tuple_rotated = quaternion_multiply(self.q_rot, q)
        pose_quaternion_rotated = Quaternion()

        # The order of the return values here was derived by simply trying until it looked right...
        (pose_quaternion_rotated.y, pose_quaternion_rotated.x, pose_quaternion_rotated.z,
         pose_quaternion_rotated.w) = pose_quaternion_tuple_rotated
        return pose_quaternion_rotated


def main():
    rclpy.init(args=None)
    vessel_model_pub = VesselModelPublisher()
    vessel_model_pub.create_subscription(CustomPoseStamped, "pose_stamped",
                                         vessel_model_pub.vessel_pose_callback, 10)
    try:
        rclpy.spin(vessel_model_pub)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        vessel_model_pub.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
