import traceback
import numpy as np
import rclpy
from nav_msgs.msg import OccupancyGrid
from map_msgs.msg import OccupancyGridUpdate
from rclpy.node import Node
from rclpy.clock import Clock
import os
import time

"""
=== Map handler node ===
Receives an occupancy grid from nav2_map_server and publishes it. Processes any updates to the OGM and publishes 
updated versions.

Subscribed-to data:
    - nav_msgs/OccupancyGrid 

Processing:

Published data:
"""


class MapHandler(Node):
    def __init__(self):
        super().__init__("map_handler")
        # Grid Map
        self.ogm = None
        self.ogm_resolution = None
        self.ogm_origin = None
        self.ogm_publisher = self.create_publisher(OccupancyGrid, "/map_handler/map", 10)
        self.ogm_update_publisher = self.create_publisher(OccupancyGridUpdate, "/map_handler/map_updates", 10)
        self.costmap_pub = self.create_publisher(OccupancyGrid, "map_handler/costmap", 10)
        self.clock = Clock()
        self.timer = self.create_timer(timer_period_sec=2, callback=self.publish_ogm)

        # Costmap
        self.enable_save_costmap = False
        self.costmap = None
        self.i = 0
        self.j = 0

    def ogm_callback(self, ogm: OccupancyGrid):
        self.get_logger().debug("Map Handler: Grid Map received!")
        self.ogm = ogm
        self.ogm_origin = self.ogm.info.origin
        self.ogm_resolution = self.ogm.info.resolution
        self.publish_ogm()

    def costmap_callback(self, costmap: OccupancyGrid):
        self.costmap = costmap
        self.i = self.i + 1
        # Save costmap once for external testing
        if self.enable_save_costmap:
            if self.i % 80 == 0 and self.j == 0:
                self.get_logger().info("=============== Saving Costmap! ========================")
                self.save_costmap()
                self.get_logger().info(f"=============== Costmap saved to {os.getcwd()}/metrics/maps/costmap_{int(time.time())}.gz ========================")
                self.j = self.j + 1

    def publish_ogm(self):
        if self.ogm is not None:
            self.ogm_publisher.publish(self.ogm)

    def save_costmap(self):
        if self.costmap is not None:
            costmap_np = np.array(self.costmap.data).reshape((self.costmap.info.height, self.costmap.info.width))
            np.savetxt(f"{os.getcwd()}/metrics/maps/costmap_{int(time.time())}.gz", costmap_np)


def main():
    rclpy.init(args=None)
    map_handler = MapHandler()

    map_handler.create_subscription(msg_type=OccupancyGrid, topic="/map", callback=map_handler.ogm_callback,
                                    qos_profile=10)
    map_handler.create_subscription(msg_type=OccupancyGrid, topic="/global_costmap/costmap",
                                    callback=map_handler.costmap_callback, qos_profile=10)
    try:
        rclpy.spin(map_handler)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        map_handler.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
