import sys
from math import pi, cos, sin

import numpy as np


def compute_distance_2d(origin, target):
    """
    Compute the euclidian distance between to 2x1 vectors
    :param origin
    :param target
    :return: Euclidian distance
    """
    if origin.shape != (2,) or target.shape != (2,):
        print("Points need to be of dimension 1x2", file=sys.stderr)
        return
    return np.linalg.norm(np.subtract(target, origin))


def normalize_angle(theta):
    """
    Normalizes an Angle between -pi and pi
    :param theta: Angle to be normalized [rad]
    :return theta: Normalized angle [rad]
    """
    theta_norm = (theta + pi) % (2 * pi) - pi
    return theta_norm


