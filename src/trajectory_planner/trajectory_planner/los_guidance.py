import traceback
from math import atan, atan2, sin, cos

import numpy as np
# noinspection PyUnresolvedReferences
from custom_msgs.msg import Trajectory2D, Trajectory2DArray

import rclpy
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry, Path
from rclpy.node import Node
from std_msgs.msg import Float64, Bool
from visualization_msgs.msg import Marker
from .common.lib_helpers import compute_distance_2d

"""
LOS guidance algorithm implementation according to 
[1] T. I. Fossen, Handbook of Marine Craft Hydrodynamics and Motion Control, 1st ed. John Wiley & Sons, Ltd, 2011. 
"""


class LOSGuidance(Node):
    def __init__(self):
        super().__init__("los_guidance")
        self.wamv_odometry = None
        self.wamv_hdg_deg = None
        self.wamv_hdg_rad = None
        self.active_path = None
        self.active_path_type = None
        self.path_length = None
        self.current_pos = None
        self.replanned_path = None

        # LOS algorithm parameters
        self.declare_parameters(namespace='', parameters=[('update_frequency', rclpy.Parameter.Type.INTEGER),
                                                          ('delta_global', rclpy.Parameter.Type.INTEGER),
                                                          ('delta_replanned', rclpy.Parameter.Type.INTEGER),
                                                          ('delta_dwa', rclpy.Parameter.Type.INTEGER),
                                                          ('goal_radius', rclpy.Parameter.Type.DOUBLE)])
        self.update_freq = self.get_parameter('update_frequency').get_parameter_value().integer_value
        self.goal_radius = self.get_parameter('goal_radius').get_parameter_value().double_value
        self.delta = 7
        self.beta = 0.0  # Sideslip angle due to ocean currents (to be implemented) [1] Eq. (10.85)
        self.los_hdg_rad = Float64()  # Intercept heading calculated by the LOS algorithm. 0 at North, increasing CW
        self.cte = Float64()  # Intercept heading calculated by the LOS algorithm. 0 at North, increasing CW

        self.timer = self.create_timer(1 / self.update_freq, self.event_loop, clock=self.get_clock())

        # LOS-related variables
        self.closest_wpt_idx = None
        self.alpha_k = None
        self.crosstrack_error = None
        self.psi_d = None
        self.R = None
        self.first_wpt = None
        self.second_wpt = None
        self.first_wpt_vec = None
        self.first_path_segment = None
        self.current_los_vector = None
        self.current_los_point = None
        self.los_active_flag = Bool()
        self.los_active_flag.data = False
        self.escape_finished_flag = Bool()
        self.escape_finished_flag.data = False

        # Flags
        self.FINAL_SEGMENT = False
        self.FIRST_LEG_ACTIVATED_ONCE = False
        self.GOAL_REACHED = False
        self.ESCAPE_IN_PROGRESS = False

        # LOS visualization
        self.goal_marker = Marker()
        self.los_pt_marker = Marker()
        self.los_vector_marker = Marker()
        self.los_vector_start_point = Point()
        self.los_vector_end_point = Point()
        self.initialize_markers()

        self.los_pub = self.create_publisher(Float64, "/wamv/los_hdg_rad", 10)
        self.los_pt_pub = self.create_publisher(Marker, "/wamv/los_pt", 10)
        self.los_vec_pub = self.create_publisher(Marker, "/wamv/los_vector", 10)
        self.goal_marker_pub = self.create_publisher(Marker, "/global_path/goal_marker", 10)
        self.los_active_flag_pub = self.create_publisher(Bool, "/wamv/los_active", 10)
        self.los_active_flag_pub.publish(self.los_active_flag)  # Publish the active flag once on startup
        self.cmd_vel_pub = self.create_publisher(Odometry, "/wamv/cmd_vel_3dof", 10)
        self.cte_pub = self.create_publisher(Float64, "/wamv/crosstrack_error", 10)

    def event_loop(self):
        if self.active_path is None:
            return
        if self.check_goal_reached():
            self.active_path = None
            self.FINAL_SEGMENT = False
            self.GOAL_REACHED = True
            self.reset_markers()
            self.los_active_flag.data = False
            self.los_active_flag_pub.publish(self.los_active_flag)
            odom_msg = Odometry()
            odom_msg.twist.twist.linear.x = 0.0
            odom_msg.twist.twist.angular.z = 0.0
            self.cmd_vel_pub.publish(odom_msg)
            return

        self.los_guidance()

        """
        If the path is not active (i.e. the WAM-V is still tracking toward the first waypoint)
        and the first leg has not been activated at least once:
         - The desired heading psi_d is the angle btw. the current WAM-V heading and the vector to the first WPT
         - The circle of acceptance radius R is equal to the length of that vector
         - As a result, the current LOS point will be located at the first WPT
        """
        if not self.is_path_active() and not self.FIRST_LEG_ACTIVATED_ONCE:
            self.psi_d = atan2(self.first_wpt_vec[1], self.first_wpt_vec[0])
            self.R = np.linalg.norm(self.first_wpt_vec)
            self.current_los_point = np.array(
                [self.current_pos[0] + self.R * cos(self.psi_d), self.current_pos[1] + self.R * sin(self.psi_d)])

        self.los_hdg_rad.data = self.psi_d
        self.build_los_pt_msg()
        self.build_los_vector_msg()
        self.publish_los()

    def wamv_odom_callback(self, wamv_odometry_msg: Odometry):
        self.wamv_odometry = wamv_odometry_msg
        self.current_pos = np.array([self.wamv_odometry.pose.pose.position.x, self.wamv_odometry.pose.pose.position.y])

    def global_path_callback(self, path_msg: Path):
        self.active_path_type = "Global Path"
        self.delta = self.get_parameter('delta_global').get_parameter_value().integer_value
        self.init_los_guidance(path_msg)

    def replanned_path_callback(self, replanned_path_msg: Path):
        self.active_path_type = "Replanned Theta* Path"
        self.delta = self.get_parameter('delta_replanned').get_parameter_value().integer_value
        self.init_los_guidance(replanned_path_msg)

    def dwa_path_callback(self, dwa_path_msg):
        if self.los_active_flag.data:
            self.active_path_type = "DWA Path"
            self.delta = self.get_parameter('delta_dwa').get_parameter_value().integer_value
            self.init_los_guidance(dwa_path_msg)

    def wamv_hdg_deg_callback(self, hdg_deg_msg: Float64):
        self.wamv_hdg_deg = hdg_deg_msg

    def wamv_hdg_rad_callback(self, hdg_rad_msg: Float64):
        self.wamv_hdg_rad = hdg_rad_msg

    def los_active_callback(self, los_active_msg: Bool):
        self.los_active_flag = los_active_msg

    def beta_callback(self, beta_msg):
        pass  # self.beta = beta_msg.data

    def init_los_guidance(self, path_msg):
        self.los_active_flag.data = True
        self.los_active_flag_pub.publish(self.los_active_flag)
        self.reset_markers()
        self.active_path = path_msg
        self.path_length = len(self.active_path.poses)
        self.goal_marker.pose.position = self.active_path.poses[-2].pose.position if self.active_path_type.__contains__(
            "DWA") else self.active_path.poses[-1].pose.position
        self.first_wpt = np.array(
            [self.active_path.poses[0].pose.position.x, self.active_path.poses[0].pose.position.y])
        self.FINAL_SEGMENT = False
        self.FIRST_LEG_ACTIVATED_ONCE = False
        self.GOAL_REACHED = False

    """
    Lookahead-based LOS algorithm according to  [1], pp. 267 ff.
    - Find closest waypoint according to Euclidian norm
    - Calculate path-tangential angle and crosstrack error
    - Calculate desired heading angle according to [1] Eq. (10.72)

    :return psi_d   Desired heading angle
    """

    def los_guidance(self):
        self.find_closest_waypoint()
        self.calculate_alpha_k()
        self.calculate_crosstrack_error()
        self.calculate_psi_d()

    """
    Iterate through all the points in the received path and find the one closest to the current WAM-V position
    """

    def find_closest_waypoint(self):
        dist_min = np.inf
        closest_so_far = None
        for idx, wp in enumerate(self.active_path.poses):
            wp_pos = np.array([wp.pose.position.x, wp.pose.position.y])
            dist = compute_distance_2d(self.current_pos, wp_pos)
            if dist < dist_min:
                dist_min = dist
                closest_so_far = idx
        self.closest_wpt_idx = closest_so_far

    """
    Calculate the path-tangential angle alpha_k. See [1] Eq. (10.55).

    If the next waypoint (i.e. the waypoint "delta" places ahead of the current waypoint) is the last point on the path,
    the bearing to the last waypoint will be followed until the goal has been reached.

    :param closest_wpt_idx  Index into the pose array of the global path belonging to the closes waypoint
    :return alpha_k  The path-tangential angle  
    :return current_path_segment    The current active path segment (segment btw. the current and next waypoint)
    """

    def calculate_alpha_k(self):
        # Current waypoint is the waypoint closest to us
        current_wpt = np.array([self.active_path.poses[self.closest_wpt_idx].pose.position.x,
                                self.active_path.poses[self.closest_wpt_idx].pose.position.y])
        if self.closest_wpt_idx >= self.path_length - self.delta:
            next_wpt = np.array(
                [self.active_path.poses[-1].pose.position.x, self.active_path.poses[-1].pose.position.y])
            self.FINAL_SEGMENT = True
        else:
            next_wpt = np.array([self.active_path.poses[self.closest_wpt_idx + self.delta].pose.position.x,
                                 self.active_path.poses[self.closest_wpt_idx + self.delta].pose.position.y])
        current_path_segment = np.subtract(next_wpt, current_wpt)
        self.alpha_k = atan2(current_path_segment[1], current_path_segment[0])

    """
    Calculate the current cross track error according to [1] Eq. (10.59) 
    and the radius of the COA according to [1] Eq. (10.63).
    If the WAM-V is on the final segment (the path segment from the penultimate to the last waypoint) R is set to that 
    distance.
    """

    def calculate_crosstrack_error(self):
        self.crosstrack_error = (
                -(self.current_pos[0] - self.active_path.poses[self.closest_wpt_idx].pose.position.x) * sin(
            self.alpha_k) + (self.current_pos[1] - self.active_path.poses[self.closest_wpt_idx].pose.position.y) * cos(
            self.alpha_k))
        if self.FINAL_SEGMENT:
            goal_pos = np.array(
                [self.active_path.poses[-1].pose.position.x, self.active_path.poses[-1].pose.position.y])
            self.R = np.linalg.norm(np.subtract(goal_pos, self.current_pos))
            return
        self.R = np.sqrt(np.square(self.crosstrack_error) + np.square(self.delta))  # Circle of acceptance radius
        self.cte.data = self.crosstrack_error
        self.cte_pub.publish(self.cte)

    """
    Calculate the LOS desired heading psi_d according to [1] Eq. (10.72) ff.
    and the current LOS point (for visualization purposes)
    """

    def calculate_psi_d(self):
        chi_r = atan(-(1 / self.delta) * self.crosstrack_error)
        chi_p = self.alpha_k
        chi_d = chi_p + chi_r
        self.psi_d = chi_d + self.beta
        self.current_los_point = np.array(
            [self.current_pos[0] + self.R * cos(self.psi_d), self.current_pos[1] + self.R * sin(self.psi_d)])

    """
    Check if the current path is active, i.e. if the WAM-V has passed the first waypoint at least once.

    The cross product of the previously computed LOS vector (vector from current position to the LOS point) and the
    vector to the first waypoint, and the crosstrack error will have different signs if the current LOS point 
    is "below" the first waypoint. As soon as the LOS point enters the first path segment (and is then located "above"
    or "after" the first waypoint) the cross product and the CTE will have the same sign 
    (< 0 if left of path, > 0 if right of path).
    """

    def is_path_active(self):
        self.first_wpt_vec = np.subtract(self.first_wpt, self.current_pos)
        self.current_los_vector = np.subtract(self.current_los_point, self.current_pos)
        # noinspection PyUnreachableCode
        cross_product = np.cross(self.first_wpt_vec, self.current_los_vector)
        if np.sign(cross_product) == np.sign(self.crosstrack_error):
            self.FIRST_LEG_ACTIVATED_ONCE = True
            return True
        return False

    """
    Check if the current position is withing a predetermined radius around the goal (i.e. the last point on the path)
    """

    def check_goal_reached(self):
        goal_pos = np.array([self.active_path.poses[-1].pose.position.x, self.active_path.poses[-1].pose.position.y])
        dist_to_goal = np.linalg.norm(np.subtract(goal_pos, self.current_pos))
        # self.get_logger().info("Distance to Goal: {}".format(dist_to_goal))
        if dist_to_goal < self.goal_radius:
            self.get_logger().warning("The goal has been reached!")
            return True
        return False

    """
    Set up all Rviz markers for visualization
    """

    def initialize_markers(self):
        self.goal_marker.type = Marker.CUBE
        self.goal_marker.header.frame_id = "map"
        self.goal_marker.action = 0
        self.goal_marker.color.a = 1.0
        self.goal_marker.color.r = 0.0
        self.goal_marker.color.g = 1.0
        self.goal_marker.color.b = 1.0
        self.goal_marker.scale.x = 1.5
        self.goal_marker.scale.y = 1.5
        self.goal_marker.scale.z = 1.5

        self.los_pt_marker.type = Marker.CUBE
        self.los_pt_marker.header.frame_id = "map"
        self.los_pt_marker.action = 0
        self.los_pt_marker.color.a = 1.0
        self.los_pt_marker.color.r = 0.0
        self.los_pt_marker.color.g = 1.0
        self.los_pt_marker.color.b = 0.0
        self.los_pt_marker.scale.x = 1.0
        self.los_pt_marker.scale.y = 1.0
        self.los_pt_marker.scale.z = 1.0

        self.los_vector_marker.type = Marker.ARROW
        self.los_vector_marker.header.frame_id = "map"
        self.los_vector_marker.action = 0
        self.los_vector_marker.color.a = 1.0
        self.los_vector_marker.color.r = 1.0
        self.los_vector_marker.color.g = 0.0
        self.los_vector_marker.color.b = 0.0
        self.los_vector_marker.scale.x = 0.5  # Shaft diameter
        self.los_vector_marker.scale.y = 1.0  # Arrow head width
        self.los_vector_marker.scale.z = 1.5  # Arrow head length

    """
    Construct the LOS point marker messages
    """

    def build_los_pt_msg(self):
        self.los_pt_marker.pose.position.x = self.current_los_point[0]
        self.los_pt_marker.pose.position.y = self.current_los_point[1]
        self.los_pt_marker.pose.position.z = 0.0

    """
    Construct the LOS vector marker messages
    """

    def build_los_vector_msg(self):
        self.los_vector_start_point.x = self.current_pos[0]
        self.los_vector_start_point.y = self.current_pos[1]
        self.los_vector_start_point.z = 0.0
        self.los_vector_end_point.x = self.current_los_point[0]
        self.los_vector_end_point.y = self.current_los_point[1]
        self.los_vector_end_point.z = 0.0
        self.los_vector_marker.points.append(self.los_vector_start_point)
        self.los_vector_marker.points.append(self.los_vector_end_point)

    def publish_los(self):
        self.los_pub.publish(self.los_hdg_rad)
        self.los_pt_pub.publish(self.los_pt_marker)
        self.los_vec_pub.publish(self.los_vector_marker)
        self.goal_marker_pub.publish(self.goal_marker)
        self.los_vector_marker.points.clear()

    """
    Reset all Rviz markers before initializing a new LOS navigation
    """

    def reset_markers(self):
        self.los_pt_marker.action = 2  # DELETE
        self.los_vector_marker.action = 2  # DELETE
        self.goal_marker.action = 2  # DELETE
        self.los_pt_pub.publish(self.los_pt_marker)
        self.los_vec_pub.publish(self.los_vector_marker)
        self.goal_marker_pub.publish(self.goal_marker)

        self.los_pt_marker = Marker()
        self.los_vector_marker = Marker()
        self.goal_marker = Marker()
        self.initialize_markers()


def main():
    rclpy.init(args=None)

    los_guidance = LOSGuidance()

    los_guidance.create_subscription(Odometry, "/wamv/sensors/position/ground_truth_odometry",
                                     los_guidance.wamv_odom_callback, 10)

    los_guidance.create_subscription(Path, "/global_path", los_guidance.global_path_callback, 10)

    los_guidance.create_subscription(Path, "/replanned_path", los_guidance.replanned_path_callback, 10)

    los_guidance.create_subscription(Path, "/dwa_path", los_guidance.dwa_path_callback, 10)

    los_guidance.create_subscription(Float64, "/wamv/hdg_deg", los_guidance.wamv_hdg_deg_callback, 10)

    los_guidance.create_subscription(Float64, "/wamv/hdg_rad", los_guidance.wamv_hdg_rad_callback, 10)

    los_guidance.create_subscription(Bool, "/wamv/los_active", los_guidance.los_active_callback, 10)
    los_guidance.create_subscription(Float64, "/wamv/beta", los_guidance.beta_callback, 10)

    try:
        rclpy.spin(los_guidance)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        los_guidance.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
