import os
import traceback
from math import atan2

import numpy as np

import rclpy

from time import perf_counter
from ament_index_python import get_package_share_directory
from geometry_msgs.msg import Pose, PoseStamped, PoseWithCovarianceStamped
from nav_msgs.msg import Odometry, OccupancyGrid, Path
from rclpy.clock import Clock
from rclpy.node import Node
from .common.lib_global_planning import a_star
from .common.lib_transformations import gt_to_grid, grid_to_gt

"""
=== Global path planner node ===
Plans a global path on an occupancy grid map 

Subscribed-to data:
    - Obstacle vessel pose(s)
    - WAM-V pose
    - Goal pose selected via Rviz2
    - Occupancy Grid Map published by the map handler 
    (TODO: Actually publish the grid map via the map handler, not just once via map_server

Processing:
    - Convert received and selected poses from world coordinates to grid cell coordinates
    - Run A* path finding algorithm to return a series of poses making up the path
    - Re-transform cell coordinates to world coordinates
    - Construct a ROS path message from poses
    
Published data:
    - nav_msgs/msg/Path
"""

# Data Collection
root_dir = get_package_share_directory("trajectory_planner").split("install")[0]
metrics_path = "{}metrics/GlobalPathPlanner".format(root_dir)


class GlobalPathPlanner(Node):
    def __init__(self):
        super().__init__("global_planner")
        # Poses
        self.wamv_pose = None  # Ground truth pose of the WAM-V
        self.initial_pose = None
        self.goal_pose = None

        # Gridmap
        self.costmap_data = None  # Occupancy Grid Map
        self.costmap_info = None
        self.costmap_origin = None
        self.costmap_resolution = None

        # Path planner
        self.goal = None
        self.global_path = None
        self.path_publisher = self.create_publisher(Path, "/global_path_a_star", 10)

        # Timer
        self.clock = Clock()

        # Testing
        self.testing = True

    def wamv_pose_callback(self, wamv_odom_msg: Odometry):
        self.wamv_pose = wamv_odom_msg.pose.pose

    def initial_pose_callback(self, initial_pose_msg: PoseWithCovarianceStamped):
        self.initial_pose = initial_pose_msg.pose.pose

    def goal_pose_callback(self, goal_pose_msg: PoseStamped):
        self.goal_pose = goal_pose_msg.pose
        if self.initial_pose is None:
            self.plan_global_path(self.wamv_pose, self.goal_pose)
        else:
            self.plan_global_path(self.initial_pose, self.goal_pose)
        self.publish_path()
        self.initial_pose = None

    def costmap_callback(self, costmap_msg: OccupancyGrid):
        self.get_logger().debug("Global Planner: Cost Map received!")
        self.costmap_data = np.array(costmap_msg.data).reshape((costmap_msg.info.height, costmap_msg.info.width))
        self.costmap_info = costmap_msg.info
        self.costmap_origin = self.costmap_info.origin.position
        self.costmap_resolution = self.costmap_info.resolution

    def plan_global_path(self, initial_pose, goal_pose):
        if self.costmap_data is not None:
            self.get_logger().info("Starting A* path planning")
            inv_costmap_resolution = 1 / self.costmap_resolution

            initial_position = initial_pose.position
            goal_position = goal_pose.position

            initial_pos_grid = gt_to_grid(initial_position.x, initial_position.y, self.costmap_origin.x,
                                          self.costmap_origin.y, inv_costmap_resolution)
            goal_pos_grid = gt_to_grid(goal_position.x, goal_position.y, self.costmap_origin.x, self.costmap_origin.y,
                                       inv_costmap_resolution)

            if self.testing:
                t_start = perf_counter()
                self.global_path = a_star(initial_pos_grid, goal_pos_grid, self.costmap_data)
                t_stop = perf_counter()
                dur = t_stop - t_start
                self.get_logger().warn(f"A* time: {dur*1e3} ms")

                dist_astar = 0.0
                wpts_gt = [np.array(
                    grid_to_gt(wpt[0], wpt[1], self.costmap_origin.x, self.costmap_origin.y, self.costmap_resolution))
                           for wpt in self.global_path]
                for i in range(len(wpts_gt) - 1):
                    dist_astar += np.linalg.norm(wpts_gt[i + 1] - wpts_gt[i])
                self.get_logger().warn(f"A* distance: {dist_astar} m")
                self.get_logger().warn(f"Euclidean Distance: {np.linalg.norm(wpts_gt[-1] - wpts_gt[0])} m")
                straight = np.subtract(wpts_gt[-1], wpts_gt[0])
                self.get_logger().warn(f"Angle: {atan2(straight[1], straight[0])}")

            else:
                self.global_path = a_star(initial_pos_grid, goal_pos_grid, self.costmap_data)
            self.get_logger().info("Path Planning finished")

    def publish_path(self):
        if self.global_path is None:
            self.get_logger().error("No global path found!")
            return

        self.get_logger().info("Publishing Path!")
        ros_path = Path()
        ros_path.header.frame_id = "map"
        for pos_grid in self.global_path:
            pos_gt = grid_to_gt(pos_grid[0], pos_grid[1], self.costmap_origin.x, self.costmap_origin.y,
                                self.costmap_resolution)
            path_pose = PoseStamped()
            path_pose.header.frame_id = "map"
            path_pose.header.stamp.sec = self.clock.now().to_msg().sec
            path_pose.header.stamp.nanosec = self.clock.now().to_msg().nanosec
            path_pose.pose.position.x = pos_gt[0]
            path_pose.pose.position.y = pos_gt[1]
            ros_path.poses.append(path_pose)
        self.path_publisher.publish(ros_path)
        self.get_logger().info("Path published on topic {}".format(self.path_publisher.topic))


def main():
    rclpy.init(args=None)

    # Spawn modules
    global_planner = GlobalPathPlanner()

    # Spawn the subscribers
    global_planner.create_subscription(msg_type=Odometry, topic="/wamv/sensors/position/ground_truth_odometry",
                                       callback=global_planner.wamv_pose_callback, qos_profile=10)

    global_planner.create_subscription(msg_type=PoseWithCovarianceStamped, topic="/initialpose",
                                       callback=global_planner.initial_pose_callback, qos_profile=10)

    global_planner.create_subscription(msg_type=PoseStamped, topic="/goal_pose",
                                       callback=global_planner.goal_pose_callback, qos_profile=10)

    global_planner.create_subscription(msg_type=OccupancyGrid, topic="/global_costmap/costmap",
                                       callback=global_planner.costmap_callback, qos_profile=10)

    try:
        rclpy.spin(global_planner)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        global_planner.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
