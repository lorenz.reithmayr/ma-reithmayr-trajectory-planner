import traceback

import numpy as np
# noinspection PyUnresolvedReferences
from custom_msgs.msg import CustomPoseStamped, CustomPoseArray, CustomFloat64, Trajectory2D
# noinspection PyUnresolvedReferences
from custom_msgs.msg import CustomTwist2DStamped
from .trajectory_generator import TrajectoryGenerator
from .common.lib_transformations import vel_body_to_ned
from std_msgs.msg import Float64

import rclpy
from nav_msgs.msg import Odometry, Path
from rclpy.node import Node
from .common.lib_transformations import euler_from_quaternion

"""
Uses theta (Euler angle, rad) for orientation
        theta = euler_from_quaternion(q_orientation)[2]
"""


class ObstacleTracker(Node):
    def __init__(self):
        super().__init__("obstacle_tracker")
        self.prediction_horizon = 20
        self.dt = 1.0  # Needs to match the value of Local Planner AND WAMV Kinematic Params

        self.obstacle_velocity = None
        self.obstacle_pose = None
        self.obstacle_position = None
        self.wamv_position = None
        self.velocity_vector_3dof_ned_msg = CustomTwist2DStamped()

        self.dist_to_obstacle = None
        self.distance_y = None
        self.distance_x = None

        self.predicted_poses = CustomPoseArray()
        self.predicted_poses.header.frame_id = "map"
        self.predicted_poses.publishing_node = self.get_namespace()
        self.dist_to_obstacle_msg = CustomFloat64()
        self.dist_to_obstacle_f64_msg = Float64()

        # Publishers
        self.prediction_pub = self.create_publisher(Trajectory2D, "predicted_trajectory", 10)
        self.path_pub = self.create_publisher(Path, "predicted_trajectory_path", 10)
        self.dist_from_obstacle_pub = self.create_publisher(CustomFloat64,
                                                            "/wamv/distance_to_obstacle{}".format(self.get_namespace()),
                                                            10)
        self.dist_from_obstacle_f64_pub = self.create_publisher(Float64, "/wamv/distance_to_obstacle_f64{}".format(
            self.get_namespace()), 10)
        self.velocity_3dof_ned_pub = self.create_publisher(CustomTwist2DStamped, "velocity_3dof_ned", 10)

        self.trajectory_generator = TrajectoryGenerator(self.dt, self.prediction_horizon)

    def wamv_odom_callback(self, wamv_odom_msg: Odometry):
        self.wamv_position = np.array([wamv_odom_msg.pose.pose.position.x, wamv_odom_msg.pose.pose.position.y])
        if self.obstacle_pose is None:
            return
        self.calculate_distance_to_obstacle()
        self.dist_from_obstacle_pub.publish(self.dist_to_obstacle_msg)
        self.dist_from_obstacle_f64_pub.publish(self.dist_to_obstacle_f64_msg)

    def obstacle_odom_callback(self, vessel_odom_msg: Odometry):
        self.obstacle_pose = self.get_oriented_pose(vessel_odom_msg)
        self.obstacle_velocity = self.get_current_velocity(vessel_odom_msg)
        predicted_traj = self.trajectory_generator.generate_trajectory(self.obstacle_pose, self.obstacle_velocity,
                                                                       np.array([0, 0, 0]))
        path_msg = self.trajectory_generator.to_PathFromTrajectory2D(predicted_traj)
        predicted_traj.publishing_node = self.get_namespace()
        self.prediction_pub.publish(predicted_traj)
        self.path_pub.publish(path_msg)
        self.publish_velocity_3dof_ned()

    def compute_velocity_vector_ned(self):
        if self.obstacle_velocity is None:
            return
        eta_dot = vel_body_to_ned(self.obstacle_velocity[0], self.obstacle_velocity[1], self.obstacle_velocity[2],
                                  self.obstacle_pose[2])
        return eta_dot

    @staticmethod
    def get_oriented_pose(odom_msg: Odometry):
        x = odom_msg.pose.pose.position.x
        y = odom_msg.pose.pose.position.y
        q_orientation = [odom_msg.pose.pose.orientation.x, odom_msg.pose.pose.orientation.y,
                         odom_msg.pose.pose.orientation.z, odom_msg.pose.pose.orientation.w]
        theta = euler_from_quaternion(q_orientation)[2]
        return np.array([x, y, theta])

    @staticmethod
    def get_current_velocity(odom_msg: Odometry):
        return np.array([odom_msg.twist.twist.linear.x, odom_msg.twist.twist.linear.y, odom_msg.twist.twist.angular.z])

    def calculate_distance_to_obstacle(self):
        vec_to_obstacle = np.subtract(self.obstacle_pose[0:2], self.wamv_position)
        self.distance_x = vec_to_obstacle[0]
        self.distance_y = vec_to_obstacle[1]
        self.dist_to_obstacle = np.linalg.norm(vec_to_obstacle)
        self.dist_to_obstacle_msg.data = self.dist_to_obstacle
        self.dist_to_obstacle_msg.publishing_node = self.get_namespace()

        self.dist_to_obstacle_f64_msg.data = self.dist_to_obstacle

    def publish_velocity_3dof_ned(self):
        eta_dot = self.compute_velocity_vector_ned()
        self.velocity_vector_3dof_ned_msg.header.frame_id = "map"
        self.velocity_vector_3dof_ned_msg.publishing_node = self.get_namespace()
        self.velocity_vector_3dof_ned_msg.twist.velocity.x = eta_dot[0]
        self.velocity_vector_3dof_ned_msg.twist.velocity.y = eta_dot[1]
        self.velocity_vector_3dof_ned_msg.twist.velocity.theta = eta_dot[2]
        self.velocity_3dof_ned_pub.publish(self.velocity_vector_3dof_ned_msg)


def main():
    rclpy.init(args=None)
    obstacle_tracker = ObstacleTracker()
    obstacle_tracker.create_subscription(Odometry, "/model{}/odometry".format(obstacle_tracker.get_namespace()),
                                         obstacle_tracker.obstacle_odom_callback, 10)
    obstacle_tracker.create_subscription(Odometry, "/wamv/sensors/position/ground_truth_odometry",
                                         callback=obstacle_tracker.wamv_odom_callback, qos_profile=10)
    try:
        rclpy.spin(obstacle_tracker)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        obstacle_tracker.destroy_node()
        rclpy.shutdown()


if __name__ == "main":
    main()
