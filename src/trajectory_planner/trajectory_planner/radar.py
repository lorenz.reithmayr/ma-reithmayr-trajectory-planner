import traceback

# noinspection PyUnresolvedReferences
from custom_msgs.msg import CustomPoseStamped, CustomPoseArray, CustomFloat64, CustomTwist2DStamped, Trajectory2D, \
    RadarData, RadarReturns

import rclpy
from rclpy.node import Node


class Radar(Node):
    def __init__(self):
        super().__init__("radar")
        self.declare_parameters(namespace='', parameters=[('obstacle_vessels', rclpy.Parameter.Type.STRING_ARRAY)])
        self.vessel_list = self.get_parameter('obstacle_vessels').get_parameter_value().string_array_value
        self.radar_targets = {}  # Dictionary (hash map) holding the vessel data
        [self.create_entry(target) for target in self.vessel_list]
        self.event_loop = self.create_timer(0.1, self.scan)
        self.radar_pub = self.create_publisher(RadarReturns, "/radar_returns", 10)

    def create_entry(self, target):
        self.radar_targets[f"/{target}"] = {}

    def poses_callback(self, pose_msg):
        self.radar_targets[pose_msg.publishing_node]["pose"] = pose_msg.pose

    def trajectories_callback(self, trajectory_msg):
        self.radar_targets[trajectory_msg.publishing_node]["trajectory"] = trajectory_msg

    def distance_callback(self, dist_msg):
        self.radar_targets[dist_msg.publishing_node]["dist"] = dist_msg.data

    def velocity_ned_callback(self, vel_msg):
        self.radar_targets[vel_msg.publishing_node]["velocity"] = vel_msg.twist.velocity

    def get_targets(self):
        return self.vessel_list

    def build_radar_message(self):
        radar_return_msg = RadarReturns()
        for vessel, data in self.radar_targets.items():
            radar_data_msg = RadarData()
            radar_data_msg.name = vessel
            for key in data:
                match key:
                    case "pose":
                        radar_data_msg.pose = data[key]
                    case "trajectory":
                        radar_data_msg.trajectory = data[key]
                    case "dist":
                        radar_data_msg.distance = data[key]
                    case "velocity":
                        radar_data_msg.velocity = data[key]
            radar_return_msg.returns.append(radar_data_msg)
        return radar_return_msg

    def scan(self):
        returns = self.build_radar_message()
        self.radar_pub.publish(returns)


def main():
    rclpy.init(args=None)
    radar = Radar()

    for vessel in radar.get_targets():
        radar.create_subscription(CustomPoseStamped, f"/{vessel}/pose_stamped", radar.poses_callback, 10)
        radar.create_subscription(Trajectory2D, f"/{vessel}/predicted_trajectory", radar.trajectories_callback, 10)
        radar.create_subscription(CustomFloat64, f"/wamv/distance_to_obstacle/{vessel}", radar.distance_callback, 10)
        radar.create_subscription(CustomTwist2DStamped, f"/{vessel}/velocity_3dof_ned", radar.velocity_ned_callback, 10)

    try:
        rclpy.spin(radar)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        radar.destroy_node()
        rclpy.shutdown()


if __name__ == "main":
    main()
