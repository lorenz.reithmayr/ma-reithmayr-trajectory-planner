import traceback
from math import sin, cos
from itertools import product

import numpy as np
import numpy.typing as npt
# noinspection PyUnresolvedReferences
from custom_msgs.msg import Trajectory2D, Trajectory2DArray

import rclpy
# noinspection PyUnresolvedReferences
from custom_interfaces.srv import SendEscapeTrajectories
from std_msgs.msg import Float64
from geometry_msgs.msg import Pose2D, PoseStamped, Twist
from nav_2d_msgs.msg import Pose2DStamped, Twist2DStamped
from nav_msgs.msg import Path
from rclpy.node import Node

"""
Uses theta (Euler angle, rad) for orientation
"""


class TrajectoryGenerator:
    def __init__(self, dt=0.5, pred_horizon=10):
        self.dt = dt
        self.pred_horizon = pred_horizon
        self.poses = []
        self.cmd_vel = None

    def generate_trajectory(self, pose: npt.NDArray, vel: npt.NDArray, accel: npt.NDArray):
        self.poses.clear()
        self.poses.append(pose)
        self.cmd_vel = self.compute_next_velocity(vel, accel)
        for _ in enumerate(range(self.pred_horizon)):
            pose = self.compute_next_pose(pose, self.cmd_vel)
            self.poses.append(pose)
        return self.to_Trajectory2D()

    def compute_next_velocity(self, vel, accel):
        u = vel[0]
        v = vel[1]
        r = vel[2]
        accel_u = accel[0]
        accel_v = accel[1]
        accel_r = accel[2]
        u_next = u + accel_u * self.dt
        v_next = v + accel_v * self.dt
        r_next = r + accel_r * self.dt
        return np.array([u_next, v_next, r_next])

    def compute_next_pose(self, pose, vel):
        x_current = pose[0]
        y_current = pose[1]
        theta_current = pose[2]
        u = vel[0]
        v = vel[1]
        r = vel[2]

        # Note: x_next and y_next are implicitly converted to {n} from {b}
        x_next = x_current + (u * cos(theta_current) + v * cos(theta_current)) * self.dt
        y_next = y_current + (u * sin(theta_current) + v * sin(theta_current)) * self.dt
        theta_next = theta_current + r * self.dt
        return np.array([x_next, y_next, theta_next])

    def to_Trajectory2D(self) -> Trajectory2D:
        traj_2d_msg = Trajectory2D()
        traj_2d_msg.header.frame_id = "map"
        traj_2d_msg.dt = self.dt
        cmd_vel = Twist()
        cmd_vel.linear.x = self.cmd_vel[0]
        cmd_vel.linear.y = self.cmd_vel[1]
        cmd_vel.angular.z = self.cmd_vel[2]
        traj_2d_msg.cmd_vel = cmd_vel
        for pose in self.poses:
            pose_msg = Pose2D()
            pose_msg.x = pose[0]
            pose_msg.y = pose[1]
            pose_msg.theta = pose[2]
            traj_2d_msg.poses.append(pose_msg)
        return traj_2d_msg

    def set_prediction_horizon(self, val: int):
        self.pred_horizon = val

    @staticmethod
    def to_PathFromTrajectory2D(t: Trajectory2D) -> Path:
        path_msg = Path()
        path_msg.header.frame_id = "map"
        for pose in t.poses:
            pose_msg = PoseStamped()
            pose_msg.header.frame_id = path_msg.header.frame_id
            pose_msg.pose.position.x = pose.x
            pose_msg.pose.position.y = pose.y
            pose_msg.pose.position.z = 0.0
            path_msg.poses.append(pose_msg)
        return path_msg


class TrajectoryGeneratorNode(Node):
    def __init__(self):
        super().__init__("trajectory_generator")
        self.declare_parameters(namespace='', parameters=[('update_frequency', rclpy.Parameter.Type.INTEGER),
                                                          ('dt', rclpy.Parameter.Type.DOUBLE),
                                                          ('prediction_horizon', rclpy.Parameter.Type.INTEGER),
                                                          ('max_accel_x', rclpy.Parameter.Type.DOUBLE),
                                                          ('max_accel_theta', rclpy.Parameter.Type.DOUBLE),
                                                          ('max_decel_x', rclpy.Parameter.Type.DOUBLE),
                                                          ('max_decel_theta', rclpy.Parameter.Type.DOUBLE),
                                                          ('dwa_dt', rclpy.Parameter.Type.DOUBLE),
                                                          ('dwa_dt', rclpy.Parameter.Type.DOUBLE),
                                                          ('dwa_linear_acc_range', rclpy.Parameter.Type.INTEGER),
                                                          ('dwa_angular_acc_range', rclpy.Parameter.Type.INTEGER),
                                                          ("dwa_prediction_horizon", rclpy.Parameter.Type.INTEGER)])
        self.update_freq = self.get_parameter('update_frequency').get_parameter_value().integer_value
        self.dt = self.get_parameter('dt').get_parameter_value().double_value
        self.pred_horizon = self.get_parameter('prediction_horizon').get_parameter_value().integer_value
        self.dwa_pred_horizon = self.get_parameter('dwa_prediction_horizon').get_parameter_value().integer_value
        self.max_accel_u = self.get_parameter('max_accel_x').get_parameter_value().double_value
        self.max_accel_r = self.get_parameter('max_accel_theta').get_parameter_value().double_value
        self.max_decel_u = self.get_parameter('max_decel_x').get_parameter_value().double_value
        self.max_decel_theta = self.get_parameter('max_decel_theta').get_parameter_value().double_value
        self.dwa_linear_range = self.get_parameter('dwa_linear_acc_range').get_parameter_value().integer_value
        self.dwa_angular_range = self.get_parameter('dwa_angular_acc_range').get_parameter_value().integer_value
        self.dwa_dt = self.get_parameter('dwa_dt').get_parameter_value().double_value

        self.timer = self.create_timer(1 / self.update_freq, self.event_loop)

        self.wamv_trajectory_generator = TrajectoryGenerator(self.dt, self.pred_horizon)
        self.dwa_trajectory_generator = TrajectoryGenerator(self.dwa_dt, self.dwa_pred_horizon)

        self.current_vel = None
        self.current_vel_ned = None
        self.total_window = np.array([0.0, 0.0, 0.0])
        self.moving_avg_vel = None
        self.counter = 0
        self.cte = None

        self.current_pose = None
        self.current_accel = np.array([0., 0, 0])
        self.track_vector = None
        self.track_vector_msg = None
        self.escape_trajectories = []
        self.escape_trajectories_msg = None
        self.escape_path_msg = None

        self.track_vector_pub = self.create_publisher(Trajectory2D, "/wamv/track_vector", 10)
        self.trajectory_pub = self.create_publisher(Trajectory2DArray, "/wamv/escape_trajectories", 10)
        self.trajectory_path_pub = self.create_publisher(Path, "/wamv/escape_trajectories_paths", 10)
        self.escape_traj_srv = self.create_service(SendEscapeTrajectories, 'escape_trajectories_service',
                                                   self.escape_trajectories_srv_callback)

    def pose2d_callback(self, pose2d_msg: Pose2DStamped):
        self.current_pose = np.array([pose2d_msg.pose.x, pose2d_msg.pose.y, pose2d_msg.pose.theta])

    def cte_callback(self, cte):
        self.cte = np.abs(cte.data)

    def twist2d_callback(self, twist2d_msg: Twist2DStamped):
        self.current_vel = np.array([twist2d_msg.velocity.x, twist2d_msg.velocity.y, twist2d_msg.velocity.theta])
        self.smooth_input_velocites()
        self.set_dynamic_prediction_horizon()

    # noinspection PyUnusedLocal
    def escape_trajectories_srv_callback(self, request, response):
        self.generate_escape_trajectories(self.moving_avg_vel)
        if self.escape_trajectories_msg is None:
            response.success = False
            return response
        response.trajectories = self.escape_trajectories_msg
        response.success = True
        return response

    def event_loop(self):
        if self.current_vel is None:
            return
        self.generate_track_vector(np.array([self.moving_avg_vel[0], 0.0, 0.0]))
        self.track_vector_pub.publish(self.track_vector_msg)
        # self.generate_escape_trajectories(np.array([self.current_vel[0], 0., 0.]))
        self.generate_escape_trajectories(self.moving_avg_vel)

    def smooth_input_velocites(self):
        self.total_window += self.current_vel
        self.counter += 1
        self.moving_avg_vel = self.total_window / self.counter
        while self.counter == 10:
            self.counter = 0
            self.moving_avg_vel = self.current_vel
            self.total_window = np.array([0.0, 0.0, 0.0])

    def set_dynamic_prediction_horizon(self):
        if self.cte is None:
            return
        cte_max = 3.0
        ph = self.pred_horizon * (1 - (1 / cte_max) * int(self.cte))
        self.wamv_trajectory_generator.set_prediction_horizon(int(ph))

    """
    Interpolates (linearly) the maximum achievable acceleration as a function of the current velocity.
    Maximum values:
        u_max ~ 4.8 m/s
        r_max ~ +/- 1.55 rad/s
    """

    def linterp_accels(self, vel):
        u = vel[0]
        r = vel[2]
        u_max = 4.8
        r_max = 1.55
        acc_u = self.max_accel_u * (1 - (1 / u_max) * u)
        dec_u = (self.max_decel_u / u_max) * u
        acc_theta = self.max_accel_r * (1 - (1 / r_max) * r)
        dec_theta = -acc_theta

        accels = np.insert(list(product(np.linspace(dec_u, acc_u, self.dwa_linear_range),
                                        np.linspace(acc_theta, dec_theta, self.dwa_angular_range))), 1, 0.0, axis=1)
        return accels

    def generate_track_vector(self, vel):
        self.track_vector_msg = self.wamv_trajectory_generator.generate_trajectory(self.current_pose, vel,
                                                                                   np.array([0., 0, 0]))

    def generate_escape_trajectories(self, vel):
        accels = self.linterp_accels(vel)
        self.escape_trajectories_msg = Trajectory2DArray()
        self.escape_trajectories_msg.trajectories = [
            self.dwa_trajectory_generator.generate_trajectory(self.current_pose, vel, x) for x in accels]
        self.trajectory_pub.publish(self.escape_trajectories_msg)


def main():
    rclpy.init(args=None)
    trajectory_generator = TrajectoryGeneratorNode()
    trajectory_generator.create_subscription(Pose2DStamped, "/wamv/pose_3dof", trajectory_generator.pose2d_callback, 10)
    trajectory_generator.create_subscription(Twist2DStamped, "/wamv/velocity_3dof",
                                             trajectory_generator.twist2d_callback, 10)
    trajectory_generator.create_subscription(Float64, "/wamv/crosstrack_error", trajectory_generator.cte_callback, 10)

    try:
        rclpy.spin(trajectory_generator)
    except Exception as exception:
        traceback_logger_node = Node('node_class_traceback_logger')
        traceback_logger_node.get_logger().error(traceback.format_exc())
        raise exception
    else:
        trajectory_generator.destroy_node()
        rclpy.shutdown()


if __name__ == "main":
    main()
