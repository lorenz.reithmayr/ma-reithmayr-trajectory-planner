# A Trajectory Planning Module for Dynamic Obstacle Avoidance in Autonomous Maritime Applications
## MA Lorenz Reithmayr

### Requirements

* ROS 2 Humble Hawksbill
* Gazebo Garden
* VRX Virtual RobotX (https://github.com/osrf/vrx)
* Matlab R2023b
* Navigation2 (Nav2) for ROS 2
* C++ 17
* Python 3.10

### Dependencies
* pysdf (pip install python-sdformat)
* PyYAML
* ros_gz (https://github.com/gazebosim/ros_gz/tree/humble)

### Build & Installation
* Install and source ROS 2 Humble
* Install and source Gazebo Garden 
* Install and source VRX
* Install and source ros_gz 
* pip install python-sdformat
* Clone the repo: 


'''

    git clone https://git.rwth-aachen.de/BKrautAc/studi_git.git
    git checkout Studi_Git_Lorenz_Reithmayr
    cd Studi_Git_Lorenz_Reithmayr
    colcon build
'''

* Open ~/.bashrc with a text editor and paste the following lines, substituting the correct paths on your machine
* Tip: I suggest creating a folder like "ros2_ws" in your home directory where you'll build all the dependencies you need from source


'''

    # Source ROS Humble Hawkbill
    source /opt/ros/humble/setup.bash

    # Source ros_interfaces
    source $PATH_TO_ROS_WORKSPACE/ros2_gz_ws/install/setup.bash

    # Gazebo environment variables
    export GZ_VERSION=garden
    export GZ_SIM_RESOURCE_PATH=$PATH_TO_REPO/models

    # Source vrx Gazebo environment
    source $PATH_TO_ROS_WORKSPACE/vrx_ws/install/setup.bash

    # Source MA ROS project
    source Studi_Git_Lorenz_Reithmayr/install/setup.bash
'''

* From the repo base directory, run the Simulink control system model

'''

    bash Studi_Git_Lorenz_Reithmayr/launch/run_simulink_model.sh
'''

* Start the system via the launch file

'''

    ros2 launch /home/lorenz/Projects/MA/ma-reithmayr-trajectory-planner/launch/path_planner.launch.py
'''

* Useful Bash aliases:

'''

    alias topics='ros2 topic list'
    alias nodes='ros2 node list'
    alias re='ros2 topic echo $1'
    alias ri='ros2 topic info $1'
    alias rospub='ros2 topic pub $1 $2 $3' # $1: Topic name, $2: Type $3: msg 'data: abcd'
    alias custom_wamv='ros2 launch vrx_gazebo generate_wamv.launch.py component_yaml:="$PWD"/custom_wamv_config.yaml wamv_target:=`pwd`/wamv_target.urdf wamv_locked:=False'

'''