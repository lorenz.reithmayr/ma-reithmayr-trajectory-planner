from pprint import pprint as pp
from itertools import product

import matplotlib.pyplot as plt
import numpy as np
from src.trajectory_planner.trajectory_planner.trajectory_generator import TrajectoryGenerator
from custom_msgs.msg import Trajectory2D


def linterp_accels(vel):
    u = vel[0]
    r = vel[2]
    u_max = 4.8
    r_max = 1.55
    max_accel_u = 2.8
    max_decel_u = -2.3
    max_accel_r = 0.5

    acc_u = max_accel_u * (1 - (1 / u_max) * u)
    dec_u = (max_decel_u / u_max) * u
    acc_theta = max_accel_r * (1 - (1 / r_max) * r)
    dec_theta = -acc_theta

    accels = np.insert(list(product(np.linspace(dec_u, acc_u, 2), np.linspace(acc_theta, dec_theta, 60))), 1, 0.0,
                       axis=1)
    return accels


tg = TrajectoryGenerator(0.5, 14)

pose = np.array([0., 0., 1.5])
nu = np.array([4.0, 0.0, 0.0])

acc = linterp_accels(nu)

dwa_t = [tg.generate_trajectory(pose, nu, a) for a in acc]

for t in dwa_t:
    x = [pose.x for pose in t.poses]
    y = [pose.y for pose in t.poses]
    theta = [pose.theta for pose in t.poses]
    plt.plot(x, y, "-")
    pp(t.cmd_vel)

plt.show()
