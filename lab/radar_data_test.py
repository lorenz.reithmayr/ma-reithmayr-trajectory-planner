from geometry_msgs.msg import Pose
# noinspection PyUnresolvedReferences
from custom_msgs.msg import Trajectory2D
from std_msgs.msg import Float64
import pprint

pose1 = Pose()
pose1.position.x = 4.0
pose1.position.y = 2.0

pose2 = Pose()
pose2.position.x = 7.0
pose2.position.y = 2.0

dist1 = Float64()
dist1.data = 123.5
dist2 = Float64()
dist2.data = 153.5


class Test:
    def __init__(self):
        self.vessels = ["vessel1", "vessel2", "vessel3"]
        self.radar_targets = {}
        [self.create_entry(target) for target in self.vessels]

    def create_entry(self, target):
        self.radar_targets[target] = {}

    def update_pose(self, name, pose):
        self.radar_targets[name]["pose"] = pose

    def update_dist(self, name, dist):
        self.radar_targets[name]["dist"] = dist.data


test = Test()
test.update_pose('vessel1', pose1)
test.update_dist('vessel1', dist1)
test.update_dist('vessel2', dist2)
test.update_pose('vessel2', pose2)
test.update_pose('vessel2', pose1)
test.update_pose('vessel2', pose2)

pprint.pprint(test.radar_targets)

for vessel, data in test.radar_targets.items():
    print(f"Vessel Name: {vessel}")

    for key in data:
        print(f"{key}: {data[key]}")
