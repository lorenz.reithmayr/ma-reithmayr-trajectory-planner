import yaml
from pysdf import SDF
from pprint import pprint as pp

world_dir = ("/home/lorenz/Projects/MA/ma-reithmayr-trajectory-planner/src/trajectory_planner/gz_files/world"
             "/sydney_regatta_OBSTACLE.sdf")

params_yaml = "/home/lorenz/Projects/MA/ma-reithmayr-trajectory-planner/lab/radar_params_test.yaml"

world_file = SDF.from_file(world_dir, remove_blank_text=True)

vessels = [model.name for model in world_file.iter("include") if model.name.__contains__("vessel")]

with open(params_yaml, "r") as f:
    params_dict = yaml.safe_load(f)

params_dict["radar"]["ros__parameters"]["obstacle_vessels"] = vessels

print(yaml.safe_dump(params_dict, default_flow_style=False, default_style="None", sort_keys=False))

with open(params_yaml, "w") as f:
    yaml.dump(params_dict, f)
