import numpy as np
import matplotlib.pyplot as plt

from math import sin, cos
from filterpy.stats import plot_covariance


class MotionModel:
    def __init__(self):
        self.dt = 1
        self.vel_total = 5
        self.F = np.array([[1., 0, self.dt, 0], [0, 1., 0, self.dt], [0., 0, 1, 0], [0., 0, 0, 1]])

        self.predictions = None
        self.covariances = None

    def predict(self, pred_horizon: int):
        # This data would come frome the vessel pose publisher
        self.predictions = []
        self.covariances = []
        pos_x = 5.0
        pos_y = 20.0
        pos_theta = 0.0
        vel_x = self.vel_total * cos(pos_theta)
        vel_y = self.vel_total * sin(pos_theta)
        x = [pos_x, pos_y, vel_x, vel_y]
        self.predictions.append(x)
        print("F:\n{}".format(self.F))

        P = np.diag([1.0, 1.0, 2.0, 2.0])
        self.covariances.append(P)
        print("P:\n{}".format(P))

        mean = x[0:2]
        # cov = np.array([[P[0, 0], P[0, 2]], [P[2, 0], P[2, 2]]])
        ev_sub_1 = np.linalg.eig(P[0:2, 0:2])
        ev_sub_2 = np.linalg.eig(P[0:2, 2:4])
        ev_sub_3 = np.linalg.eig(P[2:4, 0:2])
        ev_sub_4 = np.linalg.eig(P[2:4, 2:4])
        cov = np.array([[ev_sub_1.eigenvalues[0], ev_sub_2.eigenvalues[0]],
                        [ev_sub_3.eigenvalues[0], ev_sub_4.eigenvalues[0]]])
        print("cov:\n {}".format(cov))
        plot_covariance(mean, cov, edgecolor='r')

        for i, _ in enumerate(range(pred_horizon)):
            x = self.F @ x
            P = self.F @ P @ self.F.T
            self.predictions.append(x)
            self.covariances.append(P)
            mean = x[0:2]
            cov = np.array([[P[0, 0], P[0, 2]], [P[2, 0], P[2, 2]]])
            print("P_{}:\n{}".format(i, P))
            print("cov:\n {}".format(cov))
            plot_covariance(mean, cov, edgecolor='k', ls='dashed')


if __name__ == "__main__":
    motion_model = MotionModel()
    motion_model.predict(5)
    pred_x, pred_y, _, _ = zip(*motion_model.predictions)
    # plt.plot(pred_x, pred_y, '*')

    plt.show()
