class Test:
    def __init__(self, x, y):
        self.x = x
        self.y = y


test1 = Test(5.0, 6.0)
test2 = Test(4.6, 56.3)
list1 = [test1, test2]

closest = float("inf")
closest = lambda data, cl=closest: cl if cl < data.distance else data.distance

