import heapq
import os
from timeit import default_timer as timer

import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint


class AStarNode:
    def __init__(self, x, y, g=0.0, h=0, parent=None):
        self.x = x
        self.y = y
        self.position = [x, y]
        self.g = g  # Cost from start to node
        self.h = h  # Heuristic function (distance from node to goal)
        self.f = self.g + self.h
        self.parent = parent

    def __key(self):
        return self.position[0], self.position[1]

    def __hash__(self):
        return hash(self.__key())

    def __eq__(self, other):
        return self.position == other.position

    def __lt__(self, other):
        return self.f < other.f

    def __gt__(self, other):
        return self.f > other.f


def grid_to_gt(grid_row, grid_col, x_origin, y_origin, res):
    x_gt = x_origin + grid_col * res
    y_gt = y_origin + grid_row * res
    return x_gt, y_gt


def gt_to_grid(x_gt, y_gt, x_origin, y_origin, res_inverse):
    grid_row = (y_gt - y_origin) * res_inverse
    grid_col = (x_gt - x_origin) * res_inverse
    return int(grid_col), int(grid_row)


def load_map(path):
    return np.loadtxt(path)


def heuristic(node: AStarNode, goal: AStarNode):
    # Euclidian norm (distance between two nodes)
    return np.linalg.norm([node.x - goal.x, node.y - goal.y])


def get_neighbours(node: AStarNode):
    node_x = node.x
    node_y = node.y
    return [[AStarNode(node_x + 1, node_y), 1], [AStarNode(node_x - 1, node_y), 1], [AStarNode(node_x, node_y + 1), 1],
            [AStarNode(node_x, node_y - 1), 1], [AStarNode(node_x + 1, node_y + 1), 1.414],
            [AStarNode(node_x - 1, node_y - 1), 1.414], [AStarNode(node_x - 1, node_y + 1), 1.414],
            [AStarNode(node_x + 1, node_y - 1), 1.414]]


def a_star(start_cell, goal_cell, ogm):
    start_node = AStarNode(x=start_cell[0], y=start_cell[1])
    goal_node = AStarNode(x=goal_cell[0], y=goal_cell[1])
    start_node.h = heuristic(start_node, goal_node)
    rows, cols = ogm.shape

    open_heapq = []
    open_set = set()
    closed_set = set()
    heapq.heappush(open_heapq, (start_node.f, start_node.g, start_node))
    open_set.add(start_node)

    while open_heapq:
        current_node = heapq.heappop(open_heapq)[2]
        open_set.remove(current_node)
        closed_set.add(current_node)
        if current_node == goal_node:
            return get_path(current_node)
        for nb in get_neighbours(current_node):
            nb_node = nb[0]
            nb_delta_g = nb[1]
            if not is_traversible(nb_node, ogm, rows, cols):
                continue
            g_temp = current_node.g + nb_delta_g
            if nb_node in closed_set:
                continue
            elif nb_node in open_set:
                if nb_node.g < g_temp:
                    nb_node.g = g_temp
            else:
                nb_node.h = heuristic(nb_node, goal_node)
                nb_node.g = g_temp
                nb_node.parent = current_node
                open_set.add(nb_node)
                heapq.heappush(open_heapq, (nb_node.f, nb_node.g, nb_node))
    return None


def get_path(current_node):
    path = []
    while current_node is not None:
        path.append(current_node.position)
        current_node = current_node.parent
    return np.flip(path)


def is_traversible(node, ogm, rows, cols):
    if ogm[node.y, node.x] == (254 or 253 or 255) or not (0 <= node.x < cols) or not (0 <= node.y < rows):
        return False
    return True


def return_path(cur):
    path = []
    while cur is not None:  # Go down the parents list until we find the goal!
        path.append(cur.position)
        cur = cur.parent
    return path[::-1]  # Reverse


def main():
    plot_path = True
    gridmap = load_map(os.path.join(os.getcwd(), "maps", "costmap.gz"))
    pprint(gridmap)
    x_start, y_start = (510, 396)
    x_goal, y_goal = (295, 503)  # In grid coordinats for testing

    if plot_path:
        start_lr = timer()
        path_lr = a_star((x_start, y_start), (x_goal, y_goal), gridmap)
        end_lr = timer()
        print("Time LR [sec]: {}".format(end_lr - start_lr))

        path_y_lr, path_x_lr = zip(*path_lr)
        plt.plot(x_start, y_start, "or", markersize=2)
        plt.plot(x_goal, y_goal, "ob", markersize=2)
        plt.plot(path_x_lr, path_y_lr, "og", markersize=0.5)

    plt.imshow(gridmap, cmap='binary')
    plt.gca().invert_yaxis()
    plt.show()


main()
