from itertools import product

import numpy as np
from trajectory_planner.common.lib_transformations import vel_body_to_ned
import matplotlib.pyplot as plt

dt = 0.5
pred_horizon = 10

m = 180.0
X_u_dot = -0.05
x_G = 0.0
y_g = 0.0
Y_v_dot = 0.9
Y_r_dot = 0.5
N_v_dot = 0.5
I_z = 250.0
N_r_dot = 1.2
Y_v = -0.5

M_RB = np.array([[m, 0, 0], [0, m, m * x_G], [0, m * x_G, I_z]])
M_A = np.array([[-X_u_dot, 0, 0], [0, -Y_v_dot, -Y_r_dot], [0, -Y_r_dot, -N_r_dot]])
M = M_RB + M_A
M_inv = np.linalg.inv(M)
D = np.zeros((3, 3))

tau_X_min = 0.0
tau_X_max = 250.0
tau_Y_min = 0.0
tau_Y_max = 400.0
tau_N_min = -500.0
tau_N_max = 500.0
num = 5

taus = list(product(np.linspace(tau_X_min, tau_X_max, num), np.linspace(tau_Y_min, tau_Y_max, num),
                    np.linspace(tau_N_min, tau_N_max, num)))


# taus = [tau]
t = []
for tau in taus:
    pose = np.array([-517.0, 240.0, -1.5])
    nu = np.array([4.0, 0, 0])
    traj = []
    for i, _ in enumerate(range(pred_horizon)):
        C_RB = np.array(
            [[0, 0, -m * (x_G * nu[2] + nu[1])], [0, 0, m * nu[0]], [m * (x_G * nu[2] + nu[1]), - m * nu[0], 0]])
        C_A = np.array([[0, 0, Y_v_dot * nu[1] + Y_r_dot * nu[2]], [0, 0, -X_u_dot * nu[0]],
                        [-Y_v_dot * nu[1] - Y_r_dot * nu[2], X_u_dot * nu[0], 0]])
        C = C_RB + C_A
        D[0][0] = 72.4 * np.square(nu[0]) + 51.3 * nu[0]
        nu_dot = np.matmul(M_inv, tau - np.matmul(C, nu) - np.matmul(D, nu))
        # nu_dot = np.matmul(M_inv, tau - np.matmul(C, nu))
        nu = nu + nu_dot * dt
        print(nu)
        eta_dot = vel_body_to_ned(nu[0], nu[1], nu[2], pose[2] + nu[2] * dt)
        pose = pose + eta_dot * dt
        traj.append(pose)
        tau = np.zeros(3)
    t.append(traj)

for traj in t:
    for p in traj:
        x, y, theta = p
        plt.plot(x, y, 'ro-', linewidth=1, markersize="1")

plt.show()
