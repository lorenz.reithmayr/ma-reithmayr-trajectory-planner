Wrong Config:

Left THR --> Axis 1 --> LS Y --> js-calib: Axis 1
Right THR --> Axis 3 --> RS X --> js-calib: Axis 4
Left POS --> Axis 2 --> LT --> js-calib: Axis 5
Right POS --> Axis 4 --> RS Y --> js-calib: Axis 3


Correct Config:

Left THR --> Axis 1 
Right THR --> Axis 4 
Left POS --> Axis 0 
Right POS --> Axis 3 

Single-stick config (much easier to drive):

(Left + Right) Trust: RS Y --> Axis 4
(Left + Right) Position: LS X --> Axis 0
