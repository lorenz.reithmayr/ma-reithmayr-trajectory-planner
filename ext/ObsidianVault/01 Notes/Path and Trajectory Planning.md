Overview: [[SPG21]]
## Path vs Trajectory
### Path
+ Collection of points/curves in C-Space connection a start point and a goal
+ Does not take into account vehicle dynamic constraints
### Trajectory
+ A path that can be feasibly executed by the vehicle taking into account dynamic constraints
	==> Path Planning vs. Trajectory/Motion Planning

## Two-step approach 

+ **Path planner** selects a *path* avoiding known obstacles
	+ A* and variations
	+ RRT and variations
	+ Biological algorithms
+ **Optimizer** turns paths into *trajectory* that can be followed considering vehicle constraints and selects an *optimal trajectory*

## Further subdivision into global and local planning

+ Offline **global** optimization of an initial path 
+ Online **local** adjustment of the pre-planned path to avoid dynamic obstacles

## Anytime algorithms

+ Compute non-optimal and non-complete solution in a certain timeframe
+ Solution converges on optimality over large enough time scales
+ A non-optimal solution is available at any time step --> **Anytime Algorithms**

## Problems with graph search algorithms
[Rahul (2016): On-Road Intelligent Vehicles]

+ Environment must be fully observable
+ Environment must be (mostly) static
+ Environment must be discretized
+ Completeness and optimality are resolution-dependent