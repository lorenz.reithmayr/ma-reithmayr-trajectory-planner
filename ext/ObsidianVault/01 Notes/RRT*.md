+ Sampling the same as in RRT
	+ Point randomly sampled from C-space 
	+ If distance from new point to current point **smaller** than *maximum distance*
	+ If distance from new point to current point **greater** than *maximum distance*
	  --> Node created at maximum distance along the path between current node and sampled node

+ Every node is assigned a cost function denoting the *shortest length from start node to the node*
+ At the newly sampled point, search for nodes inside a circle with radius r
	+ Rearrange connections to nodes within the circle and all other nodes to minimize the path length
![[Pasted image 20231122142253.png]] 

## Pseudocode

![[Pasted image 20231122145103.png]]

+ SampleFree():
	+ Sample obstacle-free C-Space and return sample point
+ Nearest():
	+ Returns node v in V (set of all vertices in G) of the Graph G = (V, E) that is nearest to the sampled node (Euclidian Distance)
 + Near():
	 + Find nodes in graph within fixed radius r
+ Steer():
	+ Create new node at either sampled node or at max distance along path to sampled node
+ Line():
	+ Straight line between two points
+ Parent():
	+ Parent node of a given tree node
+ Cost():
	+ Assigns each node a cost according to the length of the path from the origin
 
		![[Pasted image 20231122145329.png]]
+ CollisionFree(x, y)
	+ True, if line segment btw.  x and y is collision free

![[Pasted image 20231122145541.png]]