## 2023-10-23

### Global Planning
+ RRT* (AR, maybe)
+ Optimal-Control-based global optimization step to model vehicle dynamics
+ Inspired by: WSL23

### Local Planning for Obstacle Avoidance
+ DWA
+ Incorporation of global path to guard against local minima
+ Inspired by: SP07