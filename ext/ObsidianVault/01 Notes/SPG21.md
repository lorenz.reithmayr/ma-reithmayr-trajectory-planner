## Classification Scheme

	![[Pasted image 20231019141134.png]]

- Broad classification in **global planning** and **local planning**   
- Four categories
	- **Reactive Computing
		 - Local Optimization
		 - Reactive Maneuvering
	- **Soft Computing
		 - Evolutionary Computation
		 - AI
	- **C-Space Search 
		 - Graph Search Algorithms
		 - Sampling Based Graph Search
	- **Optimal Control
		 - Global Optimization
		 - PDE Solving

## Environment Modeling

+ Construction of **graph** that represents environment 
+ Maps are mostly **metric maps** 
	+ Other kinds exists --> *Topological*  or *semantic* maps
+ Two main approaches to building graphs: **Cell Decomposition** and **Road Maps**

	![[Pasted image 20231019091554.png]]

+ **Cell Decomposition**
	+ **Grid-based CD**
		+ Partitioning the surface/space into cells
		+ Regular or irregular grids possible
		+ Squares, Triangle or Hexagons possible
		+ *Advantages*
			+ Fast indexation
			+ Nice layout in memory
		+ *Disadvantages*
			+ Paths are constrained to grids --> Less smooth
	+ **Navigation Meshes**  and **Circle-based Waypoint Graphs**
+ **Road Maps**
	+ Graph structure with nodes and edges
	+ Voronoi Graphs, Visibility Graphs, State-Lattice Graphs
	+ **Edges costs/states** may be based on **motion models** to ensure traversal is feasible
+ Grid size can be varied according to the planning task
   --> **Smaller grids** for local planning, **larger grids** for global planning
   --> Layered grid design
+ Assigning costs of traversal via **Cost Maps**
	+ *Uniform* or *non-uniform* cost maps possible
+ Path planning algorithms can also take **steering configurations** into account when planning feasible paths

## Reactive Computing
### Reactive Maneuvering

	![[Pasted image 20231019115353.png]]

+ Local Planning
+ Based on **use of fields** to deal with obstacle positions, **detection of obstacle boundaries** to circumvent or **production of a velocity command** after evaluating available free space
+ **Artificial Potential Fields (APF)**
	+ Obstacles create repulsive forces that repel the robot
	+ Target positions create an attractive force
	+ *Disadvantage*
		+ Robot may become stuck in local minimum 
		--> Combination with other methods like RRTs or evolutionary algorithms possible
		--> Custom escape paths on detection of being stuck
		--> Electrostatic potential field instead of virtual forces
+ **Vector Field Histogram (VFH)**
	+ Polar Histogram to evaluate density of obtacle around the robot and selecting **steering angle with lowest obstacle density**
+ **Bug Algorithms**
+ **Velocity Obstacles Method**
	+ Takes into account velocity vectors of robot and obstacle to calculate a safe path around obstacle
	+ Creates *Cone of Collision* around the obstacle
	+ Extensible with *Fast Marching Squares* algorithm and different vehicle models
+ **Dynamic Window Approach (DWA)**
	+ Searches for a feasable velocity command inside speed and/or time window
	+ Locally optimal
	+ Optimization: Reduce velocity search space
	+ Energy minimization path planning possible

### Local Optimization

+ Start from **pre-existing path** and **modify** it according to local obstacles
+ Low **computational effort** at **expense** of optimality or completeness
+ **Velocity Obstacle Method** also fall under this category
+ **Elastic Bands Method**: Deform existing path 
	+ Stretching and elongating path around obstacle
	+ Create *bubbles* around nodes of the graph that cover collision-free areas
	+ Bubble size is bounded by an obstacle --> Many smaller bubbles close to obstacle
	+ Adapted to non-holonomic vehicles by using *Bezier curves*
	+ *Timed Elastic Bands* incorporates time constraints impose by dynamic restrictions of the robot

## C-Space-Search Algorithms

### Graph Search Algorithms

	![[Pasted image 20231019115256.png]]

+ Graph-search algorithms fully (or partially) visit the whole discretized C-Space graph until a path is found
+ **Edge-restricted** Graph Search: Variations on the **Dijktra Algorithm**
  --> Angle of path from node to node restricted by the corners of the grid
	+ A*
	+ D*
		+ Incremental version of A*
		+ Recycles previous computations when cost of grid nodes changes
		+ Prevents whole new computation of entire path
	+ Focussed D*
		+ Further Reduction in computation time
	+ LPA*
	+ Anytime versions of A*
		+ Use of a fixed time frame for planning + Best path found after time x is used + Hybrid A* prioritizes feasibility of path over optimality or completeness
+ **Any Angle Algorithms**
  --> Not restricted by angles, since waypoints do not have to be placed in neighbouring nodes
	+  Field-D* (Mars rovers)
	+ Theta*

	![[Pasted image 20231019115700.png]]

### Sampling-based Algorithms

	![[Pasted image 20231019121027.png]]

+ Create samples of C-Space sequentially according to policy
+ Retreive the path from the created samples after reaching a set of conditions (e.g. time limit)
+ **Single-query**: Only two points are considered (pose and goal)
	+ **Rapidly Exploring Random Tree (RRT)**
		+ Special case of Rapidly Exploring Deterministic Tree (RDT)
		+ Dynamic creation of samples from starting point
		+ As soon as sample is closer to goal than a certain threshold, the path is retreived by backtracking until reaching the origin
	+ **Dynamic Domain RRT** is aware that obstacles exist during creation
+ **Multiple-query**: More than two points are considered during creation
	 + **Probabilistic Roadmap (PRM)**
		 + Samples scattered over C-Space
		 + New samples created by creating tree from initial samples
		 + Retrieving the path with A*
	 + **Fast Marching Tree (FMT*)**
		 + Combination of RRT and PRM with *Optimal Control* algorithm FMM (Fast Marching Method)
	 + **Group Marching Tree (GMT*)**
+ Sampling approach can be coupled with **MPC** to account for kinematic and dynamic constraints

## Optimal-Control-Based Path Planning Algorithms

### PDE Solving

+ Based on Dynamic Programming
+ Solving HJB or Eikonal equation on a grid
+ Numerical solution of propagation of a wave over the grid
+ Solving the Eikonal equation leads to a gradient where the path can be retrieved using gradient descent
+ **Fast Marching Method (FMM)**
	+ Dijkstra to visit node of the grid
	+ Assign cost value to each node based on solving Eikonal
	+ Resulting path is smooth, continuous and optimal
+ **Fast Marching Square (FMS)**
	+ Obstacle avoidance
	+ Computes FMM twice
	+ Assign repulsive field of costs around an obstacle

	![[Pasted image 20231019123102.png]]

### Global Optimization

+ Optimization of path globally
+ Existing path created by A*, RRT or another method is optimized using gradient optimization
+ Can be combined with MPC to calculate energy-minimal paths
+ Bang-bang approaches, Mixed-Integer Linear Programming

## Summary

	![[Pasted image 20231019123448.png]]

### Reactive Computing

+ Local obstacle avoidance
+ Cheap, easy to implement
+ **Reactive Maneuvering** good for high uncertainty, limited sensing
+ **Local Optimization** allows use of kinodynamic models

### C-Space Methods

+ **Graph based methods** good for global planning
+ **Sampling based methods** good for problems with higher dimensions

### Optimal Control 

+ Good for globally optimal solutions
+ **PDE** solvers depend highly on the PDE used
+ **Global Optimization** needs an already existing graph to optimize according to the robots locomotion restrictions

  
