import os
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint as pp

metrics_path = "{}/metrics".format(os.getcwd().split("/plots")[0])


def plot_a_star_time_over_length():
    fname = f"{metrics_path}/GlobalPathPlanner/a_star_metrics.txt"
    with open(fname, "r") as f:
        data = [line.rstrip("\n").split(":") for line in f if line.strip()]
        path_length = [float(d[1]) for d in data if d[0].__contains__("Path")]  # x axis
        execution_time = [float(d[1]) for d in data if d[0].__contains__("Time")]  # y axis

        z = np.polyfit(path_length, execution_time, 2)
        p = np.poly1d(z)
        print(p)

        fig, ax = plt.subplots(figsize=(8, 5), layout="constrained")
        ax.grid(True, linewidth=0.4)
        ax.set_xlabel("Path length / m")
        ax.set_ylabel("Execution time / s")
        ax.minorticks_on()
        ax.xaxis.set_major_formatter(lambda x, pos: "%.2f" % x)
        ax.yaxis.set_major_formatter(lambda x, pos: "%.2f" % x)
        ax.plot(path_length, execution_time, "o", color="0.1", markersize="3.0")
        ax.plot(np.sort(path_length), p(np.sort(path_length)), linewidth=1.0, color='lightsteelblue')
        ax.set_xlim(xmin=0)
        ax.set_ylim(ymin=0)
        plt.savefig(f"{os.getcwd()}/figures/runtime_astar.png")


def plot_dist_error_over_angle():

    def to_rad(angle):
        return angle if angle > 0 else angle + 2*np.pi

    fname = f"{metrics_path}/GlobalPathPlanner/path_dist_vs_euclidian.txt"
    with open(fname, "r") as f:
        data = [
            np.array(line.rstrip("\n").split(": ")[1].replace("(", "").replace(")", "").split(", ")).astype(np.float32)
            for line in f if line.strip()]
        pp(data)
        dist_diff = [(d[1] - d[2])/d[1] for d in data]
        r_max = np.max(dist_diff)
        dist_diff_norm = [dist/r_max for dist in dist_diff]
        angles = [d[0] for d in data]
        angles.append(angles[0])
        dist_diff_norm.append(dist_diff_norm[0])
        print(angles)
        print(dist_diff_norm)

        fig, ax = plt.subplots(subplot_kw={"projection": "polar"})
        ax.grid(True, linewidth=0.4)
        ax.set_rlabel_position(-22.5)
        ax.set_rlim(rmax=1.0)
        ax.plot(angles, dist_diff_norm, linewidth=1.0, color='lightsteelblue')
        ax.plot(angles, dist_diff_norm, "o", color="0.1", zorder=3, markersize='2.5')


def main():
    plot_a_star_time_over_length()
    # plot_dist_error_over_angle()
    plt.show()


if __name__ == "__main__":
    main()
