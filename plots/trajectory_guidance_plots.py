import os
from pprint import pprint as pp
import time
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from rosbags.rosbag2 import Reader
from rosbags.typesys import Stores, get_typestore


def load_map(path):
    return np.loadtxt(path)


def plot_costmap(path):
    origin = [-1080.378, -232.902, 0]
    x_off = -origin[0]
    y_off = -origin[1]
    extent = [-x_off, 1674 - x_off, -y_off, 949 - y_off]
    cstmap = load_map(path)
    plt.imshow(cstmap, cmap="BuPu", origin="lower", extent=extent)
    return cstmap


def plot_dwa_traj_bag(rosbag, los_active):
    typestore = get_typestore(Stores.ROS2_HUMBLE)

    with Reader(rosbag) as r:
        types = {}
        for c in r.connections:
            print(f"Topics in this bag: {c.topic}, Type: {c.msgtype}")
        typestore.register(types)

        t_gp = []
        connections = [x for x in r.connections if x.topic == "/global_path"]
        for connection, t, data in r.messages(connections=connections):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            wpts = [np.array([p.pose.position.x, p.pose.position.y]) for p in msg.poses]
            t_gp.append(t)
        gp_wpts_x, gp_wpts_y = zip(*wpts)

        t_dwa = []
        connections = [x for x in r.connections if x.topic == "/dwa_path"]
        for connection, t, data in r.messages(connections=connections):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            wpts = [np.array([p.pose.position.x, p.pose.position.y]) for p in msg.poses]
            t_dwa.append(t)
        wpts_x, wpts_y = zip(*wpts)

        gp_start = t_gp[0]
        dwa_start = t_dwa[0]
        dwa_duration = 7.5e9
        end = int(dwa_start + dwa_duration)

        u_cmd = []
        r_cmd = []
        t_cmd_vel = []
        connections = [x for x in r.connections if x.topic == "/wamv/cmd_vel_3dof"]
        for connection, t, data in r.messages(connections=connections, start=gp_start, stop=end):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            u_cmd.append(msg.twist.twist.linear.x)
            r_cmd.append(msg.twist.twist.angular.z)
            t_cmd_vel.append((t - gp_start) * 1e-9)

        pts = []
        u_act = []
        r_act = []
        t_odom = []
        connections = [x for x in r.connections if x.topic == "/wamv/sensors/position/ground_truth_odometry"]
        for connection, t, data in r.messages(connections=connections, start=gp_start, stop=end):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            pts.append(np.array([msg.pose.pose.position.x, msg.pose.pose.position.y]))
            u_act.append(msg.twist.twist.linear.x)
            r_act.append(msg.twist.twist.angular.z)
            t_odom.append((t - gp_start) * 1e-9)
        poses_x, poses_y = zip(*pts)

        cte = []
        connections = [x for x in r.connections if x.topic == "/wamv/crosstrack_error"]
        for connection, t, data in r.messages(connections=connections, start=dwa_start, stop=end):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            cte.append(np.abs(msg.data))
        pp(cte)
        pp(f"Maximum Crosstrack Error: {np.max(cte)}")
        pp(f"Average Crosstrack Error: {np.average(cte)}")
        pp(f"Median Crosstrack Error: {np.median(cte)}")

        if los_active:
            # fig = plt.figure(figsize=(6, 6))
            fig, ax = plt.subplots(2, 1, figsize=(6, 6))

            # ax1 = fig.add_axes([0.15, 0.7, 0.75, 0.2])
            ax[0].grid(True, linewidth=0.4)
            ax[0].set_xlabel("East / m")
            ax[0].set_ylabel("North / m")
            ax[0].minorticks_on()
            ax[0].xaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
            ax[0].yaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
            # ax1.set_xlim(-426, -367)
            # ax1.set_ylim(218, 233)

            ax[0].plot(gp_wpts_x, gp_wpts_y, "--", linewidth=0.8, color="g", label="Global Path")
            ax[0].plot(wpts_x, wpts_y, "--", markersize=1.0, color="0.1", label="DWA Trajectory")
            ax[0].plot(poses_x, poses_y, linewidth=1.5, color="crimson", label="Actual Path")
            ax[0].legend(loc="upper left")

            # ax2 = fig.add_axes([0.15, 0.4, 0.75, 0.2])
            ax[1].grid(True, linewidth=0.4)
            ax[1].set_xlabel("Time / s")
            ax[1].set_ylabel("Surge Velocity / m/s")
            ax[1].minorticks_on()
            ax[1].xaxis.set_major_formatter(lambda x, pos: "%.1f" % x)
            ax[1].yaxis.set_major_formatter(lambda x, pos: "%.2f" % x)

            ax[1].plot(t_odom, u_act, "-", markersize=0.5, color="steelblue", label="u")
            ax[1].plot(t_cmd_vel, u_cmd, "-", markersize=0.5, color="0.1", label="cmd_u")
            ax[1].legend(loc="upper left")
        else:
            # fig = plt.figure(figsize=(6, 6))
            fig, ax = plt.subplots(3, 1, figsize=(6, 6))

            # ax1 = fig.add_axes([0.15, 0.7, 0.75, 0.2])
            ax[0].grid(True, linewidth=0.4)
            ax[0].set_xlabel("East / m")
            ax[0].set_ylabel("North / m")
            ax[0].minorticks_on()
            ax[0].xaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
            ax[0].yaxis.set_major_formatter(lambda x, pos: "%.0f" % x)

            # ax[0].set_xlim(-426, -367)
            # ax[0].set_ylim(218, 233)

            ax[0].plot(gp_wpts_x, gp_wpts_y, "--", linewidth=0.8, color="g", label="Global Path")
            ax[0].plot(wpts_x, wpts_y, "--", markersize=1.0, color="0.1", label="DWA Trajectory")
            ax[0].plot(poses_x, poses_y, linewidth=1.5, color="crimson", label="Actual Path")
            ax[0].legend(loc="upper left")

            # ax2 = fig.add_axes([0.15, 0.4, 0.75, 0.2])
            ax[1].grid(True, linewidth=0.4)
            ax[1].set_xlabel("Time / s")
            ax[1].set_ylabel("Surge Velocity / m/s")
            ax[1].minorticks_on()
            ax[1].xaxis.set_major_formatter(lambda x, pos: "%.1f" % x)
            ax[1].yaxis.set_major_formatter(lambda x, pos: "%.2f" % x)

            ax[1].plot(t_odom, u_act, "-", markersize=0.5, color="steelblue", label="u")
            ax[1].plot(t_cmd_vel, u_cmd, "-", markersize=0.5, color="0.1", label="cmd_u")
            ax[1].legend(loc="upper left")

            # ax3 = fig.add_axes([0.15, 0.1, 0.75, 0.2])
            ax[2].grid(True, linewidth=0.4)
            ax[2].set_xlabel("Time / s")
            ax[2].set_ylabel("Yaw Velocity / rad/s")
            ax[2].minorticks_on()
            ax[2].xaxis.set_major_formatter(lambda x, pos: "%.1f" % x)
            ax[2].yaxis.set_major_formatter(lambda x, pos: "%.2f" % x)
            # ax[2].set_ylim(-0.3, 0.1)

            ax[2].plot(t_odom, r_act, "-", markersize=0.5, color="steelblue", label="r")
            ax[2].plot(t_cmd_vel, r_cmd, "-", markersize=0.5, color="0.1", label="cmd_r")
            ax[2].legend(loc="upper left")


def main():
    save_plot = True

    metrics_path = "{}/metrics".format(os.getcwd().split("/plots")[0])
    bag = f"{metrics_path}/trajectory/rosbag2_2024_04_24-15_55_28"
    bag_name = bag.split("/trajectory/")[1]
    pp(bag_name)
    los_active = True if bag_name.__contains__("LOS") else False

    plot_name = f"dwa_los_guidance_{bag_name}_APPENDIX.png" if los_active else (
        f"dwa_velocity_guidance_"
        f"{bag_name}_APPENDIX.png")
    plot_path = f"{os.getcwd()}/figures/{plot_name}"

    plot_dwa_traj_bag(bag, los_active)
    if save_plot:
        plt.savefig(plot_path)
    plt.show()


if __name__ == "__main__":
    main()
