import os
from pprint import pprint as pp

import matplotlib.pyplot as plt
import numpy as np
from rosbags.rosbag2 import Reader
from rosbags.typesys import Stores, get_typestore


def plot(fname):
    with open(fname, "r") as f:
        data = [line.rstrip("\n").split(":") for line in f if line.strip()]
        thetastar_path_length = [float(d[1].split(" m")[0]) for d in data if d[0].__contains__("Theta* distance")]
        astar_path_length = [float(d[1].split(" m")[0]) for d in data if d[0].__contains__("A* distance")]
        euclidean_dist = [float(d[1].split(" m")[0]) for d in data if d[0].__contains__("Euclidean Distance")]
        thetastar_execution_time = [float(d[1].split(" m")[0]) for d in data if d[0].__contains__("Theta* time")]
        astar_execution_time = [float(d[1].split(" m")[0]) for d in data if d[0].__contains__("A* time")]

        ts_dist_time = np.array(list(zip(thetastar_path_length, thetastar_execution_time)), dtype=[("length", float), ("time", float)])
        ts_sorted_length = np.sort(ts_dist_time, order="length")
        ts_sorted_time = np.sort(ts_dist_time, order="time")
        as_dist_time = np.array(list(zip(astar_path_length, astar_execution_time)), dtype=[("length", float), ("time", float)])
        as_sorted_length = np.sort(as_dist_time, order="length")
        as_sorted_time = np.sort(as_dist_time, order="time")

    x = np.arange(1, len(thetastar_path_length) + 1)[0:10]
    width = 0.3
    offset = width

    fig1, ax1 = plt.subplots(figsize=(10, 5), layout="constrained")
    ax1.grid(True, linewidth=0.4)
    ax1.set_ylabel("Path length / m")
    ax1.set_xlabel("Run / -")
    euc = ax1.bar(x - offset, np.sort(euclidean_dist)[0:10], width, label="Euclidean Distance", color="firebrick")
    ax1.bar_label(euc, fmt="%.0f")
    ts_length = ax1.bar(x, np.sort(thetastar_path_length)[0:10], width, label="Theta* Path Length", color="0.1")
    ax1.bar_label(ts_length, fmt="%.0f")
    as_length = ax1.bar(x + offset, np.sort(astar_path_length)[0:10], width, label="A* Path Length", color="steelblue")
    ax1.bar_label(as_length, fmt="%.0f")
    ax1.legend()
    plt.savefig(f"{os.getcwd()}/figures/thetastar_astar_length.png")

    z_thetastar = np.polyfit(thetastar_path_length, thetastar_execution_time, 2)
    p_thetastar = np.poly1d(z_thetastar)
    z_astar = np.polyfit(astar_path_length, astar_execution_time, 2)
    p_astar = np.poly1d(z_astar)

    fig2, ax2 = plt.subplots(figsize=(8, 5), layout="constrained")
    ax2.minorticks_on()
    ax2.xaxis.set_major_formatter(lambda x, pos: "%.2f" % x)
    ax2.yaxis.set_major_formatter(lambda x, pos: "%.2f" % x)
    ax2.grid(True, linewidth=0.4)
    ax2.set_ylabel("Execution Time / ms")
    ax2.set_xlabel("Path Length / m")
    ax2.plot(thetastar_path_length, thetastar_execution_time, "o", color="0.1", markersize="3.0")
    ax2.plot(np.sort(thetastar_path_length), p_thetastar(np.sort(thetastar_path_length)), linewidth=1.0, color='lightsteelblue')
    ax2.set_xlim(xmin=0)
    ax2.set_ylim(ymin=0)
    ax2.legend()
    plt.savefig(f"{os.getcwd()}/figures/runtime_thetastar.png")

    fig3, ax3 = plt.subplots(figsize=(8, 5), layout="constrained")
    ax3.grid(True, linewidth=0.4)
    ax3.minorticks_on()
    ax3.xaxis.set_major_formatter(lambda x, pos: "%.2f" % x)
    ax3.yaxis.set_major_formatter(lambda x, pos: "%.2f" % x)
    ax3.set_ylabel("Execution Time / ms")
    ax3.set_xlabel("Path Length / m")
    ax3.plot(thetastar_path_length, thetastar_execution_time, "o", color="0.1", markersize="3.0", label='Theta*')
    ax3.plot(np.sort(thetastar_path_length), p_thetastar(np.sort(thetastar_path_length)), linewidth=1.0, color='lightsteelblue')
    ax3.plot(astar_path_length, astar_execution_time, "o", color="steelblue", markersize="3.0", label='A*')
    ax3.plot(np.sort(astar_path_length), p_astar(np.sort(astar_path_length)), linewidth=1.0, color='lightsteelblue')
    ax2.set_xlim(xmin=0)
    ax2.set_ylim(ymin=0)
    ax3.legend()
    plt.savefig(f"{os.getcwd()}/figures/runtime_astar_thetastar.png")


def main():
    save_plot = False

    metrics_path = "{}/metrics".format(os.getcwd().split("/plots")[0])
    file = f"{metrics_path}/GlobalPathPlanner/astar_thetastar_comp.txt"

    plot_name = f"astar_thetastar_comp.png"
    plot_path = f"{os.getcwd()}/figures/{plot_name}"

    plot(file)
    plt.show()


if __name__ == "__main__":
    main()
