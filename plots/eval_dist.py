import os
from pprint import pprint as pp
import time
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from rosbags.rosbag2 import Reader
from rosbags.typesys import Stores, get_typestore

typestore = get_typestore(Stores.ROS2_HUMBLE)
save_plot = False

metrics_path = "{}/metrics".format(os.getcwd().split("/plots")[0])
bag = f"{metrics_path}/Evaluation/Distance/scenario3_default"
bag_name = bag.split("/Distance/")[1]
pp(bag_name)
plot_name = f"dist_to_obst_{bag_name}.png"
plot_path = f"{os.getcwd()}/figures/{plot_name}"

with Reader(bag) as r:
    start = int(r.start_time + 1e9)

    types = {}
    for c in r.connections:
        print(f"Topics in this bag: {c.topic}, Type: {c.msgtype}")
    typestore.register(types)

    t_gp = []
    connections = [x for x in r.connections if x.topic == "/global_path"]
    for connection, t, data in r.messages(connections=connections):
        t_gp.append(t)
    gp_start = t_gp[0]

    time = []
    dist = []
    connections = [x for x in r.connections if x.topic == "/wamv/distance_to_obstacle_f64/vessel_f_1"]
    for connection, t, data in r.messages(connections=connections, start=gp_start):
        msg = typestore.deserialize_cdr(data, connection.msgtype)
        dist.append(msg.data)
        time.append((t - r.start_time) * 1e-9)

names = ["Goal Distance Evaluator", "Obstacle Distance Evaluator", "Forward Evaluator"]
scores = [258430.6, -28542.5, 456.3]
norm_scores = [np.abs(s) / sum(np.abs(scores)) for s in scores]
for s in norm_scores: print(f"{s:.2f}")
pp(f"Minimum Distance: {min(dist)}")

plt.show()
