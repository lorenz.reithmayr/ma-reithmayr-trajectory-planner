import os
from pprint import pprint as pp

import matplotlib.pyplot as plt
import numpy as np
from rosbags.rosbag2 import Reader
from rosbags.typesys import Stores, get_typestore

save_plot = True

plot_name = "los_obstacles_res1.png"

plot_path = f"{os.getcwd()}/figures/{plot_name}"
metrics_path = "{}/metrics".format(os.getcwd().split("/plots")[0])
costmap = f"{metrics_path}/maps/costmap_1713418035.gz"
rosbag = f"{metrics_path}/LOS/rosbag2_2024_04_18-12_17_52"
typestore = get_typestore(Stores.ROS2_HUMBLE)


def load_map(path):
    return np.loadtxt(path)


def plot_costmap(path):
    origin = [-1087, -173.89, 0]
    x_off = -origin[0]
    y_off = -origin[1]
    extent = [-x_off, 1674 - x_off, -y_off, 949 - y_off]
    cstmap = load_map(path)
    plt.imshow(cstmap, cmap="BuPu", origin="lower", extent=extent)
    plt.xlim(-392.8, -215.4)
    plt.ylim(170, 310)
    return cstmap


def plot_los_bag():
    with Reader(rosbag) as r:
        start = r.start_time
        start_offset = int(start + 40e9)

        types = {}
        for c in r.connections:
            print(f"Topics in this bag: {c.topic}, Type: {c.msgtype}")
        typestore.register(types)

        connections = [x for x in r.connections if x.topic == "/global_path"]
        for connection, t, data in r.messages(connections=connections):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            wpts = [np.array([p.pose.position.x, p.pose.position.y]) for p in msg.poses]
        wpts_x, wpts_y = zip(*wpts)

        pts = []
        connections = [x for x in r.connections if x.topic == "/wamv/sensors/position/ground_truth_odometry"]
        for connection, t, data in r.messages(connections=connections, start=start_offset):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            pts.append(np.array([msg.pose.pose.position.x, msg.pose.pose.position.y]))
        poses_x, poses_y = zip(*pts)

        cte = []
        time = []
        connections = [x for x in r.connections if x.topic == "/wamv/crosstrack_error"]
        for connection, t, data in r.messages(connections=connections, start=start_offset):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            cte.append(np.abs(msg.data))
            time.append(t)

        pp(f"Maximum Crosstrack Error: {np.max(cte)}")
        pp(f"Average Crosstrack Error: {np.average(cte)}")
        pp(f"Median Crosstrack Error: {np.median(cte)}")

        fig, ax = plt.subplots()
        ax.grid(True, linewidth=0.4)
        ax.set_xlabel("East / m")
        ax.set_ylabel("North / m")
        ax.minorticks_on()
        ax.xaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
        ax.yaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
        ax.plot(wpts_x, wpts_y, "--", markersize=1.0, color="0.1", label="A* Path")
        ax.plot(poses_x, poses_y, linewidth=1.5, color="crimson", label="Actual Path")
        ax.legend()


def main():
    plot_los_bag()
    cmap = plot_costmap(costmap)
    if save_plot:
        plt.savefig(plot_path, dpi=cmap.shape[0])
    plt.show()


if __name__ == "__main__":
    main()
