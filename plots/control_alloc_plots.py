import os
from pprint import pprint as pp

import matplotlib.pyplot as plt
import numpy as np
from rosbags.rosbag2 import Reader
from rosbags.typesys import Stores, get_typestore


def plot_bag(rosbag):
    ly = 1.027135
    lx = 2.373776

    typestore = get_typestore(Stores.ROS2_HUMBLE)

    with Reader(rosbag) as r:
        start = int(r.start_time + 1e9)

        types = {}
        for c in r.connections:
            print(f"Topics in this bag: {c.topic}, Type: {c.msgtype}")
        typestore.register(types)

        time = []
        tau_x = []
        connections = [x for x in r.connections if x.topic == "/wamv/virtual_forces/tau_x"]
        for connection, t, data in r.messages(connections=connections, start=start):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            tau_x.append(msg.data)
            time.append((t - r.start_time) * 1e-9)

        time_n = []
        tau_n = []
        connections = [x for x in r.connections if x.topic == "/wamv/virtual_forces/tau_n"]
        for connection, t, data in r.messages(connections=connections, start=start):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            tau_n.append(msg.data)
            time_n.append((t - r.start_time) * 1e-9)

        thr_l = []
        connections = [x for x in r.connections if x.topic == "/wamv/thrusters/left/thrust"]
        for connection, t, data in r.messages(connections=connections, start=start):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            thr_l.append(msg.data)

        thr_r = []
        connections = [x for x in r.connections if x.topic == "/wamv/thrusters/right/thrust"]
        for connection, t, data in r.messages(connections=connections, start=start):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            thr_r.append(msg.data)

        pos_l = []
        connections = [x for x in r.connections if x.topic == "/wamv/thrusters/left/pos"]
        for connection, t, data in r.messages(connections=connections, start=start):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            pos_l.append(msg.data)

        pos_r = []
        connections = [x for x in r.connections if x.topic == "/wamv/thrusters/right/pos"]
        for connection, t, data in r.messages(connections=connections, start=start):
            msg = typestore.deserialize_cdr(data, connection.msgtype)
            pos_r.append(msg.data)

        f_act_x = [thr_l[i] * np.cos(pos_l[i]) + thr_r[i] * np.cos(pos_r[i]) for i in range(len(thr_r))]
        torque_act_n = [
            -thr_l[i] * np.cos(pos_l[i]) * ly - thr_l[i] * np.sin(pos_l[i]) * lx + thr_r[i] * np.cos(pos_r[i]) * ly -
            thr_r[i] * np.sin(pos_r[i]) * lx for i in range(len(thr_r))]

        fig, ax = plt.subplots(2, 1, figsize=(8, 8))
        ax[0].grid(True, linewidth=0.4)
        ax[0].set_xlabel("Time / s")
        ax[0].set_ylabel("Force / N")
        ax[0].minorticks_on()
        ax[0].xaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
        ax[0].yaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
        ax[0].set_xlim(0, 30)

        ax[0].plot(time, tau_x, linewidth=1.5, color="steelblue", label="Virtual Force X")
        ax[0].plot(time, f_act_x, linewidth=1.5, color="crimson", label="Actual Force X")
        ax[0].legend(loc="upper right")

        ax[1].grid(True, linewidth=0.4)
        ax[1].set_xlabel("Time / s")
        ax[1].set_ylabel("Torque / Nm")
        ax[1].minorticks_on()
        ax[1].xaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
        ax[1].yaxis.set_major_formatter(lambda x, pos: "%.0f" % x)
        ax[1].set_xlim(0, 30)

        ax[1].plot(time, tau_n, "-", markersize=0.5, color="steelblue", label="Virtual Torque N")
        ax[1].plot(time, torque_act_n, "-", markersize=0.5, color="crimson", label="Actual Torque N")
        ax[1].legend(loc="upper right")


def main():
    save_plot = True

    metrics_path = "{}/metrics".format(os.getcwd().split("/plots")[0])
    bag = f"{metrics_path}/ControlAlloc/rosbag2_2024_04_27-16_27_39"
    bag_name = bag.split("/ControlAlloc/")[1]
    pp(bag_name)

    plot_name = f"control_alloc_results_{bag_name}_APPENDIX.png"
    plot_path = f"{os.getcwd()}/figures/{plot_name}"

    plot_bag(bag)
    if save_plot:
        plt.savefig(plot_path)
    plt.show()


if __name__ == "__main__":
    main()
