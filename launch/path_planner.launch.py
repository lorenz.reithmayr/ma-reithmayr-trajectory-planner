import os
import yaml
from pysdf import SDF  # pip install python-sdformat
from enum import Enum

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription, LaunchContext
from launch.actions import IncludeLaunchDescription, RegisterEventHandler, TimerAction, SetEnvironmentVariable
from launch.event_handlers import OnProcessStart
from launch.events.process import ProcessStarted
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare


def generate_launch_description():
    root_dir = get_package_share_directory("trajectory_planner").split("install")[0]
    log_level = 'info'

    class Scenarios(Enum):
        STANDARD = os.path.join(get_package_share_directory('trajectory_planner'), 'gz_files', 'world',
                                'sydney_regatta_STANDARD.sdf')
        OBSTACLE = os.path.join(get_package_share_directory('trajectory_planner'), 'gz_files', 'world',
                                'sydney_regatta_OBSTACLE.sdf')
        CROSSING = os.path.join(get_package_share_directory('trajectory_planner'), 'gz_files', 'world',
                                'sydney_regatta_CROSSING.sdf')
        OVERTAKING = os.path.join(get_package_share_directory('trajectory_planner'), 'gz_files', 'world',
                                  'sydney_regatta_OVERTAKING.sdf')
        HEAD_ON = os.path.join(get_package_share_directory('trajectory_planner'), 'gz_files', 'world',
                               'sydney_regatta_HEADON.sdf')

    scenario = Scenarios.STANDARD

    """
        === Env Vars ===
    """
    SetEnvironmentVariable(name="GZ_SIM_RESOURCE_PATH", value=f"{root_dir}/models")

    """
        === Paths ===
    """
    gz_files_path = os.path.join(get_package_share_directory("trajectory_planner"), "gz_files")
    world_path = scenario.value
    occupancy_grid_config_path = os.path.join(get_package_share_directory('trajectory_planner'), 'map',
                                              'map_server_config_nobuffer.yaml') if scenario.name == "OBSTACLE" else (
        os.path.join(get_package_share_directory('trajectory_planner'), 'map', 'ogm_sydney_regatta_config.yaml'))
    global_costmap_config_path = os.path.join(get_package_share_directory('nav2_ground_truth_costmap_plugin'), 'config',
                                              'global_costmap_params.yaml')
    local_planner_config_path = os.path.join(get_package_share_directory('local_planner'), 'config',
                                             'local_planner_params.yaml')
    global_planner_config_path = os.path.join(get_package_share_directory('global_planner'), 'config',
                                              'global_planner_params.yaml')
    vrx_launch_path = os.path.join(get_package_share_directory('vrx_gz'), 'launch', 'competition.launch.py')
    rviz2_config_path = PathJoinSubstitution(
        [FindPackageShare('trajectory_planner'), 'rviz2_config', 'rviz2_config.rviz'])
    wamv_kinematic_params_path = os.path.join(get_package_share_directory('trajectory_planner'), 'config',
                                              'wamv_kinematic_params.yaml')
    radar_params_path = os.path.join(get_package_share_directory('trajectory_planner'), 'config', 'radar_params.yaml')
    los_guidance_params_path = os.path.join(get_package_share_directory('trajectory_planner'), 'config',
                                            'los_guidance_params.yaml')
    gz_models_params_path = os.path.join(get_package_share_directory('gz_model_controller'), 'config',
                                         'gz_model_params.yaml')

    """
        === Helpers ===
    """
    # Make sure the lifecycle manager is started last
    started_nodes = []

    # noinspection PyUnusedLocal
    def start_next_node(event: ProcessStarted, context: LaunchContext):
        print("Node {} started!".format(event.process_name))
        started_nodes.append([event.process_name])
        if len(started_nodes) == 4:
            print("Required Nodes are up. Starting MapServer Lifecycle Manager!")
            return map_server_lifecycle_manager

    # Parse the SDF world file, extract the vessel models, and pass them to the radar config file
    def parse_vessels_to_radar_params(world_sdf_path, radar_path):
        world_file = SDF.from_file(world_sdf_path, remove_blank_text=True)
        # noinspection PyTypeChecker
        vessels_in_world = [model.name for model in world_file.iter("include") if model.name.__contains__("vessel")]

        params = parse_params(radar_path)
        if not vessels_in_world:
            params["radar"]["ros__parameters"]["obstacle_vessels"] = ["none"]
        else:
            params["radar"]["ros__parameters"]["obstacle_vessels"] = vessels_in_world
        with open(radar_path, "w") as f:
            yaml.dump(params, f)
        return vessels_in_world

    def parse_params(path):
        with open(path, "r") as f:
            params = yaml.safe_load(f)
            return params

    vessels = parse_vessels_to_radar_params(world_path, radar_params_path)
    print(vessels)

    """
        === Lifecycle Nodes ===
    """
    map_server_lifecycle_nodes = ['map_server']
    use_sim_time = True
    autostart = True
    map_server_lifecycle_manager = Node(package='nav2_lifecycle_manager', executable='lifecycle_manager',
                                        name='lifecycle_manager_gridmap', output='screen', emulate_tty=True,
                                        # https://github.com/ros2/launch/issues/188
                                        parameters=[{'use_sim_time': use_sim_time}, {'autostart': autostart},
                                                    {'node_names': map_server_lifecycle_nodes}])

    nav2_lifecycle_nodes = ['planner_server']
    use_sim_time = True
    autostart = True
    bond_timeout = 10.0
    respawn = True
    nav2_lifecycle_manager = Node(package='nav2_lifecycle_manager', executable='lifecycle_manager',
                                  name='lifecycle_manager_navigation', output='screen', emulate_tty=True,
                                  # https://github.com/ros2/launch/issues/188
                                  parameters=[{'use_sim_time': use_sim_time}, {'autostart': autostart},
                                              {'node_names': nav2_lifecycle_nodes}, {'bond_timeout': bond_timeout},
                                              {'attempt_respawn_connection': respawn}])

    map_server = Node(package='nav2_map_server', executable='map_server', output='screen',
                      parameters=[{'yaml_filename': occupancy_grid_config_path}])
    planner_server = Node(package='nav2_planner', executable='planner_server', name='planner_server', output='screen',
                          parameters=[global_costmap_config_path])

    """
        === VRX ===
    """
    vrx_sydney_regatta = IncludeLaunchDescription(PythonLaunchDescriptionSource(vrx_launch_path),
                                                  launch_arguments={'world': world_path, 'urdf': PathJoinSubstitution(
                                                      [gz_files_path, 'custom_wamv', 'wamv_target.urdf']),
                                                                    'headless': 'True',
                                                                    'extra_gz_args': '-v 0'}.items(),
                                                  # additional_env={"GZ_SIM_RESOURCE_PATH": }
                                                  )
    vrx_teleop = IncludeLaunchDescription(PythonLaunchDescriptionSource(
        PathJoinSubstitution([FindPackageShare('vrx_gz'), 'launch', 'usv_joy_teleop.py'])))

    ros_gz_bridges = []
    for v in vessels:
        vessel_pose_bridge = Node(package='ros_gz_bridge', executable='parameter_bridge', arguments=[
            '/model/{}/pose_static@geometry_msgs/msg/PoseArray[gz.msgs.Pose_V'.format(v)], output='screen')

        vessel_odometry_bridge = Node(package='ros_gz_bridge', executable='parameter_bridge',
                                      arguments=['/model/{}/odometry@nav_msgs/msg/Odometry[gz.msgs.Odometry'.format(v)],
                                      output='screen')

        vessel_cmd_vel_bridge = Node(package='ros_gz_bridge', executable='parameter_bridge',
                                     arguments=['/model/{}/cmd_vel@geometry_msgs/msg/Twist@gz.msgs.Twist'.format(v)],
                                     output='screen')
        ros_gz_bridges.append(vessel_pose_bridge)
        ros_gz_bridges.append(vessel_odometry_bridge)
        ros_gz_bridges.append(vessel_cmd_vel_bridge)

    """
        === TrajectoryPlanner Nodes ===
    """
    vessel_nodes = []
    for v in vessels:
        vessel_pose_publisher = Node(package='trajectory_planner', namespace=v, executable='vessel_pose_publisher')
        vessel_model_publisher = Node(package='trajectory_planner', namespace=v, executable='vessel_model_publisher')
        obstacle_tracker = Node(package='trajectory_planner', namespace=v, executable='obstacle_tracker',
                                arguments=['--ros-args', '--log-level', log_level],
                                parameters=[{'use_sim_time': use_sim_time}])
        vessel_nodes.append(vessel_pose_publisher)
        vessel_nodes.append(vessel_model_publisher)
        vessel_nodes.append(obstacle_tracker)

    global_path_planner = Node(package='trajectory_planner', executable='global_path_planner',
                               arguments=['--ros-args', '--log-level', log_level],
                               parameters=[{'use_sim_time': use_sim_time}])

    map_handler = Node(package='trajectory_planner', executable='map_handler',
                       arguments=['--ros-args', '--log-level', log_level])

    wamv_odometry = Node(package='trajectory_planner', executable='wamv_odometry',
                         arguments=['--ros-args', '--log-level', log_level],
                         parameters=[{'use_sim_time': use_sim_time}])

    los_guidance = Node(package='trajectory_planner', executable='los_guidance',
                        arguments=['--ros-args', '--log-level', log_level],
                        parameters=[los_guidance_params_path, {'use_sim_time': use_sim_time}])

    visualization_module = Node(package='trajectory_planner', executable='visualization_module',
                                arguments=['--ros-args', '--log-level', log_level])

    trajectory_generator = Node(package='trajectory_planner', executable='trajectory_generator',
                                arguments=['--ros-args', '--log-level', log_level],
                                parameters=[wamv_kinematic_params_path, {'use_sim_time': use_sim_time}])

    radar = Node(package='trajectory_planner', executable='radar', arguments=['--ros-args', '--log-level', log_level],
                 parameters=[radar_params_path, {'use_sim_time': True}])

    """
    === C++ Nodes ===
    """
    local_planner = Node(package='local_planner', executable='local_planner_main',
                         parameters=[local_planner_config_path], arguments=['--ros-args', '--log-level', log_level])

    global_planner = Node(package='global_planner', executable='global_planner_main',
                          parameters=[global_planner_config_path], arguments=['--ros-args', '--log-level', log_level])
    """
    === Gazebo Obstacle Vessel Controller ===
    """
    gz_controller = Node(package='gz_model_controller', executable='gz_model_controller',
                         parameters=[gz_models_params_path], arguments=['--ros-args', '--log-level', log_level])

    """
    === Rviz2 ===
    """
    rviz2 = Node(package='rviz2', executable='rviz2', name='rviz2',
                 arguments=['-d', rviz2_config_path, '--ros-args', '--log-level', log_level])

    """
    === Testing === 
    """
    trajectory_guidance_tester = Node(package='testing', executable='trajectory_tester',
                                      arguments=['--ros-args', '--log-level', log_level])

    """
        === Launch actions ===
    """
    ld = LaunchDescription()

    # Event handlers call 'start_next_node' once the respective target_action is completed. Ensure sequential start
    # of nodes that are dependent on one another
    ld.add_action(RegisterEventHandler(event_handler=OnProcessStart(target_action=rviz2, on_start=start_next_node)))
    ld.add_action(
        RegisterEventHandler(event_handler=OnProcessStart(target_action=map_server, on_start=start_next_node)))
    ld.add_action(
        RegisterEventHandler(event_handler=OnProcessStart(target_action=map_handler, on_start=start_next_node)))
    ld.add_action(
        RegisterEventHandler(event_handler=OnProcessStart(target_action=global_path_planner, on_start=start_next_node)))

    ld.add_action(TimerAction(period=5.0, actions=[global_path_planner]))
    ld.add_action(vrx_sydney_regatta)
    ld.add_action(vrx_teleop)
    ld.add_action(rviz2)
    ld.add_action(map_server)
    ld.add_action(map_handler)

    for vn in vessel_nodes:
        ld.add_action(vn)

    for b in ros_gz_bridges:
        ld.add_action(b)

    ld.add_action(wamv_odometry)
    ld.add_action(los_guidance)
    ld.add_action(visualization_module)
    ld.add_action(trajectory_generator)
    ld.add_action(local_planner)
    ld.add_action(global_planner)
    ld.add_action(nav2_lifecycle_manager)
    ld.add_action(planner_server)
    ld.add_action(radar)
    ld.add_action(gz_controller)

    ld.add_action(trajectory_guidance_tester)

    return ld
