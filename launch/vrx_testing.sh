#!/bin/bash

ros2 bag record /global_path /wamv/distance_to_obstacle_f64/vessel_f_1 -o "${MA_METRICS_PATH}/Evaluation/Distance/$1" &
ros2 launch /home/lorenz/Projects/MA/ma-reithmayr-trajectory-planner/launch/path_planner.launch.py
