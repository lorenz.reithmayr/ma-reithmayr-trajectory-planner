#!/bin/bash
if [ "$1" = "-headless" ]; then
  /home/lorenz/MATLAB2023b/bin/matlab -nodisplay -nosplash -nodesktop -sd "/home/lorenz/Projects/MA/ma-reithmayr-trajectory-planner/simulink/" -r "run('run_tf_controller.m')"
fi

/home/lorenz/MATLAB2023b/bin/matlab  -sd "/home/lorenz/Projects/MA/ma-reithmayr-trajectory-planner/simulink/" -r "trajectory_follower_controller"