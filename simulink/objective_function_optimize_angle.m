function y = objective_function_optimize_angle(x, Q, Omega, tau, alpha_l_last, alpha_r_last) 
lx = 2.373776;
ly = 1.027135;
% Thrust Configuration Matrix for L and R thrusters
T_alpha_l = [cos(x(3)); sin(x(3)); -lx*sin(x(3))-ly*cos(x(3))]
T_alpha_r = [cos(x(4)); sin(x(4)); -lx*sin(x(4))+ly*cos(x(4))]

% Combination of the L and R TCM into one (simple concatenation of TCM R to TCM L from the
% right side)
T_alpha = [T_alpha_l T_alpha_r]

y = (tau-T_alpha*[x(1); x(2)])'*Q*(tau-T_alpha*[x(1); x(2)]) + [(x(3)-alpha_l_last); (x(4)-alpha_r_last)]'*Omega*[(x(3) - alpha_l_last); (x(4) - alpha_r_last)]; 
end

