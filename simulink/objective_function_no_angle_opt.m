function y = objective_function_no_angle_opt(x, Q, Omega, tau, T_alpha, alpha_l_last, alpha_r_last)
y = (tau-T_alpha*[x(1); x(2)])'*Q*(tau-T_alpha*[x(1); x(2)]) + [(x(3)-alpha_l_last); (x(4)-alpha_r_last)]'*Omega*[(x(3) - alpha_l_last); (x(4) - alpha_r_last)]; 
end

