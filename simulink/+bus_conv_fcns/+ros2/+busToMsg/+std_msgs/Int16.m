function rosmsgOut = Int16(slBusIn, rosmsgOut)
%#codegen
%   Copyright 2021 The MathWorks, Inc.
    rosmsgOut.data = int16(slBusIn.data);
end
