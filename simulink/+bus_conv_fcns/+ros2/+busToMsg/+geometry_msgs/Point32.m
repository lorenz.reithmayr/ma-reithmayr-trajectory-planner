function rosmsgOut = Point32(slBusIn, rosmsgOut)
%#codegen
%   Copyright 2021 The MathWorks, Inc.
    rosmsgOut.x = single(slBusIn.x);
    rosmsgOut.y = single(slBusIn.y);
    rosmsgOut.z = single(slBusIn.z);
end
