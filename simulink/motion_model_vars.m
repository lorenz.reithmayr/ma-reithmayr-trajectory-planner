m = 180.0
X_u_dot = -0.05
x_G  = 0
y_g = 0
Y_v_dot = 0.9
Y_r_dot = 0.5
N_v_dot = 0.5
I_z = 250
N_r_dot = 1.2


M_RB = [m 0 0; 0 m m*x_G; 0 m*x_G I_z]

M_A = [-X_u_dot 0 0; 0 -Y_v_dot -Y_r_dot; 0 -Y_r_dot -N_r_dot]

nu_c = [0 0 0]

tau = [0, 0, 0]