% t = out.ScopeData_u_r.time;
% u = out.ScopeData_u_r.signals.values(:,2);
% u_ref = out.ScopeData_u_r.signals.values(:,1);

t_LOS = out.ScopeData_LOS_hdg.time;
psi_d = out.ScopeData_LOS_hdg.signals(1).values;
psi = out.ScopeData_LOS_hdg.signals(2).values;

%tiledlayout(2,1)
%nexttile
hold on
box on
plot(t_LOS, psi, LineWidth=1.0);
plot(t_LOS, psi_d, LineWidth=1.0);
grid
xlabel("Time / s")
ylabel("psi / rad")
legend("Desired LOS Heading psi", "Actual Heading");

% nexttile
% hold on
% plot(t, u);
% plot(t, u_ref);
% grid
% title("Surge velocity")
% xlabel("Time / s")
% ylabel("u / m/s")
% legend("Surge velocity u", "Reference u\_ref");
% legend("location","northeast");
 
% nexttile
% hold on
% plot(t, r)
% plot(t, r_ref)
% grid
% legend("Yaw velocity r", "Reference r\_ref")
% legend("location","northeast")
% title("Yaw velocity");
% xlabel("Time / s");
% ylabel("r / m/s");
% 